from coastguard3.pipes.combiners import BaseCombiner
from coastguard3 import utils, config, errors, debug
from coastguard3.pipes.loaders.Asterix_loader import AsterixLoader

import os
import shutil
import warnings
import tempfile
from tqdm import tqdm


class AsterixCombiner(BaseCombiner):

    def __init__(self, trimpcnt=6.25, effix=False):
        """
        Args:
            trimpcnt(float, optional): Percentage (ie between 0-100) of subband
            to trim from _each_ edge of the band. Defaults to 6.25.
            effix(Bool, optional): Change observation site to eff_psrix to
            correct for asterix clock offsets. Defaults to False.
        """
        self.trimpcnt = trimpcnt
        self.effix = effix
        self.backend = 'Asterix'

    def prepare_subints(self, rawdirs, rawints, basetempdir):
        """Prepare subints by
            - Copying them to a temporary working directory
            - De-weighting a percentage from each sub-band edge
            - Converting archive format to PSRFITS

        Args:
            rawdir (list): List of raw sub-band directories containing
                    sub-ints to combine
            subints (list): Names of the subints present in rawdir
            basetempdir (str): Path to a base temporary directory where other
            temp files are to be stored

        Returns:
            tuple: A tuple containing:
                -tempdir (str): The band directory containing sub-ints to
                combine
                -tempints (list): List of subint files to be combined.
                    (NOTE: These are the file name only (i.e. no path))
                -parfile (str): Path to a parfile.
                -outdir (str): Path to output directory
        """

        # Use the first subint to extract a parfile and name
        arf = utils.ArchiveFile(os.path.join(rawdirs[0], rawints[0]))
        # Try to obtain a parfile for the pulsar
        parfile = self.get_parfile(arf)

        # Create temporary subints
        tempdir, tempints = self.create_temp_subints(basetempdir, rawdirs,
                                                     rawints)

        # Create the output directory where all output will be stored
        baseoutdir = os.path.join(config.output_location, arf['name'])
        outdir = os.path.join(baseoutdir, 'temp_combined_dir')
        self._make_dir(outdir)

        utils.print_info("Prepared {:d} subint fragments".format(
            len(tempints)), 2)

        return tempdir, tempints, parfile, outdir

    def create_temp_subints(self, basetempdir, rawdirs, rawints):
        """Create a temporary dir structure that mimics the original rawdata
        directory. Copy the rawdata to the temporary dirs and correct their
        headers while doing so.

        Args:
            basetempdir (str): Absolute path to a base temporary directory
            rawdirs (list): List of sub-band directories containing
                    sub-ints to combine
            rawints (list): Names of the subints contained in rawdirs

        Raises:
            errors.UnrecognizedValueError: If self.backend is formatted wrongly

        Returns:
            tuple: A tuple containing:
                - tempdirs (str): Absolute path to the temporary subband
                directories
                - tempints (list): Names of the subints present in tempdirs
        """

        tempdirs = []
        tempints = rawints  # For Asterix they are the same
        for subdir in tqdm(rawdirs, desc="Copying subints to temp"):
            # Create temporary directory for a subband
            freqdir = os.path.split(os.path.abspath(subdir))[-1]
            freqdir = os.path.join(basetempdir, freqdir)
            self._make_dir(freqdir)

            # Get absolute paths to raw subints
            abs_rawints = [os.path.join(subdir, fn) for fn in rawints]

            # Configure paz to correct subint headers
            preproc = 'convert psrfits'
            if self.effix:
                preproc += ',edit site=eff_psrix'
            if self.backend:
                if any(("," in self.backend, "=" in self.backend,
                        ' ' in self.backend)):
                    raise errors.UnrecognizedValueError("Backend value ({:s})"
                                                        "is invalid. It cannot"
                                                        " contain ',' or '=' "
                                                        "or ' '"
                                                        .format(self.backend)
                                                        )
                # Copy and correct subints using paz
                utils.execute(['paz', '-j', preproc, '-E', f'{self.trimpcnt}',
                               '-O', freqdir] + abs_rawints, stderr=os.devnull)
            tempdirs.append(freqdir)

        utils.print_info("Prepared {:d} subint fragments in {:d} freq sub-dirs"
                         .format(len(tempints), len(tempdirs)), 3)
        return tempdirs, tempints

    def combine_subints(self, subdirs, subints, parfile=None,
                        outdir=os.getcwd()):
        """Combine sub-ints from various freq sub-band directories.

        Args:
            subdirs (list): List of sub-band directories containing
                sub-ints to combine
            subints (list): List of subint files to be combined.
                (NOTE: These are the file name only (i.e. no path)
                    Each file listed should appear in each of the
                    subdirs.)
            parfn (str, optional): New ephemeris to install when combining
                subints. Defaults to None (Use ephemeris in archive file's
                header)
            outdir (str, optional): Directory to output combined file.
                Defaults to current working directory

        Returns:
            str: The name of the combined file.
        """

        subints = sorted(subints)

        # Make a temporary dir to store partial results
        tempdir = tempfile.mkdtemp(suffix="_combine", dir=config.tmp_directory)

        try:
            tempsubints = []
            # Perpare to mount new parfile if one is present
            if parfile is None:
                parargs = []
            else:
                parargs = ['-E', parfile]

            # Merge in frequency domain
            utils.print_info("Adding freq sub-bands for each sub-int...", 2)
            for subint in tqdm(subints, desc="Adding across frequency"):
                to_combine = [os.path.join(path, subint) for path in subdirs]
                tempsubints.append(outfile :=
                                   os.path.join(tempdir, f"combined_{subint}"))
                utils.execute(['psradd', '-q', '-R', '-o', outfile] + parargs +
                              to_combine, stderr=os.devnull)
            arf = utils.ArchiveFile(os.path.join(tempdir, "combined_%s" %
                                                 subints[0]))

            # Merge in time domain
            cmbfile = os.path.join(outdir,
                                   "{:s}_{:s}_{:s}_{:05d}_{:d}subints.cmb"
                                   .format(arf['name'], arf['band'],
                                           arf['yyyymmdd'], arf['secs'],
                                           len(subints))
                                   )
            utils.print_info("Combining %d sub-ints..." % len(tempsubints), 1)
            utils.execute(['psradd', '-q', '-o', cmbfile] + tempsubints,
                          stderr=os.devnull)

        except:
            raise # Re-raise the exception

        finally:
            if debug.is_on('reduce'):
                warnings.warn("Not cleaning up temporary directory ({:s})"
                              .format(tempdir))
            else:
                utils.print_info("Removing temporary directory ({:s})"
                                 .format(tempdir), 2)
                shutil.rmtree(tempdir)

        return cmbfile

    def read_groupfile(self, listfile):
        """Generate a rawdir and rawints from a listing file

        Args:
            listfile (str): Path to a listfile

        Returns:
            Tuple: A tuple containing:
                -rawdir (list): Absolute paths to the rawdata directories
                -rawints (list): Names of the subints contained in rawdirs
        """
        rawdirs, rawints = AsterixLoader.read_groupfile(listfile)
        return rawdirs, rawints

