import os
import importlib
from .base_combiner import BaseCombiner

# Get the current directory
current_dir = os.path.dirname(__file__)

# Loop through all .py files in the directory
for filename in os.listdir(current_dir):
    if filename.endswith(".py") and filename != "__init__.py":
        # Extract the module name (filename without .py extension)
        module_name = filename[:-3]

        # Import the module dynamically
        module = importlib.import_module(f".{module_name}",
                                         package=__package__)

        # Loop through the attributes of the module
        for name, obj in module.__dict__.items():
            # Check if it's a class and a subclass of your base class
            if isinstance(obj, type) and issubclass(obj, BaseCombiner):
                globals()[name] = obj
