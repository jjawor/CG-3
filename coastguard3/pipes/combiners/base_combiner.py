from coastguard3 import config, errors,  utils
import os
from abc import ABC, abstractmethod

class BaseCombiner(ABC):
    """The base class of Combiner objects.

    Combiner classes contain the suite of methods that are required to
    combine raw data from a specific backend.

    If a child class is implemented with the abstract methods defined following
    the specifications herein, it will work with the generic combiner manager.
    """

    @abstractmethod
    def prepare_subints(self, rawdir, rawints, basetempdir):
        """Prepare subints by
            - Copying them to a temporary working directory
            - Applying appropriate, backend dependent, corrections

        Args:
            rawdir(list): List of raw sub-band directories containing
                    sub-ints to combine
            rawints(list): Names of the subints present in rawdir
            basetempdir(str): Path to a root temporary directory, where other
            temp files are to be stored

        Returns:
            tuple: A tuple containing:
                -tempdir(str): The band directory containing sub-ints to
                combine
                -tempints(list): List of subint files to be combined.
                    (NOTE: These are the file name only (i.e. no path))
                -parfile(str): Path to a parfile.
                -outdir(str): Path to output directory
        """
        pass

    @abstractmethod
    def combine_subints(self, subdir, subints, parfile=None,
                        outdir=os.getcwd()):
        """Combine sub-ints that form an observation.
        This is backend dependent and must be implemented manually!


            Inputs:
                subdirs(list): List of sub-band directories containing
                    sub-ints to combine
                subints(list): List of subint files to be combined.
                    (NOTE: These are the file name only (i.e. no path))
                parfile(str, optional): Path to a parfile containig an
                ephemeris to install when combining. Defualts to None(do
                not install a new ephemeris)
                outdir(str, optional): Directory to output combined file.
                Defaults to None(Use the current working directory)

            Output:
                cmbfile: The name of the combined file.
        """

    @abstractmethod
    def read_groupfile(self, groupfile):
        """Generate a rawdir and rawints from a groupfile

        Args:
            groupfile (str): Path to a groupfile

        Returns:
            Tuple: A tuple containing:
                -rawdir (list): Absolute paths to the rawdata directories
                -rawints (list): Names of the subints contained in rawdirs
        """
        pass

    @staticmethod
    def get_parfile(arf):
        """Helper function that checks if an extrnal parfile exists for a given
        pulsar. If one exists, it normalizes and returns. Otherwise, it tries
        to extract a parfile from the archive instead.


        Args:
            arf (Archive): An archive object representing an archive file.

        Returns:
            str: Path to a temporary parfile
        """
        parfiles = utils.get_parfile_paths(config.parfiles_cfg)
        try:
            if arf['name'] in parfiles:
                parfile = utils.normalise_parfile(parfiles[arf['name']])
            else:
                parfile = utils.get_norm_parfile(arf.fn)
        except errors.InputError:
            # Archive has no parfile
            return None
        return parfile

    @staticmethod
    def _make_dir(path):
        """Helper method for creating a new directory, unless one already
        exists
        """
        try:
            os.makedirs(path)
        except OSError:
            # Directory already exists
            pass
