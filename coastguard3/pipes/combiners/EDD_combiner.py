from coastguard3.pipes.combiners import BaseCombiner
from coastguard3 import utils, config, errors
from coastguard3.pipes.loaders.EDD_loader import EDDLoader
import os


class EDDCombiner(BaseCombiner):

    def __init__(self) -> None:
        super().__init__()
        self.backend = 'EDD'

    def prepare_subints(self, rawdir, rawints, basetempdir):
        """Prepare subints for combination by:
            - Copying them to a temporary working directory
            - Converting archive format to PSRFITS

        Args:
            rawdir (str): Absolute path to the rawdata directory
            subints (list): Names of the subints present in rawdir
            basetempdir (str): Path to a base temporary directory, where other
            temp files are to be stored

        Returns:
            tuple: A tuple containing:
                -tempdir (str): The band directory containing sub-ints to
                combine
                -tempints (list): List of subint files to be combined.
                    (NOTE: These are the file name only (i.e. no path))
                -outdir (str): Directory to output combined file.
        """

        # Use the first subint to extract a parfile and name
        arf = utils.ArchiveFile(os.path.join(rawdir, rawints[0]))
        # Try to obtain a parfile for the pulsar
        parfile = self.get_parfile(arf)

        # Create temporary subints
        tempdir, tempints = self.create_temp_subints(basetempdir, rawdir,
                                                     rawints)

        # Create the output directory where all output will be stored
        baseoutdir = os.path.join(config.output_location, arf['name'])
        banddir_name = os.path.split(rawdir)[-1]
        outdir = os.path.join(baseoutdir, 'temp_combined_dir', banddir_name)
        self._make_dir(outdir)

        utils.print_info("Prepared {:d} subint fragments".format(
            len(tempints)), 2)
        return tempdir, tempints, parfile, outdir

    def create_temp_subints(self, basetempdir, rawdir, rawints):
        """Create a temporary dir structure that mimics the original rawdata
        directory. Copy the rawdata to the temporary dir and correct their
        headers while doing so.

        Args:
            basetempdir (str): Absolute path to a base temporary directory
            rawdir (str): Absolute path to the rawdata directory
            rawints (list): Names of the subints contained in rawdir

        Raises:
            errors.UnrecognizedValueError: If self.backend is formatted wrongly

        Returns:
            tuple: A tuple containing:
                - tempbanddir (str): Absolute path to the temporary band
                directory
                - tempints (list): Names of the subints present in tempbanddir
        """
        # Create a dir structure that mimics the original rawdata
        banddir_name = os.path.split(rawdir)[-1]
        tempbanddir = os.path.join(basetempdir, banddir_name)
        self._make_dir(tempbanddir)
        tempints = rawints  # For EDD, the names of the tempints are the same
        # as the raw ones
        # Absolute paths to the raw subints
        abs_rawints = [os.path.join(rawdir, int) for int in rawints]

        # Confgure paz to correct subint headers
        preproc = 'convert psrfits'
        if self.backend:
            if any(("," in self.backend, "=" in self.backend,
                    ' ' in self.backend)):
                raise errors.UnrecognizedValueError("Backend value ({:s}) is"
                                                    " invalid. It cannot "
                                                    "contain ',' or '=' or"
                                                    " ' '".format(self.backend)
                                                    )
            preproc += ',edit be:name=%s' % self.backend

        # Call PSRCHIVE paz. Changes archive header and stores the corrected
        # version in the temporary directory
        utils.execute(['paz', '-j', preproc, '-O', tempbanddir] + abs_rawints,
                      stderr=os.devnull)

        return tempbanddir, tempints

    def combine_subints(self, subdir, subints, parfile=None,
                        outdir=os.getcwd()):
        """Combine sub-ints that form an observation'.

        Args:
            subdir (str): The band directory containing sub-ints to combine
            subints (list): List of subint files to be combined.
                (NOTE: These are the file name only (i.e. no path))
            parfile (str, optional): Path to a parfile containig an
            ephemeris to install when combining. Defualts to None (do
            not install a new ephemeris)
            outdir (str, optional): Directory to output combined file.
            Defaults to the current working directory

        Returns:
            str: Path to the combined file.
        """

        subints = sorted(subints)

        try:
            # Perpare to mount new parfile if one is present
            if parfile is None:
                parargs = []
            else:
                parargs = ['-E', parfile]

            utils.print_info("Combining %d sub-ints..." % len(subints), 1)
            arf = utils.ArchiveFile(os.path.join(subdir, subints[0]))
            cmbfile = os.path.join(outdir, "{:s}_{:s}_{:s}_{:05d}_{:d}"
                                   "subints.cmb".format(arf['name'],
                                                        arf['band'],
                                                        arf['yyyymmdd'],
                                                        arf['secs'],
                                                        len(subints)))
            subints_abs = [os.path.join(subdir, subint) for subint in subints]
            utils.execute(['psradd', '-q', '-o', cmbfile] + parargs +
                          subints_abs, stderr=os.devnull)
        except:
            raise # Re-raise the exception

        return cmbfile

    def read_groupfile(self, groupfile):
        """Generate a rawdir and rawints from a groupfile

        Args:
            groupfile (str): Path to a groupfile

        Outputs:
            Tuple: A tuple containing:
                -rawdir (list): Absolute path to the rawdata directory (
                    contained in a list for compatibility reasons)
                -rawints (list): Names of the subints contained in rawdir
        """
        rawdir, rawints = EDDLoader.read_groupfile(groupfile)
        return rawdir, rawints
