from coastguard3.pipes.loaders import BaseLoader
from coastguard3 import utils, errors

import os
import collections
import warnings
from glob import glob
from os.path import join, isdir, basename
from datetime import datetime


class AsterixLoader(BaseLoader):
    """Loader for the Asterix backend from the Effelsberg 100m telescope.
    """

    def __init__(self, maxspan=3600, maxgap=119, missing_tol=0.7,
                 source=None):
        super().__init__()
        self.maxspan = maxspan  # Max number of seconds a combined archive can 
        # span (psradd -g)
        self.maxgap = maxgap  # Maximum gap between archives before starting 
        # a combined archive (psradd -G)
        self.missing_tol = missing_tol  # Maximal fraction of missing subints
        # from a subband. Subbands with a fraction exceeding this limit will be
        # removed from the groupfile 
        self.source = source

    backend = "Asterix"
    observatory = "Effelsberg"
    SUBINT_GLOB = '[0-9]'*4+'-'+'[0-9]'*2+'-'+'[0-9]'*2+'-' + \
                  '[0-9]'*2+':'+'[0-9]'*2+':'+'[0-9]'*2 + '.ar'
    SP_GLOB = "pulse_*.ar"
    backend_dirs = ("/fpra/timing/01/TIMING/Asterix_V2",
                    "/fpra/timing/01/TIMING/Asterix")

    @staticmethod
    def _get_start_from_subint(subint):
        subint = os.path.basename(subint)
        start = datetime.strptime(subint, "%Y-%m-%d-%H:%M:%S.ar")
        return start

    @staticmethod
    def _get_start_from_singlepulse(single):
        arf = utils.ArchiveFile(single)
        return arf.datetime

    FILETYPE_SPECIFICS = {'subint': (SUBINT_GLOB, _get_start_from_subint),
                          'single':
                          (SP_GLOB, _get_start_from_singlepulse)}

    def get_rawdata_dirs(self):
        """Get a list of directories likely to contain asterix data.
            Directories 2 levels deep with a name "YYYYMMDD" are returned.

            Returns:
                list: List of likely raw data directories.
        """

        outdirs = []
        indirs = []
        for basedir in self.backend_dirs:
            if self.source is None:
                # Not prioritizing any specific pulsars, use wildcard to match
                # all
                self.source = ["*"]
            for name in self.source:
                indirs.extend(glob(join(basedir, name)))

        # Search indirs for Asterix data
        for path in indirs:
            subdirs = glob(join(path, "*"))
            for subdir in subdirs:
                if isdir(subdir):
                    try:
                        datetime.strptime(basename(subdir), "%Y%m%d")
                    except:
                        # Bad pratice to use naked except
                        pass
                    else:
                        # Save directories whose name has the required format
                        outdirs.append(subdir)

        if not outdirs:
            warnings.warn("No rawdata directories found",
                          errors.CoastGuardWarning)

        return outdirs

    def make_groups(self, rawdir, filetype='subint'):
        """Given a directory containing ungrouped archives
        return a list of groups.

        Args:
            rawdir (str): A directory containing frequency sub-band
            directories.

        Raises:
            errors.MultipleBandDirectoriesError: There is more than one band
            directory with the same central frequency

        Returns:
            list: A list of groups. Each group is a tuple containig a list of
            subdirs and subints that make up a group.
        """

        groups = []
        # Try L-band, S-band, and C-band
        for band, subdir_pattern in \
                zip(['Lband', 'Sband', 'Cband'], ['1'+'[0-9]'*3,
                                                  '2'+'[0-9]'*3,
                                                  '[45]'+'[0-9]'*3]):
            subdirs = glob(join(rawdir, subdir_pattern))
            if subdirs:
                utils.print_info("Found %d freq sub-band dirs for %s in %s. "
                                 "Will group sub-ints contained" %
                                 (len(subdirs), band, rawdir), 2)
                usedirs, useints = self.group_subband_dirs(subdirs, filetype)

                # Format output to match the expected standard
                usedirs = [usedirs * len(useints)]
                if len(useints) == 0:
                    # Skip in case usedirs exist, but contain no subints
                    warnings.warn(f"Subdirs in directory {rawdir} exist, but "
                                  "contain no valid subints",
                                  errors.CoastGuardWarning)
                    continue
                groups.append(*zip(usedirs, useints))

        return groups

    def group_subband_dirs(self, subdirs, filetype='subint'):
        """
        Based on file names, groups sub-ints from different sub-bands.
        Each subband is assumed to be in a separate directory.

        Args:
            subdirs (list): List of sub-band directories.
            filetype (str, optional): Type of files being grouped. Can be
                'subint' or 'single'. (Default: 'subint')

        Returns:
            tuple: A tuple containing:
                usesubdirs (list): List of directories to use when combining.
                    (NOTE: This may be different than the input 'subdirs'
                    because some directories may have too few subints to be
                    worth combining. This depends on the input value of
                    'missing_tol'.)
                usesubints (list): List of lists. Each sublist contains subints
                    that make up a group. Each subint listed appears in each of
                    'usesubdirs'.

        Raises:
            errors.InputError: If the specified filetype is not recognized.
        """

        if filetype not in self.FILETYPE_SPECIFICS:
            raise errors.InputError(f"File type '{filetype}' is not recognized."
                                    f"Possible values are:"
                                    f"{list(self.FILETYPE_SPECIFICS.keys())}")
        else:
            globpat, get_start = self.FILETYPE_SPECIFICS[filetype]

        # Ensure paths are absolute
        usesubdirs = [os.path.abspath(path) for path in subdirs]
        utils.print_debug(f"Grouping subints from {len(usesubdirs)} sub-band"
                          "directories", 'combine')

        nindirs = len(usesubdirs)
        nsubbands = len(usesubdirs)
        nperdir = collections.Counter()
        noccurs = collections.Counter()
        nintotal = 0

        for subdir in usesubdirs:
            fns = glob(os.path.join(subdir, globpat))
            nn = len(fns)
            utils.print_debug(f"Found {nn} sub-int files in {subdir}",
                              'combine')
            nintotal += nn
            nperdir[subdir] = nn
            noccurs.update([os.path.basename(fn) for fn in fns])

        nsubints = len(noccurs)

        # Remove sub-bands that have too few subints
        thresh = self.missing_tol * nsubints
        for ii in range(len(usesubdirs) - 1, -1, -1):
            subdir = usesubdirs[ii]
            if nperdir[subdir] < thresh:
                utils.print_info(f"Ignoring sub-ints from {subdir}. It has too"
                                 f" few sub-ints ({nperdir[subdir]} < "
                                 f"{thresh}; missing_tol: {self.missing_tol})", 2)
                usesubdirs.pop(ii)
                del nperdir[subdir]

                fns = glob(os.path.join(subdir, globpat))
                noccurs.subtract([os.path.basename(fn) for fn in fns])
                nsubbands -= 1

        # Remove subints that are no longer included in any subbands
        to_del = []
        for fn in noccurs:
            if not noccurs[fn]:
                to_del.append(fn)
        for fn in to_del:
            del noccurs[fn]

        # Now combine subints
        lastsubint = datetime.min
        filestart = datetime.min
        usesubints = []
        if nsubbands:
            for subint in sorted(noccurs):
                if noccurs[subint] < nsubbands:
                    utils.print_info(f"Ignoring sub-int ({subint}). It doesn't"
                                     f" appear in all subbands (only"
                                     f"{noccurs[subint]} of {nsubbands})", 2)
                    continue
                start = get_start(os.path.join(usesubdirs[0], subint))
                # Check if span of a group is too large or if the gap between
                # two subints is to wide.
                if (start - filestart).total_seconds() > self.maxspan or\
                   (start - lastsubint).total_seconds() > self.maxgap:
                    # Start new group
                    filestart = start
                    utils.print_debug(f"Starting a new file at {filestart}",
                                      "combine")
                    # Start a new file
                    usesubints.append([])
                usesubints[-1].append(subint)
                lastsubint = start

        nused = sum([len(grp) for grp in usesubints])
        utils.print_info(f"Grouped {nintotal} files from {nindirs} directories"
                         f" into {len(usesubints)} groups. (Threw out "
                         f"{nindirs- len(usesubdirs)} directories and "
                         f"{nintotal - nused} files)", 2)

        return usesubdirs, usesubints

    @staticmethod
    def write_groupfile(subdirs, subints, outfn):
        """Write a groupfile containing a listing of subints and subdirs
         that should be combined into a single archive file.

        Args:
            subdirs (list): List of sub-band directories containing sub-ints to
                combine.
            subints (list): List of sub-int files to be combined. Only the file
                names (without paths) should be included.
            outfn (str): The name of the file to write the listing to.

        Raises:
            errors.InputError: If a groupfile with the requested name already
                exists.
        """

        # Ensure paths are absolute
        subdirs = [os.path.abspath(path) for path in subdirs]

        if os.path.exists(outfn):
            raise errors.InputError("A groupfile already exists with the "
                                    "requested output file name ({:s})!"
                                    .format(outfn))
        outfile = open(outfn, 'w')
        # Write header folowed by subdirs and subints
        outfile.write("# Listing of sub-int files to combine\n" +
                      "# Each file name listed below should appear " +
                      "in each of the following directories.\n" +
                      "# Each directory contains data from a different " +
                      "frequency sub-band.\n")
        outfile.write("===== Frequency sub-band directories =====\n")
        for subdir in sorted(subdirs):
            outfile.write(subdir+"\n")
        outfile.write("========== Sub-integration files =========\n")
        for subint in sorted(subints):
            outfile.write(subint+"\n")
        outfile.close()

    @staticmethod
    def read_groupfile(infn):
        """Reads a text file containing a listing of sub-ints that should be
        combined, as written by the 'write_listing' method.

        Args:
            infn (str): Path to the file containing the listing to read.

        Returns:
            tuple: A tuple containing:
                subdirs (list): List of sub-band directories containing
                sub-ints to combine.
                subints (list): List of sub-int files to be combined. Each file
                listed should appear in each of the subdirs. Only the file
                names (without paths) are included.

        Raises:
            errors.FormatError: If a non-comment line precedes the directory
            section of the file listing.
        """

        subdirs = []
        subints = []
        collector = None
        # Open the target file and start reading
        infile = open(infn, 'r')
        for line in infile:
            # Strip out comments
            line = line.partition('#')[0].strip()
            # Skip empty lines
            if not line:
                continue
            # Decied which collector to use for storing a line
            if "Frequency sub-band directories" in line:
                collector = subdirs
            elif "Sub-integration files" in line:
                collector = subints
            elif collector is None:
                raise errors.FormatError("Non-comment line preceeds directory "
                                         "section of file listing!")
            else:
                # Store line
                collector.append(line)
        infile.close()

        return subdirs, subints
