from abc import ABC, abstractmethod


class BaseLoader(ABC):
    """Abstract base class for loaders.
    Loader classes represent the connection between the DB and the
    rawdata directories. They contain methods for finding the raw archives
    from the backends and creating groupfiles that define single observations.

    If a child class is implemented with the abstract methods defined following
    the specifications herein, it will work with the generic loader manager.

    Unlike other steps. Loading classes are strictly tied to a specific
    observatory + backend pair. This is because the method is tied directly to
    the backend rawdata directory. Remember to set backend and observatory as
    class variables when defining the child class!
    """

    def __init__(self) -> None:
        super().__init__()

    observatory = None
    backend = None
    required_class_variables = ["observatory", "backend"]

    def __init_subclass__(cls, **kwargs):
        for var_name in cls.required_class_variables:
            if getattr(cls, var_name) is None:
                raise TypeError(f"Class {cls.__name__} must define the "
                                f"'{var_name}' class variable")

    @abstractmethod
    def make_groups(self, rawdir) -> list:
        """Given a directory containing ungrouped archives
        return a list of groups.

        Args:
            rawdir (str): The root directory containing rawdata. Different
            backends will have different structures

        Returns:
            list: A list of groups. Each group is a tuple containig a list of
            subdirs and subints that make up a group.
        """
        pass

    @abstractmethod
    def read_groupfile(self, infn) -> tuple:
        """Reads a text file containing a listing of sub-ints that should be
        combined, as written by the 'write_listing' method.

        Args:
            infn (str): Path to the file containing the listing to read.

        Returns:
            tuple: A tuple containing:
                dir (str): Band directory containing sub-ints to combine.
                subints (list): List of sub-int files to be combined.
                    Only the file names (without paths) are included.
        """
        pass

    @abstractmethod
    def write_groupfile(subdirs, subints, outfn) -> None:
        """Write a groupfile containing a listing of subints and subdirs
         that should be combined into a single archive file.

        Args:
            subdirs (list): List of sub-band directories containing sub-ints to
                combine.
            subints (list): List of sub-int files to be combined. Only the file
                names (without paths) should be included.
            outfn (str): The name of the groupfile.
        """
        pass

    @abstractmethod
    def get_rawdata_dirs(self) -> None:
        """Get a list of directories likely to contain backend data.

        Returns:
            list: List of likely raw data directories.
        """
        pass

    @staticmethod
    def check_if_calib(arf):
        """Check if an archive file is a pulsar observation or a calibration.
        Headers are often wrong about this, so it's important to have a
        different way to check. The default method for effelsberg archives is
        to check the file ending. _R files are PolCal scans.

        Args:
            arf (Archive): An Archive object representing an archive file

        Returns:
            str: The type of the archive
        """

        if arf['name'].endswith("_R"):
            obstype = 'cal'
        else:
            obstype = 'pulsar'

        return obstype
