from coastguard3 import utils, errors
from coastguard3.pipes.loaders import BaseLoader

import os
import re
import warnings
from glob import glob
from os.path import join, isdir, basename
from datetime import datetime, timedelta


class EDDLoader(BaseLoader):
    """Loader for the EDD backend for the Effelsberg 100m telescope"""

    def __init__(self, maxspan=3600, maxgap=119, source=None):
        super().__init__()
        self.maxspan = maxspan  # Max number of seconds a combined archive can 
        # span (psradd -g)
        self.maxgap = maxgap  # Maximum gap between archives before starting 
        # a combined archive (psradd -G)
        self.source = source

    backend = "EDD"
    observatory = "Effelsberg"
    backend_dirs= ("/fpra/comiss/01/EDD_STAGING_AREA/timing1/73-22",
                   "/fpra/comiss/01/EDD_STAGING_AREA/timing1/68-21")

    def get_rawdata_dirs(self):
        """Get a list of directories likely to contain EDD data.
        Directories 3 levels deep with a name "B/JXXXX+-YY(YY)" are returned.

        Returns:
            list: List of likely raw data directories.
        """

        outdirs = []
        for basedir in self.backend_dirs:
            subdirs = glob(join(basedir, '**'))
            for datedir in subdirs:
                # find all directories with date-like names
                if bool(re.search("^[0-9]{4}-[0-9]{2}-[0-9]{2}",
                                  basename(datedir))):
                    pulsardirs = glob(join(datedir, '**'))
                    for pulsardir in pulsardirs:
                        #find all directories with pulsar-like names
                        if isdir(pulsardir):
                            # Save dirs that conform to EDD directory pattern
                            if bool(re.search("^[JB][0-9]{4}[+-][0-9]{2,4}",
                                                basename(pulsardir))):
                                outdirs.append(pulsardir)
                            else:
                                pass
                else:
                    pass

        if not outdirs:
            warnings.warn("No directories found.", errors.CoastGuardWarning)

        return outdirs

    def make_groups(self, rawdir):
        """Given a directory containing ungrouped archives
        return a list of groups.

        Args:
            rawdir (str): A directory containing frequency band
            directories. EDD has only one (sub)band directory per observation,
            but there can be more than one band in stored in rawdir.

        Raises:
            errors.MultipleBandDirectoriesError: There is more than one band
            directory with the same central frequency

        Returns:
            list: A list of groups. Each group is a tuple containig a list of
            subdirs and subints that make up a group.
        """
        # Try L-band, S-band, and C-band
        groups = []
        for band, subdir_pattern in \
                zip(['Lband', 'Lband', 'Sband', 'Cband'], ['1400.0', '1400', '2550.0',
                                                  '4850.0']):
            subdir = glob(join(rawdir, subdir_pattern))
            if len(subdir) > 1:
                raise errors.MultipleBandDirectoriesError("There are multiple \
                                                          directories with the\
                                                           same central freq!")
            if subdir:
                utils.print_info("Found a band dir with central frequency \
                                 {:s}. Will group sub-ints contained"
                                 .format(subdir_pattern), 2)
                dir, subint_groups = self.group_subints(subdir[0])
                if dir is not None:
                    # Save group if any are possible to
                    # construct from the data in the rawdir
                    for subints in subint_groups:
                        groups.append(([dir], subints))

        return groups

    def group_subints(self, subdir):
        """
        Based on file names, groups sub-ints from a band.

        Args:
            subdir (str): Path to a band directory.

        Returns:
            tuple: A tuple containing:
                usesubdir (str): A band directory. This may be None if some
                    directories have too few subints to be worth combining.
                usesubints (list): A list of lists. Each sublist contains
                    subints that make up a single group in 'usesubdir'.
        """

        # Ensure path is absolute
        usesubdir = os.path.abspath(subdir)

        # Find all subints
        globpat = '[0-9]' * 2 + ':' + '[0-9]' * 2 + ':' + '[0-9]' * 2 + '_' +\
            '[0-9]' * 4 + '.ar'
        subints = glob(os.path.join(usesubdir, globpat))
        subints = [os.path.basename(subint) for subint in subints]
        nn = len(subints)
        utils.print_debug(f"Found {nn} sub-int files in {usesubdir}",
                          "combine")

        # Find unique groups based on subint names.
        # First, separate subints based on their starting observation time.
        unique_subint_name = set([subint[:8] for subint in subints])
        usesubints = []
        maxgap_inx = self.maxgap / 10  # The number of indices that corresponds
        # to maxgap. EDD files are written with an index number. Each file is a
        # 10s observation, so there are 10s between FILE_0001 and FILE_0002.
        maxspan_inx = self.maxspan / 10
        
        for pattern in unique_subint_name:
            # All subints with the same starting date
            samestart_subints = [subint for subint in subints if pattern in
                                 subint]

            start = 0
            for i, subint in enumerate(sorted(samestart_subints)):
                subint_inx = int(subint[9:13])  # The index of the subint

                # Create a new group if the gap between subints is too large
                if subint_inx - i > maxgap_inx:
                    usesubints.append(samestart_subints[start:i])
                    start = i
                    continue

                # Start a new group if a group exceeds the maximum size
                if start - i > maxspan_inx:
                    usesubints.append(samestart_subints[start:i])
                    start = i
                    continue

            # Save the last group if its not empty
            if not start == len(samestart_subints):
                usesubints.append(samestart_subints[start:len(samestart_subints)])

        return usesubdir, usesubints

    @staticmethod
    def write_groupfile(subdirs, subints, outfn):
        """Write a groupfile containing a listing of subints and subdirs
         that should be combined into a single archive file.

        Args:
            subdirs (list): List of sub-band directories containing sub-ints to
                combine. EDD has ony one subdir (the banddir), so there is only
                one entry in the list
            subints (list): List of sub-int files to be combined. Only the file
                names (without paths) should be included.
            outfn (str): The name of the file to write the listing to.

        Raises:
            errors.InputError: If a groupfile with the requested name already
                exists.
        """

        # Extract the one entry from subdirs and ensure path is absolute
        subdir = os.path.abspath(subdirs[0])

        if os.path.exists(outfn):
            raise errors.InputError("A groupfile already exists with the"
                                    "requested output file name ({:s})!"
                                    .format(outfn))
        # Write header folowed by subdir and subints
        outfile = open(outfn, 'w')
        outfile.write("# Listing of sub-int files to combine\n")
        outfile.write("============= Band directory =============\n")
        outfile.write(subdir+"\n")
        outfile.write("========== Sub-integration files =========\n")
        for subint in sorted(subints):
            outfile.write(subint+"\n")
        outfile.close()

    @staticmethod
    def read_groupfile(infn):
        """Reads a text file containing a listing of sub-ints that should be
        combined, as written by the 'write_listing' method.

        Args:
            infn (str): Path to the file containing the listing to read.

        Returns:
            tuple: A tuple containing:
                dir (str): Band directory containing sub-ints to combine.
                subints (list): List of sub-int files to be combined.
                Only the file names (without paths) are included.

        Raises:
            errors.FormatError: If a non-comment line precedes the directory
            section of the file listing.
        """

        subints = []
        dir = []
        collector = None
        # Open the target file and start reading
        infile = open(infn, 'r')
        for line in infile:
            # Strip out comments
            line = line.partition('#')[0].strip()
            # Skip empty lines
            if not line:
                continue
            # Decide which collector to use for storing a line
            elif "Band directory" in line:
                collector = dir
            elif "Sub-integration files" in line:
                collector = subints
            elif collector is None:
                raise errors.FormatError("Non-comment line preceeds directory "
                                         "section of file listing!")
            else:
                # Store line
                collector.append(line)
        infile.close()

        return dir[0], subints


class EDD_oldLoader(EDDLoader):
    backend_dirs = ("/fpra/comiss/01/EDD_STAGING_AREA/timing1/65-19",)

    def make_groups(self, rawdir):
        """Given a directory containing band subdirectories
        return a list of groups. This works with old EDD data, where the 
        rawdata might be a weird subdirectory

        Args:
            rawdir (str): A directory containing frequency band
            directories. EDD has only one band directory per observation,
            but there can be more than one observation in stored in rawdir.

        Raises:
            errors.MultipleBandDirectoriesError: There is more than one band
            directory with the same central frequency

        Returns:
            list: A list of groups. Each group is a tuple containig a list of
            subdirs and subints that make up a group.
        """
        # Try L-band, S-band, and C-band
        groups = []
        for band, subdir_pattern in \
                zip(['Lband', 'Lband', 'Sband', 'Cband'], ['1400.0', '1400', '2550.0',
                                                  '4850.0']):
            # Parse the band dirs
            subdir = glob(join(rawdir, subdir_pattern))
            if len(subdir) > 1:
                raise errors.MultipleBandDirectoriesError("There are multiple \
                                                          directories with the\
                                                           same central freq!")
            if subdir:
                utils.print_info("Found a band dir with central frequency "
                                 f"{subdir_pattern}.", 2)

                # Search for subdirs with rawdata in them
                archive_dirs = self._find_archives(subdir[0])
                for archive_dir in archive_dirs:
                    # Group subints into groups
                    dir, subint_groups = self.group_subints(archive_dir)
                    if dir is not None:
                        # Save groups if any are possible to
                        # construct from the data in the rawdir
                        for subints in subint_groups:
                            groups.append(([dir], subints))

        return groups

    def group_subints(self, subdir):
        """
        Based on file names, groups sub-ints from a band. There are two patters
        that are matched as the old EDD files don't conform to a single one.

        Args:
            subdir (str): Path to a band directory.

        Returns:
            tuple: A tuple containing:
                usesubdir (str): A band directory. This may be None if some
                    directories have too few subints to be worth combining.
                usesubints (list): A list of lists. Each sublist contains
                    subints that make up a single group in 'usesubdir'.
        """

        # Ensure path is absolute
        usesubdir = os.path.abspath(subdir)

        # Detect the subint pattern. Old EED data has two distinct patterns
        globpat1 = '[0-9]'*2+':'+'[0-9]'*2+':'+'[0-9]'*2+'_'+'[0-9]'*4+'.ar'
        globpat2 = '[0-9]'*4+'-'+'[0-9]'*2+'-'+'[0-9]'*2+'-'+\
            '[0-9]'*2+':'+'[0-9]'*2+':'+'[0-9]'*2+'.ar'
        subints1 = glob(os.path.join(usesubdir, globpat1))
        subints2 = glob(os.path.join(usesubdir, globpat2))

        if subints1 != [] and subints2 == []:
            subints = [os.path.basename(subint) for subint in subints1]
            usesubdir, usesubints = self._group_standard_subints(subints,
                                                                 usesubdir)
        elif subints1 == [] and subints2 != []:
             subints = [os.path.basename(subint) for subint in subints2]
             usesubdir, usesubints = self._group_old_subints(subints,
                                                             usesubdir)
        else:
            raise errors.DataReductionFailed(f"Subints in dir {usesubdir} "
                                             "have multiple naming conventions!")
        
        return usesubdir, usesubints

    def _find_archives(self, pulsardir):
        """Given a dir, return a list of all subdirs that contain .ar files"""

        datadirs = []
        for root, dirs, files in os.walk(pulsardir):
            ar = glob(join(root, "*.ar"))
            if any(s.endswith(".ar") for s in files):
                datadirs.append(root)

        return datadirs

    def _group_standard_subints(self, subints, usesubdir):
        nn = len(subints)
        utils.print_debug(f"Found {nn} sub-int files in {usesubdir} "
                          "ready to be combined", "combine")

        # Find unique groups based on subint names.
        # First, separate subints based on their starting observation time.
        unique_subint_name = set([subint[:8] for subint in subints])
        usesubints = []
        maxgap_inx = self.maxgap / 10  # The number of indices that corresponds
        # to maxgap. EDD files are written with an index number. Each file is a
        # 10s observation, so there are 10s between FILE_0001 and FILE_0002.
        maxspan_inx = self.maxspan / 10
        
        for pattern in unique_subint_name:
            # All subints with the same starting date
            samestart_subints = [subint for subint in subints if pattern in
                                 subint]

            start = 0
            for i, subint in enumerate(sorted(samestart_subints)):
                subint_inx = int(subint[9:13])  # The index of the subint

                # Create a new group if the gap between subints is too large
                if subint_inx - i > maxgap_inx:
                    usesubints.append(samestart_subints[start:i])
                    start = i
                    continue

                # Start a new group if a group exceeds the maximum size
                if start - i > maxspan_inx:
                    usesubints.append(samestart_subints[start:i])
                    start = i
                    continue

        # Save the last group if it's not empty
            if not start == len(samestart_subints):
                usesubints.append(samestart_subints[start:len(samestart_subints)])

        return usesubdir, usesubints
    
    def _group_old_subints(self, subints, usesubdir):
        nn = len(subints)
        utils.print_debug(f"Found {nn} sub-int files in {usesubdir} "
                          "ready to be combined", "combine")

        # Find unique groups based on subint names.
        usesubints = []
        maxgap = timedelta(seconds=self.maxgap)
        maxspan = timedelta(seconds=self.maxspan)

        start_dt = datetime.strptime(os.path.splitext(subints[0])[0],
                                     "%Y-%m-%d-%H:%M:%S")
        start = 0
        prev_dt = datetime.strptime(os.path.splitext(subints[0])[0],
                                    "%Y-%m-%d-%H:%M:%S")

        for i, subint in enumerate(sorted(subints)):
            subint_dt = datetime.strptime(os.path.splitext(subint)[0],
                                          "%Y-%m-%d-%H:%M:%S")
            # Create a new group if the gap between subints is too large
            if subint_dt - prev_dt > maxgap:
                usesubints.append(subints[start:i])
                start_dt = subint_dt
                start = i
                continue

            # Start a new group if a group exceeds the maximum size
            if start_dt - subint_dt > maxspan:
                usesubints.append(subints[start:i])
                start_dt = subint_dt
                start = i
                continue
            
            prev_dt = subint_dt

        # Save the last group if it's not empty
        if not start == len(subints):
            usesubints.append(subints[start:len(subints)])

        return usesubdir, usesubints
