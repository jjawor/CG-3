import os
from coastguard3.pipes.calibrators.base_calibrator import BaseCalibrator
from coastguard3 import errors, utils


class EffelsbergCalibrator(BaseCalibrator):

    def __init__(self, obs_extension='calib', cal_extension='pcal.T',
                 outpath=None) -> None:
        super().__init__()
        self.obs_extension = obs_extension
        self.cal_extension = cal_extension
        self.outpath = outpath

    def calibrate_pulsar(self, infile, caldbfile):
        """Calibrate a pulsar observation by using the provided caldb.

        Args:
            infile (str): Path the the file to be calibrated
            caldbfile (str): Path to the caldb to use

        Raises:
            errors.DataReductionFailed: If the caldb does not exist.
            errors.NoValidCalibratorError: If the file can't be calibrated as
                the caldb has no entries within the time treshold of pac.

        Returns:
            tuple: A tuple containing:
                - outfile: path to the calibrated file
                - nchans: number of channels in the calibrated file
        """

        infn, _ = os.path.splitext(os.path.basename(infile))
        inpath = os.path.dirname(infile)
        nchans = self.fix_channels(infile)

        utils.print_debug(f"Calibration DB: {caldbfile}", 'calibrate')

        if not os.path.isfile(caldbfile):
            raise errors.DataReductionFailed("Calibrator database "
                                             f"not found ({caldbfile}).")

        # Construct pac call parameters
        if nchans is not None:
            preproc = ['-j', f'F {nchans}']
        else:
            preproc = []
        if self.outpath is not None:
            # Outfile is to be in the specified directory.
            preproc.extend(['-O', self.outpath, '-e', self.obs_extension])
            outfile = os.path.join(self.outpath, infn)+'.'+self.obs_extension+'P'
        else:
            # Outfile is in the same dir as infile
            preproc.extend(['-e', self.obs_extension])
            outfile = os.path.join(inpath, infn)+'.'+self.obs_extension+'P'

        # Now calibrate polarization, scrunching to the appropriate
        # number of channels.
        stdout, stderr = utils.execute(['pac', '-P', '-d', caldbfile,
                                        infile] + preproc)

        # Check if stderr includes an error message due to a missing valid
        # polcal. I.e. pac can't calibrate due to a lack of a valild polcal
        # scan
        check = "Error::InvalidParam\nError::message\n\tno match " +\
            "found\n\tneither raw nor processed calibrator archives found"
        if stderr.find(check) > 0:
            raise errors.NoValidCalibratorError("No valid calibrator was found"
                                                f"for file {infile}")

        return outfile, nchans

    def prepare_cal(self, infile):
        """Prepare a diode scan for being used as a calibrator

        Args:
            infile (str): Path the the diode scan file

        Returns:
            tuple: A tuple containing:
                - outfile: path to the prepared diode scan file
                - nchans: number of channels in the prepared diode scan file
        """

        infn, _ = os.path.splitext(os.path.basename(infile))
        inpath = os.path.dirname(infile)
        nchans = self.fix_channels(infile)
        breakpoint()
        if self.outpath is not None:
            # Outfile is to be in the specified directory.
            preproc = ['-u', self.outpath, '-e', self.cal_extension]
            outfile = os.path.join(self.outpath,
                                   infn+'.'+self.cal_extension)
        else:
            # Outfile is in the same dir as infile
            preproc = ['-u', inpath, '-e', self.cal_extension]
            outfile = os.path.join(inpath, infn+'.'+self.cal_extension)

        utils.execute(['pam', '--setnchn', '{:d}'.format(nchans), '-T', infile]
                      + preproc)

        return outfile, nchans

    def fix_channels(self, infile):
        """Scrunch channels if nessecary
        WORK IN PROGRESS!
        """

        # arf = utils.ArchiveFile(infile)

        # if arf['rcvr'] == 'S60-2':
        #     nchans = arf['nchan']
        # else:
        #     # Reduce data to the equivalent of 128 channels over 200 MHz
        #     # That is f_chan = 1.5625 MHz
        #     nchans = abs(arf['bw'])/1.5625
        nchans = 256
        return int(nchans)


calibrator = EffelsbergCalibrator
