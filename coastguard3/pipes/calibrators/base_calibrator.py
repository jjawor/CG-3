from abc import ABC, abstractmethod

class BaseCalibrator(ABC):
    """
    The base class of Calibritor classes. Calibrator classes contain a suite of
    methods for calibrating cleaned files. 

    If a child class is implemented with the abstract methods defined following
    the specifications herein, it will work with the generic calibrator
    manager.
    """

    @abstractmethod
    def calibrate_pulsar(self, infile, caldb):
        """Calibrate a pulsar observation by using the provided caldb.

        Args:
            infile (str): Path the the file to be calibrated
            caldb (str): Path to the caldb to use

        Raises:
            errors.DataReductionFailed: If the caldb does not exist.
            errors.NoValidCalibratorError: If the file can't be calibrated as
                the caldb has no entries within the time treshold of pac.

        Returns:
            tuple: A tuple containing:
                - outfile: path to the calibrated file
                - nchans: number of channels in the calibrated file
        """

    @abstractmethod
    def prepare_cal(self, infile):
        """Prepare a diode scan for being used as a calibrator

        Args:
            infile (str): Path the the diode scan file

        Returns:
            tuple: A tuple containing:
                - outfile: path to the prepared diode scan file
                - nchans: number of channels in the prepared diode scan file
        """