from abc import ABC, abstractmethod


class BaseCorrecter(ABC):
    """The base class of Correcter objects.

    Correcter classes contain the suite of methods that are requires to
    correct the headers of combined data archives.

    If a child class is implemented with the abstract methods defined following
    the specifications herein, it will work with the generic correcter manager.
    """

    @abstractmethod
    def correct_header(self, arpath):
        """Correct the header of an archive.

        Args:
            arpath (str): Path to the input archive file.

        Returns:
            tuple: A tuple containing:
                corrfn (str): The name of the corrected file.
                corrstr (str): The parameter string of corrections to be used
                    with psredit.
                note (str): A note about header correction
        """
