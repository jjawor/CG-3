import pytz
import pyriseset3 as rs
import os
import pprint
import numpy as np
import shutil
from glob import glob
from datetime import datetime, timedelta
from sqlalchemy.orm import sessionmaker
from sqlalchemy import select, cast, DateTime, and_

from coastguard3 import errors, utils, config, database, clean_utils
from coastguard3.pipes.correcters import BaseCorrecter
from coastguard3.database.obslog_schema import ObsInfo


class AsterixCorrecter(BaseCorrecter):

    def __init__(self) -> None:
        super().__init__()
        self.recievers = {'P217': self.P217,
                          'S110': self.S110,
                          'P200': self.P200,
                          'S60': self.S60}
        self.band_reciever_map = {'Cband': ('S60', '2'), 'Xband': ('S36', '5'),
                                  'Sband': ('S110', '1')}
        self.timezone = pytz.timezone("Europe/Berlin")
        self.rs_obs = rs.sites.load('effelsberg')
        self.UTC_timezone = pytz.utc

    def P217(self, fixcoords, obsinfo, arf, backend, *args, **kwargs):
        base_corrstr = "rcvr:name=P217-3,rcvr:hand=+1,rcvr:basis=cir,"\
            "rcvr:sa=0deg,"
        corrstr, note = self.common_Eff_corrections(fixcoords, obsinfo, arf,
                                                    backend)
        corrstr = base_corrstr + corrstr
        return corrstr, note

    def S110(self, fixcoords, obsinfo, arf, backend, *args, **kwargs):
        base_corrstr = "rcvr:name=S110-1,rcvr:hand=+1,"\
            "rcvr:basis=cir,be:phase=-1,"
        corrstr, note = self.common_Eff_corrections(fixcoords, obsinfo, arf,
                                                    backend)
        corrstr = base_corrstr + corrstr
        return corrstr, note

    def P200(self, fixcoords, obsinfo, arf, backend, *args, **kwargs):
        base_corrstr = "rcvr:name=P200-3,rcvr:hand=-1,rcvr:basis=cir,"
        corrstr, note = self.common_Eff_corrections(fixcoords, obsinfo, arf,
                                                    backend)
        corrstr = base_corrstr + corrstr
        return corrstr, note

    def S60(self, fixcoords, obsinfo, arf, backend, *args, **kwargs):
        base_corrstr = "rcvr:name=S60-2,rcvr:hand=-1,rcvr:basis=cir,"
        corrstr, note = self.common_Eff_corrections(fixcoords, obsinfo, arf,
                                                    backend)
        corrstr = base_corrstr + corrstr
        return corrstr, note

    def common_Eff_corrections(self, fixcoords: bool, obsinfo, arf, backend,
                               *args, **kwargs):
        """Constructs a correction string for a number of corrections
        that most Effelsberg recievers require.

        Args:
            fixcoords (bool): Fix coordinates. Defaults to False: do not fix
                coords
            obsinfo (dict): Values from the observation log 
                used to correct the header
            arf (ArchiveFile): An ArchiveFile object representing the archive
                file which header is to be corrected
            backend (str): Backend to override the current one with.
                Defaults to None: Do not override backend

        Returns:
            tuple: A tuple containing:
                - corrstr (str): The correction string
                - note (str): the note to be inserted into the DB
        """

        note = ""

        # Fetch backend from archive if one is not provided
        if backend is None:
            backend = arf['backend']
        elif backend != arf['backend']:
            note += f"Overriding backend {arf['backend']} to '{backend}'. "
        corrstr = f"be:name={backend}"

        # If no obsinfo is provided, try fetching it from the Effelsberg DB
        try:
            # Search for observing log entry
            obsinfo = self.get_obslog_entry(arf, tolerant=True)
            if obsinfo is None:
                # No valid entry was found
                note += "Obslog could not be fetched from Effelsberg. Some"\
                        " corrections steps will not occur or use "\
                        "original archive values instead of obslog values. "
        except errors.HeaderCorrectionError as exc:
            note += exc.get_message() + "\n(Could not correct coordinates)"
            raise

        if any((fixcoords, (obsinfo is not None), arf['name'].endswith('_R'),
               arf['ra'].startswith('00:00:00'))):
            # Try fixing coordinates
            if obsinfo is not None:
                rastr, decstr = self._get_coordinates(obsinfo)
                corrstr += f",coord={rastr}{decstr}"
            else:
                note += "Not possible to correct 'coord'. "
        else:
            note += "No reasons to correct 'coord'. "

        # Try fixing object name, then, determine if the observation is a 
        # polcal diode scan, fluxcal, or a pulsar observation.
        if obsinfo is not None:
            name = obsinfo['object']
            corrstr += f",name={obsinfo['object']}"
            if name != arf['name']:
                note += f"'name' changed from {arf['name']} to {name}. "
        else:
            name = arf['name']
            note += "Original archive value of 'name' used to determine 'type'. "
        if name.endswith("_R"):
            # Calibration diode was fired.
            # Observation could be pol-cal scan or flux-cal scan
            if any([name.startswith(fluxcal) for fluxcal
                    in utils.read_fluxcal_names(config.fluxcal_cfg)]):
                # Flux calibrator
                if name.endswith("_S_R") or name.endswith("_N_R"):
                    corrstr += ",type=FluxCal-Off"
                    note += "'type' set to FluxCal-Off. "
                elif name.endswith("_O_R"): 
                    corrstr += ",type=FluxCal-On"
                    note += "'type' set to FluxCal-On. "
            else:
                # Polarization calibrator
                corrstr += ",type=PolnCal"
                note += "'type' set to PolnCal. "
        else:
            corrstr += ",type=Pulsar"
            note += "'type' set to Pulsar. "

        # Fix negative bandwidth
        if arf['bw'] < 0:
            corrstr += f",bw={abs(arf['bw'])}"
            note += "'bw' normalized to positive. "

        return corrstr, note

    def get_correction_string(self, arfile, obsinfo=None, rcvr=None,
                              backend=None, fixcoords=None):
        """Create a correction string to be passed to psredit.

        Args:
            arfile (str): Path to the archive file containinig the data to be
                corrected
            
        """

        note = ""

        # Load archive and determine reciever
        arf = utils.ArchiveFile(arfile)
        if rcvr is None:
            rcvr, rcvr_version = self.determine_rcvr(arf)
        elif rcvr.split("-")[0] in self.recievers.keys():
            rcvr, rcvr_version = rcvr.split("-")
        else:
            raise ValueError(f"Receiver provided ({rcvr}) is not recognized. "
                             f"Valid recievers are {self.recievers.keys()}")

        # Override archive reciever if it differs from the one found by the
        # determine_rcvr function or provided manually
        if arf['rcvr'] != rcvr:
            note += f"Current archive receiver ({arf['rcvr']}) is wrong "+\
                    f"setting to '{rcvr}-{rcvr_version}'. "

        # Call the appropriate method to construct the corrstring.
        corrstr, corr_note = self.recievers[rcvr](fixcoords, obsinfo, arf,
                                                  backend)
        note += corr_note

        return corrstr, note

    def get_obslog_entry(self, arf, tolerant=False):
        """Given an archive file, find the entry in the observing log.

        Inputs:
            arf: ArchiveFile object.
            tolerant: Be tolerant with name matching. 
                This is important for flux-cal observations.
                (Default: False)

        Output:
            obsinfo: A dictionary of observing information.
        """

        # Try fetching obslogs from the Effelsberg database
        obsdt_utc, names = self._prep_obslog_search(arf, tolerant)
        logentries = self._obslog_db_match(obsdt_utc, names)

        if not logentries:
            # Try searching the local obslogs for valid entries.
            pass
            # utils.print_info('No matches found in obslog DB. Searching text files.', 1)
            # logentries = self._obslog_file_match(obsdt_utc, names)

        if len(logentries) == 0:
            # No entries can be matched with the effelsberg database
            return None
        elif len(logentries) != 1:
            msg = f"Bad number ({len(logentries)}) of entries in obslog with "\
                  f"correct source name ({arf['name']}) close to observation "\
                  f"({arf.fn}) start time (UTC: {obsdt_utc.strftime('%c')})."
            msg += ":\n{:s}".format(
                "\n".join([pprint.pformat(entry) for entry in logentries]))
            raise errors.HeaderCorrectionError(msg)

        utils.print_debug("Information from matching observing log"
                          "line:\n{:s}".format(pprint.pformat(logentries[0])),
                          'correct')

        return logentries[0]

    def correct_header(self, infile, outdir=os.getcwd(), obsinfo=None, outfn=None,
                       backend=None, receiver=None, inplace=False):
        """Correct the header of an archive.

        Args:
            infile (str): Path to the input archive file.
            outdir (str): Path to the output directory. Defaults to the same
                directory as the input archive
            obsinfo (dict): A dictionary of observing log information to use.
                Default so None (search observing logs for matching entry)
            outfn (str): Output file name. Defaults to None (same as input
                file name, but with .corr extension)
            backend (str): Override backend name with this value.
                Defaults to None (Do not override backend)
            receiver (str): Override receiver name with this value. 
                Defaults None (Determine receiver automatically)
            inplace (bool): Change the original file in place. If outfn is
                specified the file will be moved to the new location. Defaults
                to False.

        Returns:
            tuple: A tuple containing:
                corrfn (str): The name of the corrected file.
                corrstr (str): The parameter string of corrections to be used
                    with psredit.
                note (str): A note about header correction
        """

        # Build correction string based on input data (achive and obsinfo)
        corrstr, note = self.get_correction_string(infile, obsinfo, backend,
                                                   receiver)
        # Correct the file using 'psredit'
        if inplace is True:
            utils.execute(['psredit', '-O', outdir, '-m', '-c', corrstr, infile],
                          stderr=os.devnull)
            # Assume the name of the corrected file
            corrfn = infile
        else:
            utils.execute(['psredit', '-O', outdir, '-e', 'corr', '-c',
                           corrstr, infile], stderr=os.devnull)
            # Assume the name of the corrected file
            corrfn = os.path.splitext(arpath)[0]+".corr"

        # Confirm that our assumed file name is accurate
        if not os.path.isfile(corrfn):
            raise errors.HeaderCorrectionError("The corrected file ({:s}) does"
                                               "not exist!".format(corrfn))
        # Rename output file
        if outfn is not None:
            arf = utils.ArchiveFile(corrfn)
            fn = outfn % arf
            shutil.move(corrfn, fn)
            corrfn = fn

        return corrfn, corrstr, note

    def determine_rcvr(self, arf, reset_weights=False):
        """Given an ArchiveFile object determine the name
        and version of the Effelsberg receiver used to gather the data

        Args:
            arf (str): An ArchiveFile object.
            reset_weights (bool): Reset weights to uniform. Defaults to False
            (use current weights).

        Returns:
            str: The name of the receiver.
        """

        if arf['band'] == 'Lband':
            # L-band
            # print("it is an lband receiver", arf['rcvr'])
            ar = arf.get_archive()
            if reset_weights:
                ar.uniform_weight(1.0)

            # Scrunch
            ar.pscrunch()
            ar.tscrunch()

            # Get the relevant data
            chnwts = clean_utils.get_chan_weights(ar).astype(bool)
            stddevs = ar.get_data().squeeze().std(axis=1)
            freqs = clean_utils.get_frequencies(ar)

            # Outside P200 receiver's response
            iout = (freqs < 1285.0) | (freqs > 1437.0)
            if np.sum(iout) < 5:
                raise errors.HeaderCorrectionError("Cannot determine L-band "
                                                   "receiver. Too few channels"
                                                   " ({:d}) outside P200's "
                                                   "response."
                                                   .format(np.sum(iout)))
            outside = stddevs[iout][chnwts[iout]].mean()
            inside = stddevs[~iout][chnwts[~iout]].mean()

            if inside/outside > 5:
                # There does not appear to be signal outside the P200-3
                # receiver's response: single-pixel receiver
                rcvr, version = 'P200', '3'
            elif inside/outside < 2:
                # There appears to be signal outside the P200-3
                # receiver's response: 7-beam receiver
                rcvr, version = 'P217', '3'
            elif 'P21' in arf['rcvr']:
                rcvr, version = 'P217', '3'
            elif 'P20' in arf['rcvr']:
                rcvr, version = 'P200', '3'
            else:
                raise errors.HeaderCorrectionError(
                    "Cannot determine receiver.(Outside: {:d} chan, avg"
                    "stddev={:g}; Inside: {:d} chan, avg stddev={:g})"
                    .format(np.sum(iout), outside, np.sum(~iout), inside))
        elif arf['band'] in list(self.band_reciever_map.keys()):
            rcvr, version = self.band_reciever_map[arf['band']] 
        else:
            raise errors.HeaderCorrectionError("No methods availible to "
                                               "correct headers for {:s} "
                                               "observations."
                                               .format(arf['band']))
        return rcvr, version

    def _obslog_db_match(self, obsdt_utc, names):
        """Find entries in observing log database matching the given
        information.

        Args:
            obsdt_utc (datetime): The UTC datetime at the start of the
                observation
            names (list): List of object names to match

        Returns:
            list: Matching log entries.
        """

        engine = database.get_engine('effelsberg')
        Session = sessionmaker(engine)
        # Find entries within +- 1 day of observation start time
        start = obsdt_utc - timedelta(days=1)
        end = obsdt_utc + timedelta(days=1)
        query = select(ObsInfo.object, (ObsInfo.lst/3600).label('lststart'),
                       cast(ObsInfo.obstimestamp, DateTime).label('utcstart'),
                       ObsInfo.azim, ObsInfo.elev, ObsInfo.scan, ObsInfo.lon,
                       ObsInfo.lat)\
                       .where(and_(ObsInfo.object.in_(names),
                                   cast(ObsInfo.obstimestamp, DateTime)
                                   >= start,
                                   cast(ObsInfo.obstimestamp, DateTime)
                                   <= end))

        with Session() as session:
            obslogs = session.execute(query).all()

        utils.print_debug("Found {:d} matching obslog DB entries " 
                          "(name: {:s}; UTC: {:s})"
                          .format(len(obslogs), ", ".join(names),
                                  obsdt_utc.strftime("%c")), 'correct')

        logentries = []
        for log in obslogs:
            # refine matching based on time. Only obslogs within 25 sec of the
            # start of the observations are to be considered
            utils.print_debug(f"{log}", 'correct')
            twentyfivesec = timedelta(seconds=25)
            logdt_utc = self.UTC_timezone.localize(log.utcstart)
            if (logdt_utc-twentyfivesec) <= obsdt_utc <= (logdt_utc+twentyfivesec):
                # Compute a few values to be consistent with obslog file parsing
                utc_hrs = log.utcstart.hour + (log.utcstart.minute +
                                               (log.utcstart.second +
                                                log.utcstart.microsecond*1e-6)
                                               / 60.0)/60.0

                logdt_local = logdt_utc.astimezone(self.timezone)
                localdate = logdt_local.date()

                entry = dict(log._mapping)
                entry['scannum'] = str(log.scan)
                entry['utcstart'] = utc_hrs
                entry['utc'] = log.utcstart.strftime('%c')
                entry['localdate'] = localdate
                entry['catalog_rastr'] = \
                    rs.utils.deg_to_hmsstr(log.lon, decpnts=3,
                                           style='units')[0]
                entry['catalog_decstr'] = \
                    rs.utils.deg_to_dmsstr(log.lat, decpnts=3,
                                           style='units')[0]

                logentries.append(entry)
        return logentries

    def _obslog_file_match(self, obsdt_utc, names):
        """Find entries in observing log files matching the given information.

            Inputs:
                obsdt_utc: The UTC datetime at the start of the observation
                names: Object names to match

            Outputs:
                logentries: Matching log entries.
        CURRENTLY NOT USED
        """
        obsdt_local = obsdt_utc.astimezone(self.timezone)
        obsutc = obsdt_utc.time()
        obsdate = obsdt_local.date()
        # NOTE: discrepancy between timezones for time and date. This is a bad
        # idea, but is done to be consistent with what is used in the observation
        # log files.
        obsutc_hours = obsutc.hour+(obsutc.minute+(obsutc.second)/60.0)/60.0
        obsutc_hhmm = obsutc.hour+(obsutc.minute)/60.0
        if obsutc.second > 30:
            delta = 1/60.0
        else:
            delta = -1/60.0

        # Get log file
        # NOTE: Date in file name is when the obslog was written out
        obslogfiles = glob(os.path.join(config.obslog_dir, "*.prot"))
        obslogfiles.sort()

        tosearch = []
        low = 0
        high = len(obslogfiles)
        # Find the logentry closest to the observation
        while True:
            mid = (low+high)//2
            currfile = obslogfiles[mid]
            currdatetime = datetime.strptime(os.path.split(currfile)[-1],
                                             '%y%m%d.prot')
            currdate = currdatetime.date()

            if currdate == obsdate:
                tosearch.append(currfile)
                break
            elif currdate < obsdate:
                low = mid + 1
            elif currdate > obsdate:
                high = mid - 1
            elif low <= high:
                tosearch.append(currfile)
                break 
        # for currfn in obslogfns:
        #     fndatetime = datetime.strptime(os.path.split(currfn)[-1], \
        #                                             '%y%m%d.prot')
        #     fndate = fndatetime.date()

        #     if fndate == obsdate:
        #         tosearch.append(currfn)
        #     elif fndate > obsdate:
        #         tosearch.append(currfn)
        #         break
        if not tosearch:
            raise errors.HeaderCorrectionError("Could not find an obslog file " \
                                            "for the obs date ({:s})."\
                                                .format(obsdate.strftime
                                                        ("%Y-%b-%d")))

        utils.print_debug('Searching obs log files:\n    {:s}'
                          .format("\n    ".join(tosearch)), 'correct')

        # Find entry associated with the pulsar in question in the logfile
        logentries = []
        check = False
        for obslogfn in tosearch:
            with open(obslogfn, 'r') as obslog:
                for line in obslog:
                    try:
                        currinfo = self.parse_obslog_line(line)
                    except errors.FormatError:
                        # Not a valid observation log entry
                        continue
                    if check:
                        utils.print_debug("Checking obslog line:\n{:s}\n"
                                          "Obs date: {:s}, obs log date: {:s},"
                                          " next date: {:s}\n Obs UTC: {:f}, "
                                          "obs log UTC: {:f}, next UTC: {:f}\n"
                                          .format(prevline, obsdate,
                                                  previnfo['localdate'], 
                                                  currinfo['localdate'],
                                                  obsutc_hhmm,
                                                  previnfo['utcstart'],
                                                  currinfo['utcstart']),
                                                  'correct')
                        if all(obsdate >= previnfo['localdate'],
                            (obsdate <= currinfo['localdate']),
                            (self.is_close(obsutc_hhmm, previnfo['utcstart'],
                                           1)))\
                        or self.is_close(obsutc_hhmm+delta,
                                            previnfo['utcstart'], 1):
                            utils.print_debug("Matching observing log line:\n"
                                              "{:s}"\
                                                .format(prevline, 'correct'))
                            logentries.append(previnfo)
                    # Check in next iteration if observation's source name matches
                    # that of the current obslog entry
                    check = (utils.get_prefname(currinfo['name']) in names)
                    prevline = line
                    previnfo = currinfo
        utils.print_debug("Found {:d} potentially matching obs-log entries"\
                          .format(len(logentries)), 'correct')
        return logentries

    def _prep_obslog_search(self, arf, tolerant):
        """Prepare some observation info for searching
            for observing log entries.

            Inputs:
                arf: ArchiveFile object.
                tolerant: Be tolerant with name matching. 
                    This is important for flux-cal observations.
                    (Default: False)

            Outputs:
                obsdt_utc: The UTC datetime at the start of the observation
                names: Object names to match
        """
        # Use tolerant name matching
        # Be sure to use the original name recorded in the header
        names = (arf['origname'],)
        if tolerant and arf['origname'].endswith("_R") and \
                not (("_O" in arf['origname']) or ("_N" in arf['origname']) or
                     ("_S" in arf['origname'])):
            base = arf['origname'][:-2]
            if base in utils.read_fluxcal_names():
                # Be tolerant with name matching
                names += (base+"_N_R", base+"_O_R", base+"_S_R")
            else:
                names += (base.lstrip('BJ')+'_R',)
        utils.print_debug("Will check for the following name for obs-log "
                          "matching: {:s}".format(", ".join(names)), 'correct')

        # Get date of observation
        obsdt_utc = rs.utils.mjd_to_datetime(arf['mjd'])
        obsdt_utc = self.UTC_timezone.localize(obsdt_utc)
        return obsdt_utc, names

    def _get_coordinates(self, obsinfo):
        """Compute telescope coordinates.

        Args:
            obsinfo (dict): A dict of values containing starting coordinates
                and time.

        Returns:
            tuple: A tuple containing:
                rastr (str):  RA in hms format
                decstr (str): Dec in dms format
        """
        ra_deg, decl_deg = \
            self.rs_obs.get_skyposn(obsinfo['elev'], obsinfo['azim']+180,
                                    lst=obsinfo['lststart'])
        rastr = rs.utils.deg_to_hmsstr(ra_deg, decpnts=3)[0]
        decstr = rs.utils.deg_to_dmsstr(decl_deg, decpnts=2)[0]
        if decstr[0] not in ('-', '+'):
            decstr = "+" + decstr
        return rastr, decstr

    def _parse_obslog_line(line):
        """
        Given a line from a observing log, parse it.

            Input:
                line: A single line from an observing log.

            Output:
                info: A dictionary of information parsed from the
                    observing log entry.
        """

        OBSLOG_FIELDS = (('localdate', rs.utils.parse_datestr),
                 ('scannum', str),
                 ('utcstart', rs.utils.parse_timestr),
                 ('lststart', rs.utils.parse_timestr),
                 ('name', str),
                 ('az', float),
                 ('alt', float),
                 ('catalog_rastr', str),
                 ('catalog_decstr', str))
        
        valstrs = line.split()
        if len(valstrs) < len(OBSLOG_FIELDS):
            # Not a valid observation log entry
            raise errors.FormatError("Observing log entry has bad format. " \
                            "Require at least %d fields." % len(OBSLOG_FIELDS))
        currinfo = {}
        for (key, caster), valstr in zip(OBSLOG_FIELDS, valstrs):
            currinfo[key] = caster(valstr)
        return currinfo

    def _is_close(hr1, hr2, delta=1):
        """Check it two times, in hours, are close.

            Inputs:
                hr1: A time in hours.
                hr2: Another time in hours.
                delta: The maximum difference (in seconds) for the two input
                    times to be considered close. (Default: 1 s)

            Output:
                close: True if the times fall within 'delta' of each other.
        """
        return abs(hr1-hr2) < (delta/3600.0)
