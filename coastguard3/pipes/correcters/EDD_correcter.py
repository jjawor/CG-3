import pytz
import pyriseset3 as rs
import os
import pprint
import numpy as np
import shutil
import warnings
from glob import glob
from datetime import datetime, timedelta
from sqlalchemy.orm import sessionmaker
from sqlalchemy import select, cast, DateTime, and_

from coastguard3 import errors, utils, config, database, clean_utils
from coastguard3.pipes.correcters import BaseCorrecter
from coastguard3.database.obslog_schema import ObsInfo


class EDDCorrecter(BaseCorrecter):
    backend = "EDD"
    band_reciever_map = {'Lband': ('P217', '1'),'Cband': ('S60', '1'),
                         'Sband': ('S110', '1')}
    timezone = pytz.timezone("Europe/Berlin")
    rs_obs = rs.sites.load('effelsberg')
    UTC_timezone = pytz.utc

    def __init__(self, obsinfo=None, rcvr=None, fixcoords=None) -> None:
        """
        Args:
            obsinfo (dict): A dictionary containing information to use for
                correcting.
                Defaults so None (search Effelsberg observing logs for matching
                and fetch the required values from there)
            receiver (str): Override receiver name with this value. 
                Defaults None (Determine receiver automatically)
            fixcoords (bool): Forcefully fix coordinates. Defaults to False:
                Only fix coords for archives with erronious coordinates and
                ones containing polcal scans. 
        """

        super().__init__()
        self.obsinfo = obsinfo
        self.rcvr = rcvr
        self.fixcoords = fixcoords
        if obsinfo is not None:
            # Check if the manually provided obsinfo has all of the required
            # entries
            self._check_obsinfo_validity()

    def _P217(self, arf):
        base_corrstr = "rcvr:name=P217-1,rcvr:basis=cir"
        corrstr, note = self._dynamic_corrections(arf)
        corrstr = base_corrstr + corrstr
        return corrstr, note

    def _S110(self, arf):
        base_corrstr = "rcvr:name=S110-1,rcvr:basis=cir"
        corrstr, note = self._dynamic_corrections(arf)
        corrstr = base_corrstr + corrstr
        return corrstr, note

    def _S60(self, arf):
        base_corrstr = "rcvr:name=S60-1,rcvr:basis=cir"
        corrstr, note = self._dynamic_corrections(arf)
        corrstr = base_corrstr + corrstr
        return corrstr, note

    recievers = {'P217': _P217, 'S110': _S110, 'S60': _S60}

    def _dynamic_corrections(self, arf):
        """Constructs a correction string for a number of corrections
        that need to be evaluated dynamically.

        Args:
            arf (ArchiveFile): An ArchiveFile object representing the archive
                file which header is to be corrected

        Returns:
            tuple: A tuple containing:
                - corrstr (str): The correction string
                - note (str): the note to be inserted into the DB
        """

        note = ""
        corrstr = ""

        if self.backend != arf['backend'] and self.backend is not None:
            # Change archive backend to EDD
            note += f"Overriding backend {arf['backend']} to '{self.backend}'. "
            warnings.warn(f"The backend of archive file {arf['inputfn']} is changed"
                          f" from {arf['backend']} to {self.backend}!")
            corrstr += f"be:name={self.backend}"

        # If no obsinfo is provided, try fetching it from the Effelsberg DB
        # SUPRESSED due to lack of EDD entries in the eff database.
        # obsinfo = self.get_obslog_entry(arf)

        # SUPRESSED due to lack of EDD entries in the eff database.
        # if any((self.fixcoords, arf['name'].endswith('_R'),
        #         arf['ra'].startswith('00:00:00'))):
        #     # Try fixing coordinates
        #     rastr, decstr = self._get_coordinates(obsinfo)
        #     corrstr += f",coord={rastr}{decstr}"
        # else:
        #     note += "No reasons to correct 'coord'. "

        # Try fixing object name, then, determine if the observation is a 
        # polcal diode scan, fluxcal, or a pulsar observation.

        # SUPRESSED due to lack of EDD entries in the eff database.
        # name = obsinfo['object']
        # if name != arf['name']:
        #     # Name in header is wrong. Correct it.
        #     corrstr += f",name={obsinfo['object']}"
        #     note += f"'name' changed from {arf['name']} to {name}. "

        # Needs to be changed when obslogs from effberg are availible!
        name = arf['name']

        if name.endswith("_R"):
            # Calibration diode was fired.
            # Observation could be pol-cal scan or flux-cal scan
            if any([name.startswith(fluxcal) for fluxcal
                    in utils.read_fluxcal_names(config.fluxcal_cfg)]):
                # Flux calibrator
                if name.endswith("_S_R") or name.endswith("_N_R"):
                    corrstr += ",type=FluxCal-Off"
                    note += "'type' set to FluxCal-Off. "
                elif name.endswith("_O_R"): 
                    corrstr += ",type=FluxCal-On"
                    note += "'type' set to FluxCal-On. "
            else:
                # Polarization calibrator
                corrstr += ",type=PolnCal"
                note += "'type' set to PolnCal. "
        else:
            # Observation is a pulsar
            corrstr += ",type=Pulsar"
            note += "'type' set to Pulsar. "

        # Fix negative bandwidth
        if arf['bw'] < 0:
            corrstr += f",bw={abs(arf['bw'])}"
            note += "'bw' normalized to a positive value. "

        return corrstr, note

    def get_correction_string(self, arfile):
        """Create a correction string to be passed to psredit.

        Args:
            arfile (str): Path to the archive file containinig the data to be
                corrected
            
        """

        note = ""

        # Load archive and determine reciever
        arf = utils.ArchiveFile(arfile)
        if self.rcvr is None:
            rcvr, rcvr_version = self._determine_rcvr(arf)
        elif self.rcvr.split("-")[0] in self.recievers.keys():
            rcvr, rcvr_version = rcvr.split("-")
        else:
            raise ValueError(f"Receiver provided ({self.rcvr}) is not recognized. "
                             f"Valid recievers are {self.recievers.keys()}")

        # Override archive reciever if it differs from the one found by the
        # determine_rcvr function or provided manually
        if arf['rcvr'] != rcvr+"-"+rcvr_version:
            note += f"Current archive receiver ({arf['rcvr']}) is wrong "+\
                    f"setting to '{rcvr}-{rcvr_version}'. "

        # Call the appropriate method to construct the corrstring.
        corrstr, corr_note = self.recievers[rcvr](self, arf)
        note += corr_note

        return corrstr, note

    def correct_header(self, infile, outdir=os.getcwd()):
        """Correct the header of an archive.


        Args:
            infile (str): Path to the input archive file.
            outdir (str): Path to the output directory. Defaults to the same
                directory as the input archive

        Returns:
            tuple: A tuple containing:
                corrfn (str): The name of the corrected file.
                corrstr (str): The parameter string of corrections to be used
                    with psredit.
                note (str): A note about header correction
        """

        outfile = os.path.join(outdir,
                               os.path.splitext(os.path.split(infile)[1])[0])\
                               + '.corr'
        # Build correction string based on input data (achive and obsinfo)
        corrstr, note = self.get_correction_string(infile)

        utils.execute(['psredit', '-O', outdir, '-e', 'corr', '-c',
                        corrstr, infile], stderr=os.devnull)

        return outfile, corrstr, note

    def get_obslog_entry(self, arf):
        """Given an archive file, find the entry in the observing log.

        Inputs:
            arf: ArchiveFile object.

        Output:
            obsinfo: A dictionary of observing information required for
            correcting archive headers.
        """

        # Try fetching obslogs from the Effelsberg database
        obsdt_utc, names = self._prep_obslog_search(arf)
        logentries = self._obslog_db_match(obsdt_utc, names)

        if len(logentries) == 0:
            # No entries can be matched with the effelsberg database
            msg = f"Error while correcting the header of archive "\
                  f"'{arf['inputfn']}'.\nSearching the Effelsberg database for "\
                  f"obslog entries close to the observation date "\
                  f"and start time (UTC) '{obsdt_utc.strftime('%c')}' for "\
                  f"object '{arf['name']}' yielded no entries"
            raise errors.HeaderCorrectionError(msg)
        elif len(logentries) != 1:
            # Can't conclusively determine the correct obslog 
            msg = f"Error while correcting the header of archive "\
                  f"'{arf['inputfn']}'.\nSearching the Effelsberg database for "\
                  f"obslog entries close to the observation date '{arf.fn}' "\
                  f"and start time (UTC) '{obsdt_utc.strftime('%c')}' for "\
                  f"object '{arf['name']}' yielded more than one "\
                  f"({len(logentries)}) entry."
            raise errors.HeaderCorrectionError(msg)

        utils.print_debug("Information from matching observing log"
                          "line:\n{:s}".format(pprint.pformat(logentries[0])),
                          'correct')

        return logentries[0]

    def _obslog_db_match(self, obsdt_utc, names):
        """Find entries in observing log database matching the given
        information.

        Args:
            obsdt_utc (datetime): The UTC datetime at the start of the
                observation
            names (list): List of object names to match

        Returns:
            list: Matching log entries.
        """

        # Connect to the Effberg database
        engine = database.get_engine('effelsberg')
        Session = sessionmaker(engine)
        # Query for entries within +- 1 day of observation start time
        start = obsdt_utc - timedelta(days=1)
        end = obsdt_utc + timedelta(days=1)
        query = select(ObsInfo.object, (ObsInfo.lst/3600).label('lststart'),
                       cast(ObsInfo.obstimestamp, DateTime).label('utcstart'),
                       ObsInfo.azim, ObsInfo.elev, ObsInfo.scan, ObsInfo.lon,
                       ObsInfo.lat)\
                       .where(and_(ObsInfo.object.in_(names),
                                   cast(ObsInfo.obstimestamp, DateTime)
                                   >= start,
                                   cast(ObsInfo.obstimestamp, DateTime)
                                   <= end))

        with Session() as session:
            obslogs = session.execute(query).all()

        utils.print_debug("Found {:d} matching obslog DB entries " 
                          "(name: {:s}; UTC: {:s})"
                          .format(len(obslogs), ", ".join(names),
                                  obsdt_utc.strftime("%c")), 'correct')

        logentries = []
        for log in obslogs:
            # Refine matching based on time. Only obslogs within 25 sec of the
            # start of the observations are to be considered
            utils.print_debug(f"{log}", 'correct')
            twentyfivesec = timedelta(seconds=25)
            logdt_utc = self.UTC_timezone.localize(log.utcstart)
            if (logdt_utc-twentyfivesec) <= obsdt_utc <= (logdt_utc+twentyfivesec):
                # Log is valid with constraints
                # Compute a few values to be consistent with obslog file parsing
                utc_hrs = log.utcstart.hour + (log.utcstart.minute +
                                              (log.utcstart.second +
                                               log.utcstart.microsecond*1e-6)
                                               / 60.0)/60.0
                logdt_local = logdt_utc.astimezone(self.timezone)
                localdate = logdt_local.date()

                # Extract the info required for corrections and save to a dict
                entry = dict(log._mapping)
                entry['scannum'] = str(log.scan)
                entry['utcstart'] = utc_hrs
                entry['utc'] = log.utcstart.strftime('%c')
                entry['localdate'] = localdate
                entry['catalog_rastr'] = \
                    rs.utils.deg_to_hmsstr(log.lon, decpnts=3,
                                           style='units')[0]
                entry['catalog_decstr'] = \
                    rs.utils.deg_to_dmsstr(log.lat, decpnts=3,
                                           style='units')[0]

                logentries.append(entry)

        return logentries

    def _prep_obslog_search(self, arf):
        """Extract information from an ArchiveFile that is nessecary for
        finding the related obslog in the Effelsberg database

        Args:
            arf (ArchiveFile): ArchiveFile object.

        Returns:
            tuple,  A tuple containing:
                - obsdt_utc (datetime): The UTC datetime at the start of
                    the observation
                - names (list): Object names to match
        """

        # Determine the object names to be searched for in the database
        # Be sure to use the original name recorded in the header
        names = [arf['origname'],]
        utils.print_debug("Will check for the following name for obs-log "
                          "matching: {:s}".format(", ".join(names)), 'correct')

        # Get date of observation
        obsdt_utc = rs.utils.mjd_to_datetime(arf['mjd'])
        obsdt_utc = self.UTC_timezone.localize(obsdt_utc)

        return obsdt_utc, names

    def _check_obsinfo_validity(self):
        """Check if the manually provided obsinfo contains the nessecary
        information to perform corrections.
        Throws warnings is the provided obsinfo is incomplete"""

        required_fields = ["object", "azim", "elev", "lststart"]
        for key in required_fields:
            if key not in self.obsinfo:
                raise KeyError(f"The key '{key}' is not in the ")
            
    def _determine_rcvr(self, arf, reset_weights=False):
        """Given an ArchiveFile object determine the name
        and version of the Effelsberg receiver used to gather the data

        Args:
            arf (ArchiveFile): An ArchiveFile object representing the archive.
            reset_weights (bool): Reset weights to uniform. Defaults to False
            (use current weights).

        Returns:
            str: The name of the receiver.
        """

        if arf['band'] in list(self.band_reciever_map.keys()):
            rcvr, version = self.band_reciever_map[arf['band']] 
        else:
            raise errors.HeaderCorrectionError("No methods availible to "
                                            "correct headers for {:s} "
                                            "observations."
                                            .format(arf['band']))
        return rcvr, version
