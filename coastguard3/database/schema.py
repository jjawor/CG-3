from sqlalchemy import Integer, String, Enum, DateTime, UniqueConstraint,\
                       ForeignKey, Float, Boolean, Text, select
from sqlalchemy.orm import DeclarativeBase, relationship, mapped_column,\
    validates
from sqlalchemy.sql import func


class Base(DeclarativeBase):
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


DIRECTORY_STATUSES = ['new', 'submitted', 'failed', 'running', 'processed',
                      'archived', 'empty']
FILE_STATUSES = ['new', 'submitted', 'failed', 'running', 'processed', 'done',
                 'calfail', 'replaced', 'awaiting_qctrl']
FILE_STAGES = ['grouped', 'combined', 'corrected', 'cleaned', 'calibrated']
OBSTYPES = ['pulsar', 'cal']
OBSBANDS = ['Pband', 'Lband', 'Sband', 'Cband', 'Xband', 'Kband']
CALDB_STATUSES = ['ready', 'submitted', 'updating', 'failed']
QCTRL_STATUSES = ['good', 'weak', 'bad_ephem', 'RFI', 'no_detect']

class Version(Base):
    __tablename__ = 'versions'
    version_id = mapped_column(Integer, primary_key=True, autoincrement=True,
                               nullable=False)
    cg_githash = mapped_column(String(64), nullable=False)
    psrchive_githash = mapped_column(String(64), nullable=False)
    __table_args__ = (UniqueConstraint('cg_githash', 'psrchive_githash'),)


class Directory(Base):
    __tablename__ = 'directories'
    dir_id = mapped_column(Integer, primary_key=True, autoincrement=True,
                           nullable=False)
    path = mapped_column(String(200), nullable=False, unique=True)
    status = mapped_column(Enum(*DIRECTORY_STATUSES,
                                name="directory_statuses"),
                           nullable=False, default='new')
    backend = mapped_column(String(32), nullable=False)
    observatory = mapped_column(String(32), nullable=False)
    note = mapped_column(Text, nullable=True)
    added = mapped_column(DateTime, nullable=False, default=func.now())
    last_modified = mapped_column(DateTime, nullable=False, default=func.now())


def setdef_backend(context):
    """Set default backend of an Observation entry to be the same as the
    related Directory entry
    """

    id = context.get_current_parameters()["dir_id"]
    backend = context.root_connection.execute(select(Directory.backend)
                                              .where(Directory.dir_id == id))\
                                             .one()[0]
    if backend:
        return backend
    else:
        raise ValueError("Failed autosetting of backend for observation. "
                         "Backend must be a string with length 32 or less")


def setdef_observatory(context):
    """Set default observatory of an Observation entry to be the same as the
    related Directory entry
    """

    id = context.get_current_parameters()["dir_id"]
    observatory = context.root_connection.execute(select(Directory.observatory)
                                              .where(Directory.dir_id == id))\
                                             .one()[0]
    if observatory:
        return observatory
    else:
        raise ValueError("Failed autosetting of observatory for observation."
                         " Observatory must be a string with length 32 or"
                         " less")


class Observation(Base):
    __tablename__ = 'observations'
    obs_id = mapped_column(Integer, primary_key=True, autoincrement=True,
                           nullable=False)
    dir_id = mapped_column(Integer, ForeignKey("directories.dir_id"),
                           nullable=False)
    sourcename = mapped_column(String(32), nullable=False)
    obstype = mapped_column(Enum(*OBSTYPES, name="obstypes"), nullable=False)
    start_mjd = mapped_column(Float(53), nullable=False)
    length = mapped_column(Float, nullable=True)
    bw = mapped_column(Float, nullable=True)
    freq = mapped_column(Float, nullable=True)
    rcvr = mapped_column(String(32), nullable=True, default=None)
    nsubints = mapped_column(Integer, nullable=True, default=None)
    nsubbands = mapped_column(Integer, nullable=True, default=None)
    obsband = mapped_column(Enum(*OBSBANDS, name='obsbands'), nullable=True,
                            default=None)
    nonstd_setup = mapped_column(Boolean, nullable=True, default=None)
    added = mapped_column(DateTime, nullable=False, default=func.now())
    last_modified = mapped_column(DateTime, nullable=False, default=func.now())
    backend = mapped_column(String(32), nullable=False, default=setdef_backend)
    observatory = mapped_column(String(32), nullable=True,
                                default=setdef_observatory)
    directory = relationship("Directory", backref="observations")


class File(Base):
    __tablename__ = 'files'
    file_id = mapped_column(Integer, primary_key=True, autoincrement=True,
                            nullable=False)
    obs_id = mapped_column(Integer, ForeignKey("observations.obs_id"),
                           nullable=False)
    parent_file_id = mapped_column(Integer, ForeignKey("files.file_id"),
                                   nullable=True)
    cal_file_id = mapped_column(Integer, ForeignKey("files.file_id"),
                                nullable=True)
    version_id = mapped_column(Integer, ForeignKey("versions.version_id"),
                               nullable=False)
    filepath = mapped_column(String(200), nullable=False)
    filename = mapped_column(String(200), nullable=False)
    note = mapped_column(Text, nullable=True)
    stage = mapped_column(Enum(*FILE_STAGES, name="file_stages"),
                          nullable=False)
    status = mapped_column(Enum(*FILE_STATUSES, name="file_statuses"),
                           nullable=False, default='new')
    md5sum = mapped_column(String(64), nullable=False, unique=True)
    ephem_md5sum = mapped_column(String(64), nullable=True, default=None)
    coords = mapped_column(String(32), nullable=False)
    filesize = mapped_column(Integer, nullable=False)
    is_deleted = mapped_column(Boolean, nullable=False, default=False)
    snr = mapped_column(Float, nullable=True, default=None)
    maskfrac = mapped_column(Float, nullable=True, default=None)
    added = mapped_column(DateTime, nullable=False, default=func.now())
    last_modified = mapped_column(DateTime, nullable=False, default=func.now())
    __table_args__ = (UniqueConstraint('filepath', 'filename'),)
    observation = relationship("Observation", backref="files",
                               foreign_keys=[obs_id])
    parent_file = relationship("File", remote_side=[file_id],
                               foreign_keys=[parent_file_id],
                               backref="child_files")
    cal_file = relationship("File", remote_side=[file_id],
                            foreign_keys=[cal_file_id],
                            backref="calibrated_files")
    qctrl = relationship("Qctrl", back_populates="file", uselist=False)

class Diagnostic(Base):
    __tablename__ = 'diagnostics'
    diagnostic_id = mapped_column(Integer, primary_key=True,
                                  autoincrement=True, nullable=False)
    file_id = mapped_column(Integer, ForeignKey("files.file_id"),
                            name="fk_diag_file")
    diagnosticpath = mapped_column(String(300), nullable=False)
    diagnosticname = mapped_column(String(300), nullable=False)
    added = mapped_column(DateTime, nullable=False, default=func.now())
    last_modified = mapped_column(DateTime, nullable=False, default=func.now())
    __table_args__ = (UniqueConstraint('diagnosticpath', 'diagnosticname'),
                      {'mysql_engine': 'InnoDB', 'mysql_charset': 'ascii'})


class Log(Base):
    __tablename__ = 'logs'
    log_id = mapped_column(Integer, primary_key=True, autoincrement=True,
                           nullable=False)
    obs_id = mapped_column(Integer, ForeignKey("observations.obs_id"),
                           name="fk_log_obs")
    logpath = mapped_column(String(300), nullable=False)
    logname = mapped_column(String(300), nullable=False)
    added = mapped_column(DateTime, nullable=False, default=func.now())
    last_modified = mapped_column(DateTime, nullable=False, default=func.now())


class Caldb(Base):
    __tablename__ = 'caldbs'
    caldb_id = mapped_column(Integer, primary_key=True, autoincrement=True,
                             nullable=False)
    caldbpath = mapped_column(String(300), nullable=False)
    caldbname = mapped_column(String(300), nullable=False)
    sourcename = mapped_column(String(32), nullable=False, unique=True)
    status = mapped_column(Enum(*CALDB_STATUSES, name='caldb_statuses'),
                           nullable=False, default='ready')
    numentries = mapped_column(Integer, nullable=False, default=0)
    note = mapped_column(Text, nullable=True)
    added = mapped_column(DateTime, nullable=False, default=func.now())
    last_modified = mapped_column(DateTime, nullable=False, default=func.now())


class Qctrl(Base):
    __tablename__ = 'qctrl'
    qctrl_id = mapped_column(Integer, primary_key=True, autoincrement=True,
                             nullable=False)
    file_id = mapped_column(Integer, ForeignKey("files.file_id"),
                            name="fk_qc_file")
    user = mapped_column(String(32), nullable=False)
    qcpassed = mapped_column(Boolean, nullable=True, default=None)
    status = mapped_column(Enum(*QCTRL_STATUSES, name='qctrl_status'),
                                nullable=False,)
    note = mapped_column(Text, nullable=True)
    added = mapped_column(DateTime, nullable=False, default=func.now())
    last_modified = mapped_column(DateTime, nullable=False, default=func.now())
    file = relationship("File", back_populates="qctrl", uselist=False)
    __table_args__ = (UniqueConstraint('fk_qc_file'),)