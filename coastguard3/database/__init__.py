
import os
import shutil
from sqlalchemy import select, update, create_engine, text
from sqlalchemy.orm import joinedload
from datetime import datetime
from urllib.parse import quote

from coastguard3 import config, errors, utils
from coastguard3.database.schema import File


db_host = '127.0.0.1'
db_port = 3307
db_username = 'effdb_user'
db_password = 'db@effberg'
encoded_password = quote(db_password, safe='')
db_name = 'effdb'


def get_engine(url='coastguard'):
    """
    Given a DB URL string return the corresponding DB engine.

        Input:
            url: A DB URL string.

        Output:
            engine: The corresponding DB engine.
    """
    if url == 'effelsberg':
        # constring = f'mysql://{db_username}:{encoded_password}@localhost:3307/{db_name}'
        constring = f'mysql://{db_username}:{encoded_password}@{db_host}:'\
            f'{db_port}/{db_name}'
        engine = create_engine(constring, echo=False)
        # with engine.connect() as connection:
            # result = connection.execute(text('SELECT * FROM obsinfo limit 1'))
            # print(result.scalars().all())  # Print the result
        return engine
    elif url == 'coastguard':
        constring = config.dburl
        return create_engine(constring, echo=False, pool_recycle=3600, pool_pre_ping=True)
    else:
        raise errors.DatabaseError("Requested database {:s} is not recognised"
                                   .format(url))


def move_file(Session, file_id, destdir, destfn=None):
    """Given a file ID move the associated archive.

        Inputs:
            db: Database object to use.
            file_id: The ID of a row in the files table.
            destdir: The destination directory.
            destfn: The destination file name.
                (Default: Keep old file name).

        Outputs:
            None
    """

    with Session() as session:
        files = session.scalars(select(File).where(File.file_id == file_id))\
            .all()
        if len(files) != 1:
            raise errors.DatabaseError(f"Bad number of rows ({len(files)}) "
                                       f"with file_id={file_id}!")
        file = files[0]

        if destfn is None:
            destfn = file.filename
        # Copy file
        src = os.path.join(file.filepath, file.filename)
        dest = os.path.join(destdir, destfn)
        utils.print_info(f"Moving archive file from {src} to {dest}.", 2)
        if src == dest:
            utils.print_info(f"File is already at its destination ({dest}). "
                             "No need to move.", 2)
        else:
            try:
                os.makedirs(destdir)
            except OSError:
                # Directory already exists
                pass
            shutil.copy(src, dest)

            # Update database
            file_update = update(File).where(File.file_id == file_id)\
                .values(filepath=destdir, filename=destfn,
                        last_modified=datetime.now())
            session.execute(file_update)
            session.commit()

            # Remove original
            os.remove(src)
            utils.print_info(f"Moved archive file from {src} to {dest}. The "
                             f"database has been updated accordingly.", 2)


def get_obs_files(Session, obs_id):
    """Given a observation ID return the corresponding entries
        in the files table and the observation table.

    Args:
        Session (sessionmaker). A sqlalchemy sessionmaker object bound
            to a database.
        obs_id: An observation ID.

    Returns:
        list: List File objects representing the queried files associated
            with the chosen observation. The File object is configured to
            earger load its corresponding observation so that columns from
            the observattions table can be accessed via
            File.observaton.column_name
    """

    with Session() as session:
        query = select(File).where(File.obs_id == obs_id)\
            .options(joinedload(File.observation))
        files = session.scalars(query).all()
    return files


def get_parent(Session, file_id):
    """Find the parent of a file.

    Args:
        Session (sessionmaker). A sqlalchemy sessionmaker object bound
            to a database.
        file_id (int): The ID of the child file

    Raises:
        errors.DatabaseError: If there are more then 1 parent of the child

    Returns:
        File: A File object representing an entry in the files table in the DB
    """

    with Session() as session:
        parents = session.scalars(File).where(File.file_id == file_id).all()

        if len(parents) == 1:
            if parents[0].parent_file_id is not None:
                parent = parents[0]
            else:
                parent = None
        else:
            raise errors.DatabaseError(f"Bad number of files ({len(parents)}) "
                                       f"with ID={file_id}!")
    return parent


def get_all_ancestors(Session, file_id):
    """Generate a list of all ancestors of a file from the DB

    Args:
        Session (sessionmaker). A sqlalchemy sessionmaker object bound
            to a database.
        file_id (int): The ID of the descendant file

    Returns:
        list: A list of File objects. Each File object represents an entry in
        the files table in the DB
    """

    ancestors = []
    parent = get_parent(file_id)
    if parent:
        ancestors.append(parent)
        ancestors.extend(get_all_ancestors(parent.file_id, Session))

    return ancestors


# def fancy_getitem(self, key):
#     filterfunc = null
#     if (type(key) in (type('str'), type('str'))) and key.endswith("_L"):
#         filterfunc = string.lower
#         key = key[:-2]
#     elif (type(key) in (type('str'), type('str'))) and key.endswith("_U"):
#         filterfunc = string.upper
#         key = key[:-2]
#     elif (type(key) in (type('str'), type('str'))) and toround_re.search(key):
#         head, sep, tail = key.rpartition('_R')
#         digits = int(tail) if tail else 0
#         filterfunc = lambda x: round(x, digits)
#         key = head
#     elif (type(key) in (type('str'), type('str'))) and key.startswith("date:"):
#         fmt = key[5:]
#         key = 'start_mjd'
#         filterfunc = lambda mjd: utils.mjd_to_datetime(mjd).strftime(fmt)
#     elif (type(key) in (type('str'), type('str'))) and (key == "secs"):
#         key = 'start_mjd'
#         filterfunc = lambda mjd: int((mjd % 1)*24*3600+0.5)
#     if key in self:
#         return filterfunc(super(self.__class__, self).__getitem__(key))
#     else: 
#         matches = [k for k in list(self.keys()) if k.startswith(key)]
#         if len(matches) == 1:
#             return filterfunc(super(self.__class__, self).__getitem__(matches[0]))
#         elif len(matches) > 1:
#             raise errors.BadColumnNameError("The column abbreviation "
#                                             "'%s' is ambiguous. "
#                                             "('%s' all match)" %
#                                             (key, "', '".join(matches)))
#         else:
#             raise errors.BadColumnNameError("The column '%s' doesn't exist! "
#                                             "(Valid column names: '%s')" %
#                                             (key, "', '".join(sorted(self.keys()))))


# # sa.engine.row.RowProxy.__getitem__ = fancy_getitem
    

# def before_cursor_execute(conn, cursor, statement, parameters, \
#                             context, executemany):
#     """
#     An event to be executed before execution of SQL queries.

#         See SQLAlchemy for details about event triggers.
#     """
#     # Step back 7 levels through the call stack to find
#     # the function that called 'execute'
#     msg = str(statement)
#     if executemany and len(parameters) > 1:
#         msg += "\n    Executing %d statements" % len(parameters)
#     elif parameters:
#         msg += "\n    Params: %s" % str(parameters)
#     utils.print_debug(msg, "queries", stepsback=6)


# def on_commit(conn):
#     """
#     An event to be executed when a transaction is committed.

#         See SQLAlchemy for details about event triggers.
#     """
#     utils.print_debug("Committing database transaction.", 'database', \
#                         stepsback=7)


# def on_rollback(conn):
#     """
#     An event to be executed when a transaction is rolled back.
        
#         See SQLAlchemy for details about event triggers.
#     """
#     utils.print_debug("Rolling back database transaction.", 'database', \
#                         stepsback=7)
        

# def on_begin(conn):
#     """
#     An event to be executed when a transaction is opened.
        
#         See SQLAlchemy for details about event triggers.
#     """
#     utils.print_debug("Opening database transaction.", 'database', \
#                         stepsback=7)


# def on_sqlite_connect(dbapi_conn, conn_rec):
#     """
#     An even to be execute when sqlite connections
#         are established. This turns on foreign key support.

#         See SQLAlchemy for details about activating SQLite's
#         foreign key support:
#         http://docs.sqlalchemy.org/en/rel_0_7/dialects/sqlite.html#foreign-key-support
    
#         Inputs:
#             dbapi_conn: A newly connected raw DB-API connection 
#                 (not a SQLAlchemy 'Connection' wrapper).
#             conn_rec: The '_ConnectionRecord' that persistently 
#                 manages the connection.

#         Outputs:
#             None
#     """
#     cursor = dbapi_conn.cursor()
#     cursor.execute("PRAGMA foreign_keys=ON")
#     cursor.close()


# def get_engine(url):
#     """
#     Given a DB URL string return the corresponding DB engine.

#         Input:
#             url: A DB URL string.

#         Output:
#             engine: The corresponding DB engine.
#     """
#     # Create the database engine
# #    engine = sa.create_engine(url, connect_args=ssl_args, echo=False)

#     engine = sa.create_engine(url, pool_recycle=1) 
#     if engine.name == 'sqlite':
#         sa.event.listen(engine, "connect", on_sqlite_connect)
#     sa.event.listen(engine, "before_cursor_execute",
#                         before_cursor_execute)
#     if config.debug.is_on('database'):
#         sa.event.listen(engine, "commit", on_commit)
#         sa.event.listen(engine, "rollback", on_rollback)
#         sa.event.listen(engine, "begin", on_begin)
#     return engine


# class Database(object):
#     def __init__(self, db='effreduce'):
#         """
#         Set up a Database object using SQLAlchemy.

#             Inputs:
#                 db: The name of the database to connect to.
#                     Options are 'effreduce' and 'obslog'
#                     (Default: effreduce)
#         """ 
#         if db == 'effreduce':
#             url = config.dburl
#             self.metadata = schema.CustomBase.metadata
#         elif db == 'obslog':
#             url = config.obslog_dburl
#             self.metadata = obslog.metadata 
#         else:
#             raise errors.DatabaseError("Database (%s) is not recognized. "
#                                        "Cannot connect." % db)
#         self.engine = get_engine(url) 
#         if not self.is_created():
#             raise errors.DatabaseError("The database (%s) does not appear " \
#                                     "to have any tables. Be sure to run " \
#                                     "'create_tables.py' before attempting " \
#                                     "to connect to the database." % \
#                                             self.engine.url.database)

#         # The database description (in metadata)
#         self.tables = self.metadata.tables

#     def get_table(self, tablename):
#         return self.tables[tablename]

#     def __getitem__(self, key):
#         return self.get_table(key)

#     def __getattr__(self, key):
#         return self.get_table(key)

#     def is_created(self):
#         """
#         Return True if the database appears to be setup
#             (i.e. it has tables).

#             Inputs:
#                 None

#             Output:
#                 is_setup: True if the database is set up, False otherwise.
#         """ 
#         Inspector = inspect(self.engine)
#         table_names = Inspector.get_table_names()
# #        print "is_created", bool(table_names)
#         return bool(table_names)

#     def transaction(self, *args, **kwargs):
#         """
#         Return a context manager delivering a 'Connection'
#             with a 'Transaction' established. This is done by
#             calling the 'begin' method of 'self.engine'.

#             See http://docs.sqlalchemy.org/en/rel_0_7/core/connections.html
#                         #sqlalchemy.engine.base.Engine.begin

#             Inputs:
#                 Arguments are passed directly to 'self.engine.begin(...)'

#             Output:
#                 context: The context manager returned by 
#                     'self.engine.begin(...)'
#         """
# #        print self.args, self.kwargs
#         return self.engine.begin(*args, **kwargs)

#     @staticmethod
#     def select(*args, **kwargs):
#         """
#         A staticmethod for returning a select object.

#             Inputs:
#                 ** All arguments are directly passed to 
#                     'sqlalchemy.sql.select'.

#             Outputs:
#                 select: The select object returned by \
#                     'sqlalchemy.sql.select'.
#         """ 
#         return sa.sql.select(*args, **kwargs)

