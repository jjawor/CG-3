from pyriseset3.actions import ParseDate
from pyriseset3.utils import date_to_mjd

from coastguard3 import utils, errors
from coastguard3.pipes.loaders import AsterixLoader, EDDLoader, EDD_oldLoader
from coastguard3.pipes.combiners import EDDCombiner, AsterixCombiner
from coastguard3.pipes.correcters import EDDCorrecter, AsterixCorrecter
from coastguard3.pipes.cleaners import ReceiverBandCleaner, SurgicalScrubCleaner
from coastguard3.pipes.calibrators import EffelsbergCalibrator
from coastguard3.managers.calibrators import GenerericCalibratorManager
from coastguard3.managers.cleaners import GenericCleanerManager
from coastguard3.managers.combiners import GenericCombinerManager
from coastguard3.managers.correcters import GenericCorrecterManager
from coastguard3.managers.loaders import GenericLoaderManager

def parse_dates(start_date=None, end_date=None):
    if start_date:
        start_day, start_month, start_year = float(args.start_date.day),\
            int(args.start_date.month), int(args.start_date.year)
        start_mjd = date_to_mjd(start_year, start_month, start_day)
    else:
        start_mjd = None

    if end_date:
        end_day, end_month, end_year = float(args.end_date.day),\
            int(args.end_date.month), int(args.end_date.year)
        end_mjd = date_to_mjd(end_year, end_month, end_day)
    else:
        end_mjd = None

    return start_mjd, end_mjd

def main(args):
    # Parse dates
    start_mjd, end_mjd = parse_dates(args.start_date, args.end_date)

    if not args.no_rawdata_load:
        # %% Set up loaders and load new rawdata into the DB
        ast_load = GenericLoaderManager(AsterixLoader())
        EDD_load = GenericLoaderManager(EDDLoader())
        ast_load.load(force=args.reattempt_dirs)
        EDD_load.load(force=args.reattempt_dirs)
        EDD_load_old = GenericLoaderManager(EDD_oldLoader())
        EDD_load_old.load(force=args.reattempt_dirs)
        
    # %% Set up the other steps
    combiners, correcters, cleaners, calibrators = [], [], [], []
    for target in args.targets:
        if "Asterix" in args.backend:
            combiners.append(GenericCombinerManager(AsterixCombiner(), ['Asterix'],
                                                    ['Effelsberg'], target,start_mjd, end_mjd))
            correcters.append(GenericCombinerManager(AsterixCorrecter(), ['Asterix'],
                                        ['Effelsberg'], target,
                                        start_mjd, end_mjd))
        if "EDD" in args.backend:                        
            combiners.append(GenericCombinerManager(EDDCombiner(), ['EDD'],
                                                    ['Effelsberg'], target))
            correcters.append(GenericCorrecterManager(EDDCorrecter(), ['EDD'],
                                                ['Effelsberg'], target))

        cleaners.append(GenericCleanerManager([ReceiverBandCleaner,
                                               SurgicalScrubCleaner],
                                               args.backend,
                                               ['Effelsberg'], target))
        calibrators.append(GenerericCalibratorManager(EffelsbergCalibrator(),
                                                      args.backend,
                                                      ['Effelsberg'], target))

    # # %% Run steps in sequence
    for combiner in combiners:
        try:
            combiner.combine()
        except errors.QueryError:
            # No targets to combine, skip to next target
            pass
    for correcter in correcters:
        try:
            correcter.correct()
        except errors.QueryError:
            # No targets to combine, skip to next target
            pass
    for cleaner in cleaners:
        try:
            cleaner.clean()
        except errors.QueryError:
            # No targets to combine, skip to next target
            pass
    for calibrator in calibrators:
        try:
            calibrator.calibrate()
        except errors.QueryError:
            # No targets to combine, skip to next target
            pass

if __name__ == '__main__':
    parser = utils.DefaultArguments(description="Automated reduction of data.")
    parser.add_argument("-t", "--targets", nargs='+', default=[],
                        help="A list of targets to reduce.")
    parser.add_argument("--reattempt-dirs", dest="reattempt_dirs",
                        action="store_true",
                        help="Try to reload all directories regardless of "
                             "modification time. Exisiting DB entries will "
                             "not be modified or duplicated. (Default: "
                             "only load recently modified directories.)")
    parser.add_argument("-b", "--backend", nargs='+',
                        default=["EDD", "Asterix"],
                        help="Which backends to reduce data for. (Default: "
                        "reduce both EDD and Asterix data).")
    parser.add_argument("--no-rawdata-load", action="store_true", default=False,
                        dest='no_rawdata_load',
                        help="Do not try to load new rawdata (Default: False)")
    parser.add_argument('--end-date', type=str, default=None,
                    action=ParseDate, dest='end_date',
                    help="Start date (in YYYY-MM-DD format). Only observations"
                         " performed after this date will be processed "
                         "(Default: None. Process from the earliest availible "
                         "observation)")
    parser.add_argument('--start-date', type=str, default=None,
                    action=ParseDate, dest='start_date',
                    help="End date (in YYYY-MM-DD format). Only observations"
                         " performed before this date will be processed "
                         "(Default: None. Process until the latest availible "
                         "observation)")
    args = parser.parse_args()

    main(args)