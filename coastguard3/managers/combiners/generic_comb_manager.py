from coastguard3 import config, errors, utils, debug, diagnose
from coastguard3.database.schema import Observation, File, Diagnostic
from coastguard3.managers import BaseManager

import warnings
import shutil
import hashlib
import tempfile
import os
from tqdm import tqdm
from datetime import datetime
from sqlalchemy import select, update, and_, insert
from sqlalchemy.exc import OperationalError

class GenericCombinerManager(BaseManager):
    """The generic implementation of CombinerManagers. Handles loading and DB
    transations in pipeline mode.
    """
    in_stage = "grouped"
    out_stage = "combined"

    def __init__(self, combiner, backends: list, observatories: list,
                 source: str = None, start_mjd: str=None, end_mjd:str=None,
                 engine=None):
        super().__init__(backends, observatories, source, start_mjd, end_mjd,
                         engine)
        self.combiner = combiner

    def load_combined_file(self, file):
        """Create a combined archive and load it into the database.

            Args:
                file (File): A File object representing a DB file table entry.

            Returns:
                file_id: The ID of newly created combined file.
        """

        # Setup a logger to collect relevant output
        self._setup_logger(file.obs_id)
        self._mark_as_running(file.file_id)  # Mark as running

        # Get the path of the groupfile
        groupfile = os.path.join(file.filepath, file.filename)

        try:
            rawdir, rawints = self.combiner.read_groupfile(groupfile)

            try:
                # Create a root temporary directory where all temp files will
                # be stored
                basetempdir = tempfile.mkdtemp(suffix="_combine",
                                               dir=config.tmp_directory)
                # Prepare raw data for combination
                tempdir, tempints, parfile, outdir = \
                    self.combiner.prepare_subints(rawdir, rawints, basetempdir)

                # Combine the prepared data
                cmbfile = self.combiner.combine_subints(tempdir, tempints,
                                                        parfile, outdir)
                if debug.is_on('reduce'):
                    warnings.warn("Not cleaning up temporary directory ({:s})"
                                  .format(basetempdir))
                else:
                    utils.print_info("Removing temporary directory ({:s})"
                                     .format(basetempdir), 2)
                    shutil.rmtree(basetempdir)
            except:
                raise  # Re-raise the exception

            # Make diagnostic plots
            # Temporarily disabled due to RAM limitations
            cmbarf = utils.ArchiveFile(cmbfile)
            fullresfn, lowresfn = diagnose.make_summary_plots(cmbarf)

            values = {'filepath': os.path.dirname(cmbfile),
                      'filename': os.path.basename(cmbfile),
                      'stage': 'combined',
                      'md5sum': utils.get_md5sum(cmbfile),
                      'filesize': os.path.getsize(cmbfile),
                      'parent_file_id': file.file_id,
                      'obs_id': file.obs_id,
                      'coords': cmbarf['coords'],
                      'snr': cmbarf['snr']}

            try:
                ephem = utils.extract_parfile(cmbfile)
                values['ephem_md5sum'] = \
                    hashlib.md5(ephem.encode()).hexdigest()
            except errors.InputError as exc:
                # Warn about a lack of an ephemeris
                warnings.warn(exc.get_message(), errors.CoastGuardWarning)

            diagvals = [{'diagnosticpath': os.path.dirname(fullresfn),
                        'diagnosticname': os.path.basename(fullresfn)},
                        {'diagnosticpath': os.path.dirname(lowresfn),
                        'diagnosticname': os.path.basename(lowresfn)}
                        ]

        except Exception as exc:
            # If an exception occurs, log info and update DB, marking the
            # directory as failed
            exc_note = "Combining failed!"
            self._step_failed(file, exc, exc_note)
            raise

        else:
            # If no exception is raised, update the DB
            version_id = utils.get_version_id()
            try:
                with self.Session() as session:
                    # Insert new file
                    values['version_id'] = version_id
                    new_file_insert = insert(File).values(values)
                    new_file_id = session.execute(new_file_insert)\
                        .inserted_primary_key[0]

                    # Insert new diagnostics
                    for diagval in diagvals:
                        diagval['file_id'] = new_file_id
                        new_diag = insert(Diagnostic).values(diagval)
                        session.execute(new_diag)

                    # Update status of partent file
                    file_update = (
                        update(File)
                        .where(File.file_id == file.file_id)
                        .values(status='processed', last_modified=datetime.now()))
                    session.execute(file_update)

                    # Update observation
                    obs_update = (
                        update(Observation)
                        .where(Observation.obs_id == file.obs_id)
                        .values(length=cmbarf['length'], bw=abs(cmbarf['bw']),
                                current_file_id=new_file_id,
                                last_modified=datetime.now())
                    )
                    session.execute(obs_update)
                    session.commit()
            except Exception as exc:
                # Something went wrong when trying to update the DB
                exc_note = "Combining failed do to a database error when "\
                           "insertig new row"
                utils.print_info(f"An error occurred while trying to insert a "
                                 f"new row: {str(exc)}")
                self._step_failed(file, exc, exc_note)
                raise

        return new_file_id

    def combine(self):
        """Entrypoint for pipeline mode.
        Query the DB for observations that need combining, finding the
        appropriate groupfiles, reading them, and combining the subints listed
        therein into a single archive.
        Afterwards, update the DB with the new combined file.
        """

        with utils.std_out_err_redirect_tqdm() as orig_stdout:
            for file in (files_to_combine :=
                         tqdm(self.get_toprocess(), file=orig_stdout)):
                files_to_combine.set_description("Combining {:s}"
                                                 .format(file.filename))
                try:
                    self.load_combined_file(file)
                except KeyboardInterrupt:
                    # On user interrupt, release current running dir and reset
                    # queried dirs
                    self._change_file_status([file], from_stat=['running'])
                    self._change_file_status(files_to_combine)
                    raise
                except Exception:
                    # Release the unprocessed files
                    self._change_file_status(files_to_combine)
                    raise
