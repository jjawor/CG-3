import time
import os
import tempfile
import shutil
import hashlib
import warnings
import traceback
import re
from tqdm import tqdm
from datetime import datetime
from sqlalchemy import select, insert, update, and_
from coastguard3.database.schema import Directory, File, Log, Observation,\
    OBSTYPES
from coastguard3 import config, errors, utils, logger
from coastguard3.managers import BaseManager


class GenericLoaderManager(BaseManager):
    """The generic implementation of LoaderManagers. Handles loading and DB
    transations in pipeline mode.
    This is a two stage process. First, directories are found and inserted into
    the DB. Then, groupfiles are written for each directory. The groupfiles
    specify which files in the directory are to be combined in the next step.
    """
    in_stage = "None"
    out_stage = "grouped"

    def __init__(self, loader, engine=None):
        self.engine = self._get_engine(engine)
        self.loader = loader

    def load_rawdata_dirs(self, force=False):
        """Search for directories containing raw data.
        For each newly found directory, insert a row in the database
        directories table.

        Args:
            force (bool, optional): Attempt to load all directories regardless
                of modification times. Defaults to False: Only load the
                directories with modification times after the latest
                already existing entry.

        Returns:
            int: number of inserted rows
        """

        # Get add-time of most recently added directory DB entry
        # Only entries more recent that this one will be loaded into the DB by
        # default
        with self.Session() as session:
            row = session.scalars(select(Directory.added)
                                  .order_by(Directory.added).limit(1)
                                  ).one_or_none()
        if row is None:
            most_recent_addtime = 0
        else:
            most_recent_addtime = time.mktime(row.timetuple())

        ninserts = 0

        # Fetch list of directories from the loader pipe
        dirs = self.loader.get_rawdata_dirs()
        for ii, path in enumerate(dirs):
            if force or (os.path.getmtime(path) > most_recent_addtime):
                # Only try to add new entries unless Force is True
                with self.Session() as session:
                    try:
                        new_dir = insert(Directory)\
                            .values(path=path, backend=self.loader.backend, 
                                    observatory=self.loader.observatory)
                        session.execute(new_dir)
                        session.commit()
                        # 'directories.path' is constrained to be unique, so
                        # trying to insert a directory that already exists
                        # will result in an error, which will be automatically
                        # rolled back by the context manager (i.e. no new
                        # database entry will be inserted)
                    except Exception:
                        pass  # This has to be reworked. catching Exeption and
                        # passing is not good practise
                    else:
                        # If no exceptions were raised
                        ninserts += 1

        return ninserts

    def get_togroup(self):
        """Query DB for a list of directories that need to have groupfiles
        written for them. Update their status to 'submitted'

        Returns:
            list: A list of directory objects that represent the queried
            DB entries
        """

        # Create select query, selecting new directories with data to combine
        select_query = select(Directory)\
            .where(Directory.status == "new",
                   Directory.backend == self.loader.backend,
                   Directory.observatory == self.loader.observatory)\
            .with_for_update()

        # Query DB
        with self.Session(expire_on_commit=False) as session:
            dirs = session.scalars(select_query).all()
            if not dirs:
                warnings.warn("No new directories.",
                              errors.CoastGuardWarning)
            for dir in dirs:
                dir.status = "submitted"
            session.commit()

        utils.print_info(f"Got {len(dirs)} rows to be grouped")
        return dirs

    def load_groups(self, dir):
        """Create a groupfile from the data stored in a directory
        and load it into the database.

        Args:
            dir(Directory): A Directory object representing a directories
            table entry.

        Returns:
            int: The number of group rows inserted.
        """

        # Initialize a logger in a temp file
        _, tmplogfn = self._create_temp_logger()

        # Mark the dirs as running in the DB
        self._mark_dir_as_running(dir)

        try:
            # Find files that make up groups and create groupfiles
            ninserts = 0
            values = []
            obsinfo = []
            logfns = []

            groups =  self.loader.make_groups(dir.path)
            if groups:
                for subdirs, subints in groups:
                    # Generate arf file for the purouses of getting a pulsar
                    # name and other values that are used to crate the outdir
                    arf = utils.ArchiveFile(os.path.join(subdirs[0], subints[0]))
                    
                    # Create outupt dir for groupfiles based on the Jname
                    groupoutdir = os.path.join(config.output_location, 'groups',
                                            arf['jname'])
                    try:
                        os.makedirs(groupoutdir)
                    except OSError:
                        # Directory already exists
                        pass

                    # Create out dir for logs
                    logoutdir = os.path.join(config.output_location, 'logs',
                                            arf['jname'])
                    self._make_dir(logoutdir)

                    # For each group, write a groupfile that lists all
                    # information required to combine the group into one archive
                    basegroupname = "{:s}_{:s}_{:s}_{:05d}_{:d}subints"\
                        .format(arf['jname'], arf['band'], arf['yyyymmdd'],
                                arf['secs'], len(subints))
                    groupfile = os.path.join(groupoutdir, basegroupname+'_0.txt')
                    logfile = os.path.join(logoutdir, basegroupname+'.log')
                    logfns.append(logfile)
                    try:
                        self.loader.write_groupfile(subdirs, subints, groupfile)
                    except errors.InputError:
                        # File already exists, create a unique name
                        highest_suffix = self.find_highest_suffix(groupfile)
                        new_suffix = highest_suffix + 1
                        base, ext = os.path.splitext(groupfile)
                        new_groupfile =\
                            os.path.join(groupoutdir,
                                        basegroupname+f'_{new_suffix}.txt')
                        utils.print_info(f"Renaming '{groupfile}' to "
                                        f"'{new_groupfile}'")
                        # Retry creating the file
                        groupfile = new_groupfile
                        self.loader.write_groupfile(subdirs, subints, groupfile)
                    grouppath, groupfn = os.path.split(groupfile)

                    # Check if the observation is a calibrator run or a pulsar
                    obstype = self.loader.check_if_calib(arf)
                    if obstype not in OBSTYPES:
                        raise ValueError("The type  of the observation constructed"
                                        f" from groupfile: '{groupfn}' does not "
                                        "match the values allowed by the "
                                        "database. The allowed values are: "
                                        f"'{OBSTYPES}'")

                    # Find the hash of the ephemeris, if present
                    try:
                        ephem = utils.extract_parfile(os.path.join(subdirs[0],
                                                    subints[0]))
                        ephem_md5sum = hashlib.md5(ephem.encode()).hexdigest()
                    except errors.InputError as exc:
                        warnings.warn(exc.get_message(),
                                    errors.CoastGuardWarning)
                        ephem_md5sum = None

                    obsinfo.append({'sourcename': arf['name'],
                                    'start_mjd': arf['intmjd']+arf['fracmjd'],
                                    'obstype': obstype,
                                    'nsubbands': len(subdirs),
                                    'nsubints': len(subints),
                                    'obsband': arf['band']},)

                    values.append({'filepath': grouppath,
                                'filename': groupfn,
                                'stage': 'grouped',
                                'md5sum': utils.get_md5sum(groupfile),
                                'ephem_md5sum': ephem_md5sum,
                                'coords': arf['coords'],
                                'filesize': os.path.getsize(groupfile)})
            else:
                # No valid groups could be constructed for this directory
                utils.print_info("No valid groups were found in directory "
                                 f"{dir.path}")
                # Copy the temporary log to the permanent log location.
                shutil.copy(tmplogfn, os.path.join(config.output_location, 'logs',
                                                   f"dir{dir.dir_id}.log"))

                # Mark dir as empty
                with self.Session() as session:
                    dir_update = update(Directory)\
                        .where(Directory.dir_id == dir.dir_id)\
                        .values(note="No valid groups were found in directory",
                                last_modified=datetime.now(), status='empty')
                    session.execute(dir_update)
                    session.commit()
                return None

        except Exception as exc:
            # If an exception occurs, log info and update DB, marking the
            # directory as failed
            utils.print_info("Exception caught while working on Dir ID "
                             f"{dir.dir_id}", 0)

            # Copy the temporary log to the permanent log location.
            shutil.copy(tmplogfn, os.path.join(config.output_location, 'logs',
                                               f"dir{dir.dir_id}.log"))

            if isinstance(exc, (errors.CoastGuardError,
                                errors.FatalCoastGuardError)):
                msg = exc.get_message()
            else:
                msg = str(exc)
                utils.log_message(traceback.format_exc(), 'error')

            # Set directory status to failed
            with self.Session() as session:
                dir_update = update(Directory)\
                    .where(Directory.dir_id == dir.dir_id)\
                    .values(note='Grouping failed! {:s}: {:s}'.format(
                                                                type(exc).
                                                                __name__,
                                                                msg),
                            last_modified=datetime.now(), status='failed')
                session.execute(dir_update)
                session.commit()
            # Add ID number to exception arguments
            raise errors.DataReductionFailed(f"Grouping failed for Dir ID: "
                                             f"{dir.dir_id}") from exc

        except KeyboardInterrupt:
             # On user interrupt, release current running dir
             self._change_dir_status([dir], from_stat=['running'])
             raise

        else:
            # If no exception is raised, update the DB
            version_id = utils.get_version_id(self.Session)
            with self.Session() as session:
                for obsvals, vals, logfn in zip(obsinfo, values, logfns):
                    # Create a new observation entry
                    obsvals['dir_id'] = dir.dir_id
                    new_obs_insert = insert(Observation).values(obsvals)
                    new_obs_id = session.execute(new_obs_insert)\
                        .inserted_primary_key[0]

                    # Retrieve new observation
                    new_obs_query = select(Observation)\
                        .where(Observation.obs_id == new_obs_id)
                    new_obs = session.scalars(new_obs_query).one()

                    # Create a new file entry
                    vals['obs_id'] = new_obs_id
                    vals['version_id'] = version_id
                    new_file_insert = insert(File).values(vals)
                    
                    new_file_id = session.execute(new_file_insert)\
                        .inserted_primary_key[0]

                    # Update new observation with new file id
                    new_obs.current_file_id = new_file_id

                    # Insert log
                    shutil.copy(tmplogfn, logfn)
                    new_log_insert = insert(Log)\
                        .values(obs_id=new_obs.obs_id,
                                logpath=os.path.dirname(logfn),
                                logname=os.path.basename(logfn))
                    session.execute(new_log_insert)

                    # Change directory status to processed
                    directory_update = \
                        update(Directory)\
                        .where(Directory.dir_id == dir.dir_id)\
                        .values(status="processed",
                                last_modified=datetime.now())
                    session.execute(directory_update)
                    session.commit()
                    
            ninserts += len(values)

        finally:
            # Remove temporary log
            if os.path.isfile(tmplogfn):
                os.remove(tmplogfn)
        return ninserts

    def load(self, force=False):
        """Entrypoint for pipeline mode.
        Finds directories with raw data and loads them into the DB.
        Afterwards, queries DB for ungrouped rawdata, creates
        groupfiles and loads them to the DB files table, also creating
        a new entry in the observations table in the process

        Args:
           force (bool, optional): force loading of rawdata regardless of
                the date it was last modified. Dafaults to False (load only
                those rawdata dirs, that are newer than the newest directory
                in the DB).
        """

        with utils.std_out_err_redirect_tqdm() as orig_stdout:
            utils.print_info("Loading and cleaning data from {:s}".format(
                            self.loader.backend), 1)
            # Load new rawdata dirs into the DB
            self.load_rawdata_dirs(force)
            # Query DB for new directories
            dirs = self.get_togroup()
            for dir in (dirs_to_load := tqdm(dirs, file=orig_stdout,
                                             dynamic_ncols=True)):
                dirs_to_load.set_description("Writing groupfiles for rawdata "
                                             f"in directory {dir.path}")
                try:
                    self.load_groups(dir)
                except:
                    # If an exception occures, release all unprocessed dirs
                    self._change_dir_status(dirs)
                    raise

    def _create_temp_logger(self):
        """Create a temporary directory and initialize a python logger instance
        bound to that direcory
        """
        tmplogdesc, tmplogfn = tempfile.mkstemp(suffix='.log',
                                                dir=config.tmp_directory)
        os.close(tmplogdesc)
        logger.setup_logger(tmplogfn)

        return tmplogdesc, tmplogfn

    def _mark_dir_as_running(self, dir):
        """Mark a directory as running in the database

        Args:
            dir (Directory): A directory object representing the DB entry to be
            set to 'running'
        """

        with self.Session() as session:
            # Query the DB to get the up-to date entry
            query = select(Directory)\
                .where(Directory.dir_id == dir.dir_id).with_for_update()
            check_dir = session.scalars(query).one()

            if dir.status != 'submitted':
                msg = "Groupings can only be generated for 'directory' " +\
                      "entries with status 'submitted'. (The status of Dir " +\
                      f"ID {check_dir.dir_id} is '{check_dir.status}'.)"
                return errors.BadStatusError(msg)

            # Update the dir and release the lock
            check_dir.status = "running"
            check_dir.last_modified = datetime.now()
            session.commit()

    def _change_dir_status(self, dirs, from_stat=['submitted'],
                                to_stat='new'):
        """Change the status of directory entries in the DB

        Args:
            dirs (list): A list of Directory objects representing DB 
            entries.
            from_stat (optional, list): List of initial statuses.
                Defaults to ["submitted"]
            to_stat (str): End status. Defaults to "new"
        """

        dirs_ids = [dir.dir_id for dir in dirs]
        with self.Session() as session:
            query = update(Directory)\
                .where(and_(Directory.dir_id.in_(dirs_ids),
                            Directory.status.in_(from_stat)))\
                .values({'status': to_stat, 'last_modified': datetime.now()})
            session.execute(query)
            session.commit()

    def find_highest_suffix(self, file):
        """Helper function for avoiding file name conflicts. Scans the dir of
        a file, finds all files with names like filename_X and outputs a new,
        unique name.
        """
        filepath, filename = os.path.split(file)
        fn, fileext = os.path.splitext(filename)
        fn_no_num = "_".join(fn.split("_")[:-1])

        pattern = re.compile(f'{re.escape(fn_no_num)}_(\d+)')
        matching_files = [file for file in os.listdir(filepath)
                          if pattern.match(file)]
        if not matching_files:
            return 0
        suffixes = [int(pattern.match(file).group(1)) for file in matching_files]
        return max(suffixes)
