from coastguard3 import logger, errors, utils, database, config
from coastguard3.database.schema import File, Log
from coastguard3.database.schema import Observation, File

import os
import traceback
from sqlalchemy import select, update, and_
from sqlalchemy.orm import sessionmaker
from datetime import datetime
from abc import ABC


class BaseManager(ABC):
    """This is the base class for all pipeline managers."""

    required_class_variables = ["in_stage", "out_stage"]
    in_stage = None    
    out_stage = None

    def __init__(self, backends: list, observatories: list,
                 source: str = None, start_mjd=None, end_mjd=None,
                 engine=None):
        
        # Initialize database connection
        self.engine = self._get_engine(engine)

        # Create instance variables
        self.source = source
        self.start_mjd = start_mjd
        self.end_mjd = end_mjd
        self.backends = ['all'] if backends in ('all', ['all']) else backends
        self.observatories = ['all'] if observatories in ('all', ['all']) else\
            observatories

    def __init_subclass__(cls, **kwargs):
        for var_name in cls.required_class_variables:
            if getattr(cls, var_name) is None:
                raise TypeError(f"Class {cls.__name__} must define the "
                                f"'{var_name}' class variable")

    def get_toprocess(self):
        """Get a list of files that need to be processed.

            Args:
                targets (list, optional): List of pulsar to search for.
                Defaults to None (fetch all availible pulsars).

            Returns:
                list: A list of File objects that represent the queried
                DB entries
        """

        query = self._create_base_getquery()

        with self.Session(expire_on_commit=False) as session:
            files = session.scalars(query).all()

            if not files:
                raise errors.QueryError("No files are ready to be processed. "
                                        "Check if the source, backend and "
                                        "reciever are specified correcty.")

            # Set the files as 'submitted' release lock and refresh the queried
            # files
            for file in files:
                file.status = 'submitted'
            session.commit()
        utils.print_info("Got {} files to be processed".format(len(files))) 

        return files

    def _create_base_getquery(self):
        """Get a list of files that need to be processed

            Args:
                targets (list, optional): List of pulsar to search for.
                Defaults to None (fetch all availible pulsars).

            Returns:
                list: A list of File objects that represent the queried
                DB entries
        """

        # Construct the query
        query = select(File).join(File.observation).where(
            and_(File.status == "new", File.stage == self.in_stage,
                 File.is_deleted != True,
                 Observation.sourcename == self.source)).with_for_update()
        if self.observatories != ['all']:
            query = query.where(
                Observation.observatory.in_(self.observatories))
        if self.backends != ['all']:
            query = query.where(
                Observation.backend.in_(self.backends))
        if self.start_mjd:
            query = query.where(Observation.start_mjd > self.start_mjd)
        if self.end_mjd:
            query.where(Observation.start_mjd < self.end_mjd)

        return query

    @staticmethod
    def _make_dir(path, permissions=None):
        """Helper method for creating a new directory, unless one already
        exists
        """
        try:
            os.makedirs(path)
            if permissions is not None:
                utils.add_group_permissions(path, permissions)
        except OSError:
            # Directory already exists
            pass

    def _setup_logger(self, obs_id):
        """Given an obs_id retrive the corresponding entry
            in the logs table and setup a python logger.

            Inputs:
                obs_id: The ID of the obsevation

            Output:
                None
        """

        with self.Session() as session:
            logs = session.scalars(
                select(Log).where(Log.obs_id == obs_id)).all()

        if len(logs) != 1:
            raise errors.DatabaseError("Bad number of rows ({:d}) with \
                                    obs_id={:d}!".format(len(logs), obs_id))
        log = logs[0]
        logfn = os.path.join(log.logpath, log.logname)
        logger.setup_logger(logfn)

    def _mark_as_running(self, file_id):
        """Connect to the database and check if the status of the submitted
        file is 'submitted'. If true, set the status of the file to 'running'

        Args:
            file_id (int): ID of a file table entry from the DB.

        Raises:
            errors.BadStatusError: If the File object has a status different 
            from 'submitted'
        """
        with self.Session() as session:
            check_query = select(File)\
                .where(File.file_id == file_id).with_for_update()
            check_file = session.scalars(check_query).one()

            if check_file.status != 'submitted':
                raise errors.BadStatusError("New files can only be "
                                            "generated from 'files' entries "
                                            "with status 'submitted'. "
                                            "(The status of "
                                            "File ID {:d} is '{:s}'.)"
                                            .format(check_file.file_id,
                                                    check_file.status))
            # Update the file, setting status to 'running'
            update_file = (update(File)
                           .where(File.file_id == check_file.file_id)
                           .values(status='running',
                                   last_modified=datetime.now()))
            session.execute(update_file)
            # release lock
            session.commit()

    def _change_file_status(self, files, from_stat=["submitted"],
                            to_stat="new"):
        """Change status of file entries in the DB

        Args:
            files (list): A list of File objects representing DB file table
            entries.
            from_stat (optional, list): List of initial statuses.
                Defaults to ["submitted"]
            to_stat (str): End status. Defaults to "new"
        """

        files_ids = [file.file_id for file in files]
        with self.Session() as session:
            query = update(File)\
                .where(and_(File.file_id.in_(files_ids),
                            File.status.in_(from_stat)))\
                .values({'status': to_stat,
                         'last_modified': datetime.now()})
            session.execute(query)
            session.commit()

    def _step_failed(self, file, exc, exc_note, status='failed'):
        """Mark a step as failed in the database and insert a message
        containing the caught excetion.

        Args:
            file (File): A File object representing a DB file table entry.
            exc (Exception): The caught exception
        """
        utils.print_info("Exception caught while working on File ID {:d}"
                         .format(file.file_id), 0)
        if isinstance(exc, (errors.CoastGuardError,
                            errors.FatalCoastGuardError)):
            # Get error message without colours mark-up
            msg = exc.get_message()
        else:
            msg = str(exc)
            utils.log_message(traceback.format_exc(), 'error')

        # Mark the file as failed
        with self.Session() as session:
            file_update = (update(File).where(File.file_id == file.file_id)
                           .values(status=status, note='{:s} {:s}: {:s}'
                           .format(exc_note, type(exc).__name__, msg),
                                   last_modified=datetime.now()))
            session.execute(file_update)
            session.commit()

    def _get_engine(self, engine):
        engine = database.get_engine() if not engine else engine
        self.Session = sessionmaker(engine)
        return engine