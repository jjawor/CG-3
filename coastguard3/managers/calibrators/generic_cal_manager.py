import os
import shutil
import tempfile
import pyriseset3 as rs
from tqdm import tqdm
from datetime import datetime
from sqlalchemy import insert, update, select, and_
from sqlalchemy.orm import joinedload
from sqlalchemy.exc import IntegrityError

from coastguard3 import utils, config, diagnose, errors
from coastguard3.database.schema import Observation, File, Caldb, Diagnostic,\
    Qctrl, OBSTYPES
from coastguard3.managers import BaseManager

class GenerericCalibratorManager(BaseManager):
    """
    The generic implementation of Combining managers. Handles database
    interactions related to calibration when running in pipeline mode.
    """
    in_stage = "cleaned"
    out_stage = "calibrated"

    def __init__(self, calibrator, backends: list, observatories: list,
                 source: str, cal_ext='pcal.T', poldiag_ext="Scyl.png",
                 scrunch_ext='scrunched', reattempt=False, start_mjd: str=None,
                 end_mjd:str=None, engine=None):
        """Initialize parameters and establish a connection to the database.

        Args:
            calibrator (class): A class object that represents the calibrator
            to be applied to the data.
            backends (list): List of the backends to fetch data from. ['all']
                will fetch data from every backend availible
            observatories (list): List of the observatories to fetch data from.
                ['all'] will fetch data from every observatory availible
            source (str): Name of the pulsar to reduce.

            cal_ext (str, optional): Extension to apply to configured diode
                scans. Defaults to 'pcal.T'.
            poldiag_ext (str, optional): Extension to apply to poldiag
                diagnostic plots. Defaults to "Scyl.png".
            scrunch_ext (str, optional): Extension to apply to scrunched
                diagnostics. Defaults to 'scrunched'.
            reattempt (bool, optional): Try recalibrating files that
                preiviously failed calibration due to a lacking calibrator.
                Defaults to False.
            engine (class, optional): An engine class connected to a
                database. Defaults to None (Create a sessionmaker using the
                engine from the config file).
        """

        super().__init__(backends, observatories, source, start_mjd, end_mjd,
                         engine)

        self.calibrator = calibrator
        self.cal_ext = cal_ext
        self.poldiag_ext = poldiag_ext
        self.scrunch_ext = scrunch_ext
        self.reattempt = reattempt

    def get_tocalib(self):
        """Get a list of files that need to be calibrated. Sort the files into
        pulsar observations and calibrator observations.

        Returns:
            list: A list of File objects that represent the queried
                    DB entries
        """

        # Construct the query
        query = select(File).join(File.observation).join(File.qctrl).where(
            and_(File.is_deleted != True, Qctrl.qcpassed == True,
                 Observation.sourcename == self.source))\
            .with_for_update()
        if self.reattempt is False:
            query = query.where(and_(File.status == "new",
                                     File.stage == "cleaned"))
        else:
            query = query.where(and_(File.status == "calfail",
                                     File.stage == "calibrated"))
        if self.observatories != ['all']:
            query = query.where(
                Observation.observatory.in_(self.observatories))
        if self.backends != ['all']:
            query = query.where(
                Observation.backend.in_(self.backends))

        with self.Session() as session:
            # Change status to 'submitted' and release lock
            files = session.scalars(query).all()
            if not files:
                raise errors.QueryError("No files are ready to be calibrated. "
                                        "Check if the source, backend and "
                                        "reciever are specified correcty.")

            for file in files:
                file.status = 'submitted'
            session.commit()

            # Requery with eager load to ensure that observation info is loaded
            eager_query = select(File)\
                .where(File.file_id.in_([f.file_id for f in files]))\
                .options(joinedload(File.observation))
            files = session.scalars(eager_query).all()

        utils.print_info("Got {} files to be calibrated".format(len(files)))

        return files

    def load_calibrated_file(self, file):
        """Calibrate a file and load the output into the database.

        In the case of a 'pulsar' observation this will fetch an associated
        calibrator database and run the calibrate_pulsar method of the
        calibrator class.
        In the case of a 'cal' scan, this will run the prepare_cal method of
        the calibrator class

        The output is loaded into the database.

        Args:
            file(File): A File object representing a DB file table entry.

        Returns:
            str: The file_id of the newly loaded corrected file.
        """

        # Setup logger and mark file as running
        self._setup_logger(file.obs_id)
        self._mark_as_running(file.file_id)  # Mark the file as running

        # Special calibration check for qcpassed and stage of input file
        if (file.status != 'submitted') or (file.stage != 'cleaned' or
                (file.stage != 'calibrated' and file.status == 'calfail')) or\
                (not file.qctrl.qcpassed):
            raise errors.BadStatusError(
                "Calibrated files can only be generated from 'file' entries "
                "with status='submited' and stage='cleaned' "
                f"and with qcpassed=True. (For File ID {file.file_id}: status="
                f"'{file.status}', stage='{file.stage}', "
                f"qcpassed={file.qcpassed})")

        # Initialize storage to carry values to insert into DB
        values = {}
        diagvals = []
        note = ""

        try:
            # Process the file based on its type
            if file.observation.obstype == 'pulsar':
                # The file is a pulsar, find a caldb
                self.update_caldb(file.observation.sourcename)
                caldb = self.get_caldb(file.observation.sourcename)
                if caldb is None:
                    # caldb does not exist
                    raise ValueError(f"The caldb for pulsar: "
                                     f"{file.observation.sourcename} does "
                                     f"not exist.")

                arf = utils.ArchiveFile(os.path.join(file.filepath,
                                                     file.filename))
                try:
                    infile = os.path.join(file.filepath, file.filename)
                    caldbfile = os.path.join(caldb.caldbpath, caldb.caldbname)
                    outfile, nchans = self.calibrator.calibrate_pulsar(infile,
                                                                       caldbfile)
                    if nchans != arf['nchan']:
                        note = f"Scrunched to {nchans} channels " +\
                            "(1.5625 MHz each)"
                    values['status'] = 'awaiting_qctrl'
            
                except errors.NoValidCalibratorError:
                    # Calibration of a pulsar failed due to a lack of a
                    # valid polcal scan. Create a copy of the cleaned file and
                    # mark it file as an uncalibrated file with status 'calfail'
                    infn, _ = os.path.splitext(file.filename)
                    stdout, stderr = utils.execute(['pam', '-e',
                                                    self.calfail_ext, infile])
                    outfile = os.path.join(file.filepath, infn)+\
                        '.'+self.calfail_ext
                    note = "File could not be calibrated"
                    values['status'] = 'calfail'

            elif file.observation.obstype == 'cal':
                # Prepare a calibrator observation for being used to calibrate
                infile = os.path.join(file.filepath, file.filename)
                outfile, nchans = self.calibrator.prepare_cal(infile)
                values['status'] = 'done'
        
            elif file.observation.obstype not in OBSTYPES:
                # File is not in the allowed obstypes. This is very bad
                raise ValueError(f"The observation: '{file.obs_id}' has a type"
                                 f": {file.observation.obstype} which is not "
                                 "supported by the database.")
            else:
                # File is in allowed obstypes, but is neither a calibration or
                # a pulsar observation
                raise ValueError(f"The observation: '{file.obs_id}' has a type"
                                 f": {file.observation.obstype}, which is not "
                                 "'cal' or 'pulsar'")

            outarf = utils.ArchiveFile(outfile)
            # Create summary plots
            fullresfn, lowresfn = diagnose.make_summary_plots(outarf)
            diagvals.extend(
                ({'diagnosticpath': os.path.dirname(fullresfn),
                  'diagnosticname': os.path.basename(fullresfn)},
                 {'diagnosticpath': os.path.dirname(lowresfn),
                  'diagnosticname': os.path.basename(lowresfn)}))

            # Make polarization-specific diagnostic plots
            pp_fullresfn = outarf.fn+'.'+self.poldiag_ext
            diagnose.make_polprofile_plot(outarf, outfn=pp_fullresfn)
            preproc = 'C,D,T,F,B 128'
            pp_lowresfn = outarf.fn+'.'+self.scrunch_ext+'.'+self.poldiag_ext
            diagnose.make_polprofile_plot(outarf, preproc, outfn=pp_lowresfn)
            # Make sure plots are group-readable
            utils.add_group_permissions(pp_fullresfn, "r")
            utils.add_group_permissions(pp_lowresfn, "r")
            # Add values to insert into DB
            diagvals.extend(
                ({'diagnosticpath': os.path.dirname(pp_fullresfn),
                  'diagnosticname': os.path.basename(pp_fullresfn)},
                 {'diagnosticpath': os.path.dirname(pp_lowresfn),
                  'diagnosticname': os.path.basename(pp_lowresfn)}))
            values['snr'] = outarf['snr']

            if not os.path.isfile(outarf.fn):
                raise ValueError(f"Cannot find output file ({outarf.fn})!")

            if file.observation.obstype == 'cal':
                # Extra daignostics for calibrator scans
                plotfn = self.make_stokes_plot(outarf)
                diagvals.extend([{'diagnosticpath': os.path.dirname(plotfn),
                                  'diagnosticname': os.path.basename(plotfn)}])

            # Add other file-related values to be inserted into the DB
            values['filepath'], values['filename'] =\
                os.path.split(outarf.fn)
            values['md5sum'] = utils.get_md5sum(outarf.fn)
            values['filesize'] = os.path.getsize(outarf.fn)
            values['coords'] = outarf['coords']

        except Exception as exc:
            utils.print_info("Exception caught while working on File ID "
                             f"{file.file_id}", 0)
            # Add ID number to exception arguments
            exc.args = (exc.args[0] + f"\n(File ID: {file.file_id})",)

            # Set the fail message and staus based on the type of file handled
            if file.observation.obstype == 'cal':
                status = 'failed'
                exc_note = 'Calibration failed!'

            elif file.observation.obstype == 'pulsar':
                status = 'failed'
                exc_note = 'Calibration failed!'


            self._step_failed(file, exc, exc_note, status)
            raise

        else:
            # Success!
            version_id = utils.get_version_id(self.Session)
            with self.Session() as session:
                # Insert new file
                values['version_id'] = version_id
                values['obs_id'] = file.obs_id
                values['note'] = note
                values['stage'] = 'calibrated'
                values['parent_file_id'] = file.file_id
                new_file_insert = insert(File).values(values)
                new_file_id = session.execute(new_file_insert)\
                        .inserted_primary_key[0]

                # Insert new diagnostics
                for diagval in diagvals:
                    diagval['file_id'] = new_file_id
                    new_diag = insert(Diagnostic).values(diagval)
                    session.execute(new_diag)

                # Update observation with new current_file_id
                obs_update = (
                    update(Observation)
                    .where(Observation.obs_id == file.obs_id)
                    .values(current_file_id=new_file_id,
                            last_modified=datetime.now())
                )
                session.execute(obs_update)

                # Update status of partent file
                file_update = (
                    update(File)
                    .where(File.file_id == file.file_id)
                    .values(status='processed', last_modified=datetime.now()))
                session.execute(file_update)
                session.commit()

            if file.observation.obstype == 'cal':
                # Update the caldb
                self.update_caldb(file.observation.sourcename)
            return new_file_id

    def get_caldb(self, sourcename):
        """Given a sourcename return the corresponding entry in the caldb
        table or None, if no caldb exists for the source.

        Args:
            sourcename (str): The name of the source to match.
                (NOTE: '_R' will be removed from the sourcename, if
                present)

        Returns:
            Caldb: A caldb object representing a entry from the DB if one was
                found, else None.
        """
        name = utils.get_prefname(sourcename)
        if name.endswith('_R'):
            name = name[:-2]

        with self.Session() as session:
            cals = session.scalars(
                select(Caldb).where(Caldb.sourcename == name)).all()

        if len(cals) == 1:
            return cals[0]
        elif len(cals) == 0:
            return None
        else:
            raise errors.DatabaseError(f"Bad number of caldb rows "
                                       f"({len(cals)}) with sourcename="
                                       f"'{name}'!")

    def update_caldb(self, sourcename, force=False):
        """Check for new calibrator scans. If found, update the calibrator
        database.

            Args:
                sourcename (str): The name of the source to match.
                    (NOTE: '_R' will be removed from the sourcename, if
                    present)
                force (bool): Forcefully update the caldb. Defaults to False

            Returns:
                str: The path to the updated caldb.
        """

        name = utils.get_prefname(sourcename)
        if name.endswith('_R'):
            name = name[:-2]

        with self.Session() as session:
            # Get the caldb, locking the rows if any exist
            caldbs = session.scalars(
                select(Caldb).where(Caldb.sourcename == name).with_for_update()
                    ).all()

            if caldbs == []:
                # Set up new caldb
                outdir = os.path.join(config.output_location, 'caldbs')
                self._make_dir(outdir)
                outfnf = f'{name.upper()}.caldb.txt'
                outfile = os.path.join(outdir, outfnf)
                # Construc temp values
                values = {'sourcename': name,
                        'caldbpath': outdir,
                        'caldbname': outfnf}
                try:
                    # Create a calibrator database
                    basecaldir = os.path.join(config.output_location,
                                            name.upper()+"_R")
                    utils.execute(['pac', '-w', '-u', self.cal_ext, '-k',
                                    outfile], dir=basecaldir)

                except:
                    # Mark as failed
                    values['note'] = ""
                    values['status'] = 'failed'
                    values['last_modified'] = datetime.now()
                    updated_caldb = insert(Caldb).values(values)
                    session.execute(updated_caldb)

                else:
                    # Update database with new/updated caldb entry
                    values['note'] = 'New caldb created'
                    values['last_modified'] = datetime.now()
                    values['status'] = 'ready'
                    updated_caldb = insert(Caldb).values(values)
                    session.execute(updated_caldb)

                session.commit()
                return outfile

            elif len(caldbs) != 1:
                # Raise an exception if there is more than one caldb
                # If this trigers, something is very, very wrong.
                raise errors.DatabaseError(f"Bad number of caldb rows "
                                           f"({len(caldbs)}) with sourcename="
                                           f"'{name}'!")

            else:
                # Set up update of existing caldb
                caldb = caldbs[0]
                lastupdated = caldb.last_modified
                outfile = os.path.join(caldb.caldbpath, caldb.caldbname)
                values = {}

                # Check if there are any files to update the caldb with
                query = select(File).join(File.observation)\
                    .where(and_(File.status == 'done',
                                File.stage == 'calibrated',
                                Observation.obstype == 'cal'))\
                    .options(joinedload(File.observation))
                files = session.scalars(query).all()
                numnew = 0
                for file in files:
                    if file.added > lastupdated:
                        numnew += 1

                utils.print_info(f"Found {len(files)} suitable calibrators for"
                                 f" {name}. {numnew} are new.", 2)

                values['numentries'] = len(files)

                # Mark update of caldb as in-progress
                caldb_update = update(Caldb)\
                    .where(Caldb.caldb_id == caldb.caldb_id)\
                    .values(status='updating', last_modified=datetime.now())
                session.execute(caldb_update)
                session.commit()

                try:
                    if numnew or force:
                        # Create an updated version of the calibrator database
                        basecaldir = os.path.join(config.output_location,
                                                  name.upper()+"_R")
                        utils.execute(['pac', '-w', '-u', self.cal_ext, '-k',
                                       outfile], dir=basecaldir)

                except:
                    # Mark as failed
                    values['status'] = 'failed'
                    values['note'] = 'caldb update failed'
                    values['last_modifed'] = datetime.now()
                    caldb_fail = update(Caldb)\
                        .where(Caldb.caldb_id == caldb.caldb_id)
                    session.execute(caldb_fail.values(values))

                else:
                    # Update database with new/updated caldb entry
                    values['status'] = 'ready'
                    values['note'] = f'{numnew} new entries added'
                    values['last_modified'] = datetime.now()
                    caldb_new = update(Caldb)\
                        .where(Caldb.caldb_id == caldb.caldb_id)
                    session.execute(caldb_new.values(values))
                session.commit()

        return outfile

    def calibrate(self):
        """Entrypoint for pipeline mode.
        Queries the DB for observations that need calibrating and launches the
        calibrating procedure. The DB is updated with new information.
        """

        with utils.std_out_err_redirect_tqdm() as orig_stdout:
            for file in (files_to_calib :=
                         tqdm(self.get_tocalib(), file=orig_stdout)):
                files_to_calib.set_description("Calibrating {:s}"
                                               .format(file.filename))
                try:
                    self.load_calibrated_file(file)
                except KeyboardInterrupt:
                    # On user interrupt, release current running dir and reset
                    # queried dirs
                    self._change_file_status([file], from_stat=['running'])
                    self._change_file_status(files_to_calib)
                    raise
                except Exception:
                    # Release the unprocessed files
                    self._change_file_status(files_to_calib)
                    raise

    @staticmethod
    def make_stokes_plot(arf):
        """Make a stokes profile plot.

            Input:
                arf: An ArchiveFile object.

            Output:
                str: The name of the stokes plot.
        """
        utils.print_info(f"Creating stokes profile plot for {arf.fn}", 3)
        outfile = f"{arf.fn}.stokes.png"
        utils.print_info(f"Output plot name: {outfile}", 2)
        outext = os.path.splitext(outfile)[-1]
        handle, tmpfile = tempfile.mkstemp(suffix=outext)

        grdev = f"{tmpfile}/PNG"
        utils.execute(['psrplot', '-p', 'stokes', '-j', 'CDTF', arf.fn, '-D',
                       grdev])
        # Rename tmpfn to requested output filename
        shutil.move(tmpfile, outfile)

        # Make sure plot is group-readable
        utils.add_group_permissions(outfile, "r")
        return outfile