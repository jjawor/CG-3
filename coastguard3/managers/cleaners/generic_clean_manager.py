import os
import hashlib
import warnings
from tqdm import tqdm
from datetime import datetime
from sqlalchemy import update, select, insert, and_

from coastguard3 import config, utils, errors, diagnose
from coastguard3.managers import BaseManager
from coastguard3.database.schema import File, Observation, Diagnostic


class GenericCleanerManager(BaseManager):
    in_stage = "corrected"
    out_stage = "cleaned"

    def __init__(self, cleaners: list, backends: list,
                 observatories: list, source: str, start_mjd: str = None,
                 end_mjd: str = None, engine=None):
        super().__init__(backends, observatories, source, start_mjd, end_mjd,
                         engine)
        self.cleaners = cleaners

    def execute_cleaning_queue(self, infile):
        """Run selected cleaners on a archive file

        Args:
            infile (str): Path to the archive to be cleaned

        Returns:
            ArchiveFile: An ArchiveFile object representing the newly cleaned
            file
        """

        arf = utils.ArchiveFile(infile)
        # Clean the data file
        config.cfg.load_configs_for_archive(arf)

        for cleaner in self.cleaners:
            cleaner = cleaner()
            cleaner.run(arf.get_archive())

        return arf

    def load_cleaned_file(self, file):
        """ Process the file by correcting its header and load the new file
        into the database.

        Args:
            file: A File object representing a file entry from the DB.

        Returns:
            int: The ID of the newly loaded corrected file.
        """

        # Setup a logger to collect relevant output
        self._setup_logger(file.obs_id)
        self._mark_as_running(file.file_id)  # Mark as running

        infn = os.path.join(file.filepath, file.filename)
        try:

            # Run the cleaner queue
            arf = self.execute_cleaning_queue(infn)

            # Write out the cleaned data file
            archivedir = os.path.join(config.output_location,
                                      config.output_layout) % arf
            archivefn = (config.outfn_template+".clean") % arf
            cleanfn = os.path.join(archivedir, archivefn)

            # Create the output directory
            self._make_dir(archivedir, permissions="rwx")

            arf.get_archive().unload(cleanfn)
            arf = utils.ArchiveFile(cleanfn)
            # Make diagnostic plots
            fullresfn, lowresfn = diagnose.make_summary_plots(arf)

            # Pre-compute values to insert because some might be
            # slow to generate
            arf = utils.ArchiveFile(cleanfn)
            values = {'filepath': archivedir,
                      'filename': archivefn,
                      'stage': 'cleaned',
                      'md5sum': utils.get_md5sum(cleanfn),
                      'filesize': os.path.getsize(cleanfn),
                      'parent_file_id': file.file_id,
                      'coords': arf['coords'],
                      'snr': arf['snr']}
            try:
                ephem = utils.extract_parfile(cleanfn)
                values['ephem_md5sum'] = hashlib.md5(ephem.encode())\
                    .hexdigest()
            except errors.InputError as exc:
                warnings.warn(exc.get_message(), errors.CoastGuardWarning)
            diagvals = [{'diagnosticpath': os.path.dirname(fullresfn),
                        'diagnosticname': os.path.basename(fullresfn)},
                        {'diagnosticpath': os.path.dirname(lowresfn),
                        'diagnosticname': os.path.basename(lowresfn)}
                        ]

        except Exception as exc:
            exc_note = "Cleaning failed!"
            self._step_failed(file, exc, exc_note)
            raise

        else:
            # Success!
            version_id = utils.get_version_id(self.Session)
            with self.Session() as session:
                # Insert new file
                values['version_id'] = version_id
                values['obs_id'] = file.obs_id
                values['status'] = 'awaiting_qctrl'
                new_file_insert = insert(File).values(values)
                new_file_id = session.execute(new_file_insert)\
                        .inserted_primary_key[0]

                # Update observation
                obs_update = (
                    update(Observation)
                    .where(Observation.obs_id == file.obs_id)
                    .values(current_file_id=new_file_id,
                            last_modified=datetime.now())
                )
                session.execute(obs_update)

                # Insert new diagnostics
                for diagval in diagvals:
                    diagval['file_id'] = new_file_id
                    new_diag = insert(Diagnostic).values(diagval)
                    session.execute(new_diag)

                # Update status of partent file
                file_update = (
                    update(File)
                    .where(File.file_id == file.file_id)
                    .values(status='processed', last_modified=datetime.now()))
                session.execute(file_update)
                session.commit()

            return new_file_id

    def clean(self):
        """Entrypoint for pipeline mode.
        Queries the DB for observations that need cleaning and launches the
        cleaning procedure. The DB is updated with new information.
        """

        with utils.std_out_err_redirect_tqdm() as orig_stdout:
            for file in (files_to_clean :=
                         tqdm(self.get_toprocess(),
                              file=orig_stdout)):
                files_to_clean.set_description(f"Cleaning {file.filename}")
                try:
                    self.load_cleaned_file(file)
                except KeyboardInterrupt:
                    # On user interrupt, release current running dir and reset
                    # queried dirs
                    self._change_file_status([file], from_stat=['running'])
                    self._change_file_status(files_to_clean)
                    raise
                except Exception:
                    # Release the unprocessed files
                    self._change_file_status(files_to_clean)
                    raise