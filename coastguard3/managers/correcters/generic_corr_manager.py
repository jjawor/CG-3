from coastguard3.managers import BaseManager
from coastguard3 import utils, config, diagnose, errors
from coastguard3.database.schema import Observation, File, Diagnostic
from coastguard3.database import move_file, get_obs_files

import os
import shutil
import hashlib
import warnings
from tqdm import tqdm
from datetime import datetime
from sqlalchemy import insert, update, select, and_


class GenericCorrecterManager(BaseManager):
    in_stage = "combined"
    out_stage = "corrected"

    def __init__(self, correcter, backends: list, observatories: list,
                 source: str = None, start_mjd: str=None, end_mjd:str=None,
                 engine=None):
        super().__init__(backends, observatories, source, start_mjd, end_mjd,
                         engine)
        self.correcter = correcter

    def load_corrected_file(self, file):
        """Process a file by correcting its header. Then load the new file into
        the database. The files must have status='submitted'

        Args:
            file (File): A file object representing a row from the DB.

        Returns:
            file_id: The ID of the newly loaded corrected file.
        """

        # Setup a logger to collect relevant output
        self._setup_logger(file.obs_id)
        self._mark_as_running(file.file_id)  # Mark as running

        try:
            outdir = file.filepath
            inpath = os.path.join(file.filepath, file.filename)
            # Launch correcting procedure
            corrfn, corrstr, note = self.correcter.correct_header(inpath,
                                                                  outdir)

            arf = utils.ArchiveFile(corrfn)
            # Move file to archive directory
            archivedir = os.path.join(config.output_location,
                                      config.output_layout) % arf
            archivefn = (config.outfn_template+".corr") % arf
            try:
                os.makedirs(archivedir)
                utils.add_group_permissions(archivedir, "rwx")
            except OSError:
                # Directory already exists
                pass
            shutil.move(corrfn, os.path.join(archivedir, archivefn))
            # Update corrfn so it still refers to the file
            corrfn = os.path.join(archivedir, archivefn)
            arf.fn = corrfn

            # Make diagnostic plots
            fullresfn, lowresfn = diagnose.make_summary_plots(arf)

            # Pre-compute values to insert because some might be
            # slow to generate
            arf = utils.ArchiveFile(corrfn)
            values = {'filepath': archivedir,
                      'filename': archivefn,
                      'stage': 'corrected',
                      'note': note,
                      'md5sum': utils.get_md5sum(corrfn),
                      'filesize': os.path.getsize(corrfn),
                      'parent_file_id': file.file_id,
                      'coords': arf['coords'],
                      'snr': arf['snr']}
            try:
                ephem = utils.extract_parfile(corrfn)
                values['ephem_md5sum'] = hashlib.md5(ephem.encode()).hexdigest()
            except errors.InputError as exc:
                warnings.warn(exc.get_message(), errors.CoastGuardWarning)
            diagvals = [{'diagnosticpath': os.path.dirname(fullresfn),
                        'diagnosticname': os.path.basename(fullresfn)},
                        {'diagnosticpath': os.path.dirname(lowresfn),
                        'diagnosticname': os.path.basename(lowresfn)}
                        ]

        except Exception as exc:
            exc_note = "Correction failed!"
            self._step_failed(file, exc, exc_note)
            raise

        else:
            # Success!
            version_id = utils.get_version_id(self.Session)
            with self.Session() as session:
                # Insert new file
                values['version_id'] = version_id
                values['obs_id'] = file.obs_id
                new_file_insert = insert(File).values(values)
                new_file_id = session.execute(new_file_insert)\
                        .inserted_primary_key[0]

                # Insert new diagnostics
                for diagval in diagvals:
                    diagval['file_id'] = new_file_id
                    new_diag = insert(Diagnostic).values(diagval)
                    session.execute(new_diag)

                # Update observation with correct reciever
                obs_update = (
                    update(Observation)
                    .where(Observation.obs_id == file.obs_id)
                    .values(rcvr=arf['rcvr'], current_file_id=new_file_id,
                            last_modified=datetime.now())
                )
                session.execute(obs_update)

                # Update status of partent file
                file_update = (
                    update(File)
                    .where(File.file_id == file.file_id)
                    .values(status='processed', last_modified=datetime.now()))
                session.execute(file_update)
                session.commit()

            # If all went well. Move the combined file to the location of the
            # corrected file
            for file_tomove in get_obs_files(self.Session, file.obs_id):
                if file_tomove.filepath != values['filepath']:
                    move_file(self.Session,
                                file_tomove.file_id, values['filepath'])

            return new_file_id

    def correct(self):
        """Entrypoint for pipeline mode.
        Queries the DB for observations that need correcting and launches the
        correcting procedure. The DB is updated with new information.

        Only meant to be used in pipeline mode!!!

        Args:
            Session (sessionmaker). A sqlalchemy sessionmaker object bound
            to a database.
            targets (list, optional): List of pulsars to process.
            Defaults to None(process all availible pulsars).
        """
        with utils.std_out_err_redirect_tqdm() as orig_stdout:
            for file in (files_to_correct :=
                         tqdm(self.get_toprocess(),
                              file=orig_stdout)):
                files_to_correct.set_description("Correcting {:s}"
                                                 .format(file.filename))
                try:
                    self.load_corrected_file(file)
                except KeyboardInterrupt:
                    # On user interrupt, release current running dir and reset
                    # queried dirs
                    self._change_file_status([file], from_stat=['running'])
                    self._change_file_status(files_to_correct)
                    raise
                except Exception:
                    # Release the unprocessed files
                    self._change_file_status(files_to_correct)
                    raise
