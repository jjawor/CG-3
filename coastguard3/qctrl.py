#!/usr/bin/env python

"""
An interactive graphical user interface to check the quality
of automatically reduced data files.
"""

import sys
import os
import tempfile
import shutil
import warnings
from datetime import datetime, timedelta
from functools import partial

from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtWidgets import QDialog, QVBoxLayout, QLabel, QPlainTextEdit,\
     QWidget, QPushButton, QListWidget, QCheckBox, QTextEdit, QHBoxLayout,\
     QDateEdit, QProgressDialog
from PyQt6.QtGui import QFont, QKeySequence, QShortcut
from PyQt6.QtCore import QByteArray, pyqtSignal, QTimer, Qt, QDate, QThread

from sqlalchemy.orm import sessionmaker
from sqlalchemy import update, select, and_, or_
from sqlalchemy.dialects.mysql import insert

from pyriseset3 import utils as rsutils, actions as rsactions

from coastguard3.database.schema import File, Qctrl, Observation, Diagnostic
from coastguard3 import config, database, utils, errors, diagnose


class MissingManyDiagnosticsPopup(QDialog):
    def __init__(self, create_diag, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Missing Diagnostics")

        layout = QVBoxLayout()
        self.setLayout(layout)

        label = QLabel("Some of the observations in queue are missing "
                       "diagnostics. Do you want to create the missing "
                       "diagnostics before you start the manual quality "
                       "control? WARNING: This may take a while!")
        layout.addWidget(label)

        button1 = QPushButton("Create missing diagnostics")
        button1.clicked.connect(lambda: self.execute_and_close(create_diag))
        layout.addWidget(button1)

        button2 = QPushButton("Skip")
        button2.clicked.connect(self.close)
        layout.addWidget(button2)

        shortcut1 = QShortcut(QKeySequence("Space"), self)
        shortcut2 = QShortcut(QKeySequence("Escape"), self)
        shortcut1.activated.connect(button1.click)
        shortcut2.activated.connect(button2.click)

    def execute_and_close(self, create_diag):
        self.accept()
        create_diag()

class MissingSingleDiagnosticPopup(QDialog):
    NoClicked = pyqtSignal()
    YesClicked = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Missing Diagnostics")

        layout = QVBoxLayout()
        self.setLayout(layout)

        label = QLabel("This observation has no diagnostics. Do you want to " 
                       "create one?")
        layout.addWidget(label)

        button1 = QPushButton("Yes")
        button1.clicked.connect(self.execute_and_close)
        layout.addWidget(button1)

        button2 = QPushButton("No")
        button2.clicked.connect(self.set_default_and_close)
        layout.addWidget(button2)

        shortcut1 = QShortcut(QKeySequence("Space"), self)
        shortcut2 = QShortcut(QKeySequence("Escape"), self)
        shortcut3 = QShortcut(QKeySequence("Y"), self)
        shortcut4 = QShortcut(QKeySequence("N"), self)
        shortcut1.activated.connect(button1.click)
        shortcut2.activated.connect(button2.click)
        shortcut3.activated.connect(button1.click)
        shortcut4.activated.connect(button2.click)

    def execute_and_close(self):
        self.YesClicked.emit()
        self.close()

    def set_default_and_close(self):
        self.NoClicked.emit()
        self.close()


class HelpPopup(QWidget):
    def __init__(self, keyboard_shortcuts):
        super().__init__()

        self.setWindowTitle("Help")
        layout = QVBoxLayout()
        self.setLayout(layout)

        label = QLabel("This program is designed to make manual evaluation of "
                       "data as painless as possible (i.e. it still sucks).\n")
        layout.addWidget(label)

        shortcuts_label = QLabel("Keyboard Shortcuts:")
        layout.addWidget(shortcuts_label)

        for shortcut in keyboard_shortcuts:
            shortcut_label = QLabel(f"{shortcut[0]}: {shortcut[2]}")
            layout.addWidget(shortcut_label)

        close_button = QPushButton("Close")
        close_button.clicked.connect(self.close)
        layout.addWidget(close_button)


class IntroWidget(QWidget):
    def __init__(self, targets=None, stage='cleaned', re_eval=False,
                 mjd_start=None, mjd_end=None, backends=[]):
        super().__init__()
        self.__setup()

        # Initialize variables
        self.stage = [stage]
        self.re_eval = re_eval
        self.mjd_start, self.mjd_end = mjd_start, mjd_end
        self.backends = backends
        self.initial_targets = targets

        # Set up widgets
        self.__add_widgets()

        # populate target list
        self._populate_target_table()

    def __setup(self):
        # Geometry arguments: x, y, width, height (all in px)
        # self.setGeometry(10, 10, 900, 700)
        self.setWindowTitle("Coast Guard Quality Control Launcher")
        self.Session = sessionmaker(database.get_engine())


    def __add_widgets(self):
        layout = QVBoxLayout()
        self.setLayout(layout)

        # Add target selector layout
        target_layout = QVBoxLayout()
        layout.addLayout(target_layout)
        title = QLabel("Target Selection")
        title.setStyleSheet("font-weight: bold; font: 25px Arial Bold;")
        title.setAlignment(Qt.AlignmentFlag.AlignCenter)
        target_layout.addWidget(title)

        # Add unselected target lists
        unselected_layout = QHBoxLayout()
        target_layout.addLayout(unselected_layout)
        self.unselected_targets = QListWidget()
        self.unselected_targets.setSelectionMode(
            QListWidget.SelectionMode.ExtendedSelection)
        unselected_label = QLabel("Unselected targets")
        unselected_layout.addWidget(unselected_label)
        unselected_layout.addWidget(self.unselected_targets)

        self.unselected_info = QTextEdit()
        unselected_layout.addWidget(self.unselected_info)
        self.unselected_targets.itemSelectionChanged.connect(
            self.update_single_info)

        # Add selected target list and info screen
        selected_layout = QHBoxLayout()
        target_layout.addLayout(selected_layout)
        self.selected_targets = QListWidget()
        if self.initial_targets is not None:
            self.selected_targets.addItems(self.initial_targets)
        self.selected_targets.setSelectionMode(
            QListWidget.SelectionMode.ExtendedSelection)
        selected_label = QLabel("Selected targets")
        selected_layout.addWidget(selected_label)
        selected_layout.addWidget(self.selected_targets)

        self.selected_info = QTextEdit()
        selected_layout.addWidget(self.selected_info)

        # Buttons for adding/removing targets
        add_button = QPushButton("Add >>")
        remove_button = QPushButton("<< Remove")
        add_button.clicked.connect(self.move_items_to_selected)
        remove_button.clicked.connect(self.move_items_to_unselected)
        layout.addWidget(add_button)
        layout.addWidget(remove_button)
        # Double-click events
        self.unselected_targets.itemDoubleClicked.connect(
            self.move_items_to_selected)
        self.selected_targets.itemDoubleClicked.connect(
            self.move_items_to_unselected)

        # DateSelector
        # Checkbox for enabling date input
        self.date_checkbox = QCheckBox("Use Date Range")
        self.date_checkbox.stateChanged.connect(self.toggle_date_selection)
        layout.addWidget(self.date_checkbox)

        # Start and end dates fields
        self.start_date_label = QLabel("Start Date:")
        layout.addWidget(self.start_date_label)
        self.start_date_edit = QDateEdit()
        self.start_date_edit.setDate(QDate.currentDate())
        self.start_date_edit.setCalendarPopup(True)
        self.start_date_edit.dateChanged.connect(self.handle_date_changed)
        layout.addWidget(self.start_date_edit)
        self.start_date_edit.setEnabled(False)

        self.end_date_label = QLabel("End Date:")
        layout.addWidget(self.end_date_label)
        self.end_date_edit = QDateEdit()
        self.end_date_edit.setDate(QDate.currentDate())
        self.end_date_edit.setCalendarPopup(True)
        self.end_date_edit.dateChanged.connect(self.handle_date_changed)
        layout.addWidget(self.end_date_edit)
        self.end_date_edit.setEnabled(False)

        # Buttons layout
        check_layout = QVBoxLayout()
        layout.addLayout(check_layout)

        # Checkbox for controlling target pipeline stages
        label = QLabel("Select target stage")
        check_layout.addWidget(label)

        self.stage_cleaned = QCheckBox("cleaned")
        self.stage_calibrated = QCheckBox("calibrated")
        self.stage_cleaned.setChecked('cleaned' in self.stage) # set initial state
        self.stage_calibrated.setChecked('calibrated' in self.stage)
        self.stage_cleaned.stateChanged.connect(self.handle_stage_toggle)
        self.stage_calibrated.stateChanged.connect(self.handle_stage_toggle)
        check_layout.addWidget(self.stage_cleaned)
        check_layout.addWidget(self.stage_calibrated)

        # Checkbox for controlling re-evaluation
        label = QLabel("Re-evaluate already controlled observations?")
        check_layout.addWidget(label)

        self.re_eval_button = QCheckBox("Re-eval")
        self.re_eval_button.setChecked(self.re_eval is True)
        self.re_eval_button.stateChanged.connect(self.handle_re_eval_toggle)
        check_layout.addWidget(self.re_eval_button)

        # Checkbox for choosing backend
        backend_label = QLabel("Backend")
        check_layout.addWidget(backend_label)
        self.backend_buttons = []
        self.__backend_button_create(check_layout)

        launch_button = QPushButton("Launch Quality Control")
        launch_button.clicked.connect(self.launch_quality_control)
        layout.addWidget(launch_button)

    def __backend_button_create(self, layout):
        """ Query the DB for all backends and create buttons for them all """
        with self.Session() as session:
            backends = session.scalars(
                select(Observation.backend).distinct()).all()

        first_button = True
        for backend in backends:
            button = QCheckBox(str(backend))
            self.backend_buttons.append(button)
            layout.addWidget(button)
            # connect button to the correct hander
            button.stateChanged.connect(self.handle_backend_toggle)
            # If this is the first button, toggle it on
            if first_button:
                button.setChecked(True)
                # self.backends.append(button.text())
                first_button = False

    def _populate_target_table(self):
        """ Fetch a list of files that need manual control """
        # Clear preivious list
        self.unselected_targets.clear()

        if self.re_eval:
            whereclause = (File.status == 'failed') & (Qctrl.qcpassed == False)
        else:    
            whereclause = (File.status == 'awaiting_qctrl')

        if 'cleaned' in self.stage and 'calibrated' not in self.stage:
            whereclause &= (File.stage == 'cleaned')
        elif 'calibrated' in self.stage and 'cleaned' not in self.stage:
            whereclause &= (File.stage == 'calibrated') & \
                           (Observation.obstype == 'pulsar')
        else:
            whereclause &= or_((File.stage == 'calibrated') &
                               (Observation.obstype == 'pulsar'),
                               (File.stage == 'cleaned'))

        if self.mjd_start is not None and self.mjd_end is not None:
            whereclause &= (Observation.start_mjd.between(self.mjd_start,
                                                          self.mjd_end))

        if self.backends is not None:
            whereclause &= (Observation.backend.in_(self.backends))

        with self.Session() as session:
            query = select(File.file_id, File.filename,
                           Observation.sourcename, Observation.start_mjd)\
                            .join(Observation,
                                  File.obs_id == Observation.obs_id)\
                            .join(Qctrl, Qctrl.file_id == File.file_id,
                                  isouter=True)\
                            .where(whereclause)\
                            .order_by(Observation.obstype.asc())
            self.files = session.execute(query).all()

        # Set new unselected target list
        target_names = sorted(set([file[2] for file in self.files]))
        self.unselected_targets.addItems(target_names)

        # Update selected target list
        self.__sync_tables()

    def __sync_tables(self):
        """ Remove selected targets that aren't in the unselected target list
        """
        unselected_items = set(self.unselected_targets.item(i).text() for i in
                        range(self.unselected_targets.count()))
        selected_items = set(self.selected_targets.item(i).text() for i in
                range(self.selected_targets.count()))

        for i in range(self.selected_targets.count() -1, -1, -1):
            item = self.selected_targets.item(i).text()
            if item not in unselected_items:
                self.selected_targets.takeItem(i)

        for i in range(self.unselected_targets.count() -1, -1, -1):
            item = self.unselected_targets.item(i).text()
            if item in selected_items:
                self.unselected_targets.takeItem(i)

        self.update_selected_info()

    def toggle_date_selection(self):
        """handle toggling the DateEdit fields on and off"""
        if self.date_checkbox.isChecked():
            self.start_date_edit.setEnabled(True)
            self.end_date_edit.setEnabled(True)
            self._set_minmax_dates()  
            self.handle_date_changed()
        else:
            self.start_date_edit.setEnabled(False)
            self.end_date_edit.setEnabled(False)
            self.mjd_start, self.mjd_end = None, None
            self._populate_target_table()

    def _set_minmax_dates(self):
        """ find the min and max dates in self.files and populate the DateEdit
        fields """

        dates = [rsutils.mjd_to_datetime(file[3]) for file in self.files]
        pad = timedelta(days=1)
        if dates:
            max_date, min_date = max(dates)+pad, min(dates)-pad
        else:
            max_date, min_date = datetime.now(), datetime.now()
        self.start_date_edit.setDate(QDate(min_date.year, min_date.month,
                                            min_date.day))
        self.end_date_edit.setDate(QDate(max_date.year, max_date.month,
                                            max_date.day))

    def handle_date_changed(self):
        """ When the  DateEdit fields change, requery the database and update
        the tables """
        start_date_qdate = self.start_date_edit.date()
        start_date_datetime = datetime(start_date_qdate.year(),
                                       start_date_qdate.month(),
                                       start_date_qdate.day())
        self.mjd_start = rsutils.datetime_to_mjd(start_date_datetime)

        end_date_qdate = self.end_date_edit.date()
        end_date_datetime = datetime(end_date_qdate.year(),
                                     end_date_qdate.month(),
                                     end_date_qdate.day())
        self.mjd_end = rsutils.datetime_to_mjd(end_date_datetime)

        # Re-populate tables
        self._populate_target_table()

    def handle_stage_toggle(self):
        """ When the QCheckboxes for pipeline state are pressed, requrey the
        DB and update the tables"""

        sender = self.sender()
        stage = sender.text()
        if sender.isChecked():
            if stage not in self.stage:
                self.stage.append(stage)
        else:
            if stage in self.stage and len(self.stage) > 1:
                self.stage.remove(stage)

        # Stop unchecking if there is only one button pressed
        if not self.stage_cleaned.isChecked() and not\
           self.stage_calibrated.isChecked():
            QTimer.singleShot(0, lambda:
                              self.__ensure_one_stage_checked(sender, stage))

        # Re-populate tables
        self._populate_target_table()

    def __ensure_one_stage_checked(self, checkbox, stage):
        """ Force one stage checkbox to be checked at all times"""
        checkbox.blockSignals(True)
        checkbox.setChecked(True)
        checkbox.blockSignals(False)
        if stage not in self.stage:
            self.stage.append(stage)

    def handle_re_eval_toggle(self, checked):
        """ When the re-eval button is pressed, requery the DB and repopulate
        the tables"""

        self.re_eval = True if checked else False
        # re-populate target table
        self._populate_target_table()

    def handle_backend_toggle(self):
        sender = self.sender()
        backend = sender.text()
        if sender.isChecked():
            if backend not in self.backends:
                self.backends.append(backend)
        else:
            if backend in self.backends and len(self.backends) > 1:
                self.backends.remove(backend)

        # Stop unchecking if there is only one button pressed
        if not any([button.isChecked() for button in self.backend_buttons]):
            QTimer.singleShot(0, lambda:
                              self.__ensure_one_backend_checked(sender,
                                                                backend))

        # Re-populate tables
        self._populate_target_table()

    def __ensure_one_backend_checked(self, checkbox, backend):
        """ Force one backend checkbox to be checked at all times"""
        checkbox.blockSignals(True)
        checkbox.setChecked(True)
        checkbox.blockSignals(False)
        if backend not in self.backends:
            self.backends.append(backend)

    def move_items_to_selected(self, item=None):
        """ Move an item from the unselected table to the selected table"""

        if not item:
            selected_items = self.unselected_targets.selectedItems()
        else:
            selected_items = [item]
        for item in selected_items:
            self.unselected_targets.takeItem(self.unselected_targets.row(item))
            self.selected_targets.addItem(item.text())

        # Update the info of the selected targets
        self.update_selected_info()

    def move_items_to_unselected(self, item=None):
        """ Move an item from the selected table to the unselected table"""

        if not item:
            selected_items = self.selected_targets.selectedItems()
        else:
            selected_items = [item]
        for item in selected_items:
            self.selected_targets.takeItem(self.selected_targets.row(item))
            self.unselected_targets.addItem(item.text())

        # Update the info of the selected targets
        self.update_selected_info()

    def update_single_info(self):
        selected = [item.text() for item in
                    self.unselected_targets.selectedItems()]
        if not selected:
            self.unselected_info.clear()
            return
        targets_info = [file for file in self.files if file[2] in
                        selected]
        dates = [rsutils.mjd_to_datetime(row[3]) for row in targets_info]
        info_str = f"No. of files to check = {len(targets_info)}\n"\
            f"Earliest observation = {min(dates)}\n"\
            f"Latest observation = {max(dates)}\n"
        self.unselected_info.setText(info_str)

    def update_selected_info(self):
        members = [self.selected_targets.item(i).text() for i in
                   range(self.selected_targets.count())]
        if not members:
            self.selected_info.clear()
            return
        members_info = [file for file in self.files if file[2] in
                        members]
        dates = [rsutils.mjd_to_datetime(float(row[3])) for row in members_info]

        info_str = f"No. of files to check = {len(members_info)}\n"\
            f"Earliest observation = {min(dates)}\n"\
            f"Latest observation = {max(dates)}\n"
        self.selected_info.setText(info_str)

    def launch_quality_control(self):
        priorities = [self.selected_targets.item(i).text() for i in 
                      range(self.selected_targets.count())]
        quality_control_widget = QualityControl(
            priorities=priorities, stage=self.stage,
            re_eval=self.re_eval, mjd_start=self.mjd_start,
            mjd_end=self.mjd_end)
        quality_control_widget.show()
        self.close()


class CreateDiagnosticsThread(QThread):
    """_summary_

    Args:
        QThread (_type_): _description_
    """
    finished = pyqtSignal()
    cancelled = pyqtSignal()
    progress = pyqtSignal(int, int) 

    def __init__(self, files):
        super().__init__()
        self.files = files
        self._is_canceled = False
        # Establish a sessionmaker
        self.Session = sessionmaker(database.get_engine())

    def run(self):
        """ Create new diagnostics and load them to the DB.
        """
        with self.Session() as session:
            total_files = len(self.files)
            for inx, file in enumerate(self.files):

                if self._is_canceled:
                    # Kill the process
                    self.canceled.emit()
                    session.rollback()
                    return  

                self.progress.emit(inx, total_files)

                arfn = os.path.join(file.filepath, file.filename)
                arf = utils.ArchiveFile(arfn)
                fullresfn, lowresfn = diagnose.make_summary_plots(arf)
                diagvals = [
                    {'diagnosticpath': os.path.dirname(fullresfn),
                        'diagnosticname': os.path.basename(fullresfn)},
                    {'diagnosticpath': os.path.dirname(lowresfn),
                        'diagnosticname': os.path.basename(lowresfn)}
                            ]

                for diagval in diagvals:
                    diagval['file_id'] = file.file_id
                    new_diagnostic = insert(Diagnostic).values(diagval)
                    session.execute(new_diagnostic)
                session.commit()
        self.finished.emit()
        
        # Emit final progress update
        self.progress.emit(total_files, total_files)

    def cancel(self):
        self._is_canceled = True


class QualityControl(QtWidgets.QWidget):
    """Quality control window.
    """

    valueChanged = pyqtSignal(str)

    def __init__(self, priorities=None, stage='cleaned', re_eval=False,
                 mjd_start=None, mjd_end=None, backend=None):
        super(QualityControl, self).__init__()
        # Set up the window
        self.__setup()
        self.__set_keyboard_shortcuts()
        self.__add_widgets()

        # Establish a sessionmaker
        self.Session = sessionmaker(database.get_engine())

        # Initialize
        self.priorities = priorities
        self.stage = stage
        self.re_eval = re_eval
        self.idiag = 0
        self.mjd_start, self.mjd_end = mjd_start, mjd_end
        self.backend = backend
        self.file_id = None
        self.diagplots = []
        self.action_history = []

    def __setup(self):
        # Geometry arguments: x, y, width, height (all in px)
        self.setGeometry(10, 10, 900, 700)
        self.setWindowTitle("Coast Guard Quality Control")

    def __set_keyboard_shortcuts(self):
        self.shortcuts = [
            ("Space", self.cycle_diag_fwd, "Next diagnostic"),
            ("Shift+Space", self.cycle_diag_rev, "Preivious diagnostic"),
            ("A", self.set_file_as_good, "Mark observation as good (passed "
             "qctrl)"),
            ("S", lambda: self.set_file_as_good(reason='weak'), "Mark "
             "observation as weak (passed qctrl)"),
            ("D", lambda: self.set_file_as_good(reason='ephem'), "Mark "
             "observation as needing an ephemeris update (passed qctrl)"),
            ("Z", self.set_file_as_bad, "Mark observation as bad due to "
             "RFI (failed qctrl)"),
            ("X", lambda: self.set_file_as_bad(reason="nondetect"), "Mark "
             "observation as a non-detection (failed qctrl)"),
            ("V", self.zap_file_manually, "Zap file using pazi"),
            ("V+Shift", lambda: self.zap_file_manually(reset_weights=True),
             "Zap file using pazi (set channel weigths to uniform)"),
            ("G", self.skip_file, "Skip to next file"),
            ("W", self.create_queue, "Reload observation queue"),
            ("Q", self.close, "Quit"),
            ("L", self.launch_launcher, "Quit and relaunch"),
            ("E", self.add_parents_diags, "Add diagnostic plots of parent "
             "file(s)."),
            ("P", self.revert_last_action, "Revert last action")
        ]

        for shortcut, function, _ in self.shortcuts:
            QtGui.QShortcut(QtGui.QKeySequence(shortcut), self, function)

    def __add_widgets(self):
        self.image_holder = QtWidgets.QLabel()
        self.plot_lbl = QtWidgets.QLabel()

        prev_button = QtWidgets.QPushButton("Prev. Diagnostic Plot")
        prev_button.clicked.connect(self.cycle_diag_rev)
        next_button = QtWidgets.QPushButton("Next Diagnostic Plot")
        next_button.clicked.connect(self.cycle_diag_fwd)

        good_button = QtWidgets.QPushButton("Good(A)")
        good_button.setStyleSheet("background-color: green;")
        good_button.clicked.connect(self.set_file_as_good)
        weak_button = QtWidgets.QPushButton("Weak(S)")
        weak_button.setStyleSheet("background-color: green;")
        weak_button.clicked.connect(lambda: self.set_file_as_good(reason='weak'))
        bad_button = QtWidgets.QPushButton("Bad(Z)")
        bad_button.setStyleSheet("background-color: red;")
        bad_button.clicked.connect(lambda: self.set_file_as_bad())
        ephem_button = QtWidgets.QPushButton("Bad Ephem(D)")
        ephem_button.setStyleSheet("background-color: green;")
        ephem_button.clicked.connect(lambda: self.set_file_as_good(reason='ephem'))
        nodetect_button = QtWidgets.QPushButton("No Detect(X)")
        nodetect_button.setStyleSheet("background-color: red;")
        nodetect_button.clicked.connect(lambda: self.set_file_as_bad(reason='nondetect'))
        zap_button = QtWidgets.QPushButton("Zap(V)")
        zap_button.clicked.connect(self.zap_file_manually)
        skip_button = QtWidgets.QPushButton("Skip(G)")
        skip_button.clicked.connect(self.skip_file)
        reload_button = QtWidgets.QPushButton("Reload(W)")
        reload_button.clicked.connect(lambda: self.create_queue())
        priority_button = QtWidgets.QPushButton("New Qctrl(L)")
        priority_button.clicked.connect(self.launch_launcher)
        addparents_button = QtWidgets.QPushButton("Add plots(E)")
        addparents_button.clicked.connect(self.add_parents_diags)
        revert_button = QtWidgets.QPushButton("Revert last action(P)")
        revert_button.clicked.connect(self.revert_last_action)

        self.help_popup = HelpPopup(self.shortcuts)
        help_button = QPushButton("Help")
        help_button.clicked.connect(self.__show_help_popup)

        # Counter for the number of plots left
        self.lcd = QtWidgets.QLCDNumber(4)
        self.lcd.setSegmentStyle(QtWidgets.QLCDNumber.SegmentStyle.Flat)

        plotctrl_box = QtWidgets.QHBoxLayout()
        plotctrl_box.addWidget(prev_button)
        plotctrl_box.addStretch(1)
        plotctrl_box.addWidget(self.plot_lbl)
        plotctrl_box.addStretch(1)
        plotctrl_box.addWidget(next_button)

        left_box = QtWidgets.QVBoxLayout()
        left_box.addWidget(self.image_holder)
        left_box.addStretch(1)
        left_box.addLayout(plotctrl_box)

        right_box = QtWidgets.QVBoxLayout()

        # Add the good/bad buttons
        right_box.addWidget(good_button)
        right_box.addWidget(weak_button)
        right_box.addWidget(ephem_button)
        right_box.addWidget(bad_button)
        right_box.addWidget(nodetect_button)
        right_box.addWidget(zap_button)
        right_box.addWidget(skip_button)
        right_box.addWidget(reload_button)
        right_box.addWidget(priority_button)
        right_box.addWidget(addparents_button)
        right_box.addWidget(revert_button)
        right_box.addWidget(help_button)

        # Add spacing or stretch as needed between the buttons and the QLCDNumber
        right_box.addStretch(2)

        # Add the QLCDNumber to the bottom of the right_box
        right_box.addWidget(QLabel("Num left:"))
        right_box.addWidget(self.lcd)


        main_box = QtWidgets.QHBoxLayout()
        main_box.addLayout(left_box)
        main_box.addLayout(right_box)

        self.setLayout(main_box)

    def showEvent(self, event):
        # Runs when .show() is called
        self.create_queue()

    def __show_help_popup(self):
        self.help_popup.show()

    def set_file_as_good(self, reason='good'):
        """Mark file as good in the DB

        Args:
            note (str, optional): The note to be uploaded to the DB. 
            Defaults to None.
        """
        if self.file_id:
            if reason == 'weak':
                note = "A weak detection."
                qcstatus = "weak"
            elif reason == 'ephem':
                note = "Ephemeris needs to be updated."
                qcstatus = "ephem"
            elif reason == "good":
                note = "Passed QC"
                qcstatus = "good"

            # Define values to be inserted into the qctrl table
            qctrl_values = {"file_id": self.file_id, "user": os.getlogin(),
                            "qcpassed": True, "note": note,
                            "status": qcstatus, "added": datetime.now(),
                            "last_modified": datetime.now()}

            with self.Session() as session:
                now = datetime.now()
                file_update = update(File)\
                    .where(File.file_id == self.file_id)\
                    .values(status="processed", note=note, last_modified=now)
                session.execute(file_update)

                new_qctrl = insert(Qctrl).values(qctrl_values)
                update_vals = {k: new_qctrl.inserted[k] for k in
                               qctrl_values.keys() if k != 'file_id'}
                new_qctrl = new_qctrl.on_duplicate_key_update(**update_vals)
                session.execute(new_qctrl)
                session.commit()

            # Add file to history
            self.action_history.append({'file_id': self.file_id,
                                        'action': 'good'})
            
            self.advance_file()

    def set_file_as_bad(self, reason='rfi'):
        """Mark file as bad in the DB

        Args:
            reason (str, optional): The reason why the file is bad. This will
            be uploaded to the DB in the note filed. Defaults to rfi.
        """
        if self.file_id:
            note = "File failed quality control."
            if reason == 'rfi':
                note += " RFI has rendered the observation unsalvageable!"
                qcstatus = "RFI"
            elif reason == 'nondetect':
                note += " The observation is a non-detection."
                qcstatus = "non_detection"
            else:
                raise errors.UnrecognizedValueError("The reason for "
                                                    "setting the file as "
                                                    "bad is not "
                                                    f"recognized: {reason}")

            # Define values to be inserted into the qctrl table
            qctrl_values = {"file_id": self.file_id, "user": os.getlogin(),
                            "qcpassed": False, "note": note,
                            "status": qcstatus, "added": datetime.now(),
                            "last_modified": datetime.now()}
            
            with self.Session() as session:
                now = datetime.now()
                file_update = update(File).where(File.file_id == self.file_id)\
                    .values(status='failed', note=note, last_modified=now)
                session.execute(file_update)

                new_qctrl = insert(Qctrl).values(qctrl_values)
                update_vals = {k: new_qctrl.inserted[k] for k in
                               qctrl_values.keys() if k != 'file_id'}
                new_qctrl = new_qctrl.on_duplicate_key_update(**update_vals)
                session.execute(new_qctrl)
                session.commit()

            # Add file to history
            self.action_history.append({'file_id': self.file_id,
                                        'action': 'bad'})

            self.advance_file()

    def skip_file(self):
        self.action_history.append({'file_id': self.file_id, 'action': 'skip'})     
        self.advance_file()

    def advance_file(self):
        """Go to next file in queue"""
        self.idiag = 0
        if self.files_to_check:
            file_id = self.files_to_check.pop()
            self.set_file(file_id)
            # Decrement number of files left
            self.lcd.display(len(self.files_to_check)+1)
        else:
            self.file_id = None
            self.diagplots = []
            self.image_holder.setText("No files in queue...")
            self.lcd.display(0)

    def cycle_diag_fwd(self):
        """Go to next diagnostic """
        if self.diagplots:
            self.idiag = (self.idiag + 1) % len(self.diagplots)
            self.display_file()

    def cycle_diag_rev(self):
        """Go to prev diagnostic """
        if self.diagplots:
            self.idiag = (self.idiag - 1) % len(self.diagplots)
            self.display_file()

    def display_file(self):
        """ Display diagnostics """
        diagfn = self.diagplots[self.idiag]
        image = QtGui.QPixmap(diagfn)
        self.image_holder.setPixmap(image)
        # Display text information
        self.plot_lbl.setText(os.path.basename(diagfn))

    def set_file(self, file_id):
        """Find the diagnostics of a file and load them into the widget"""
        if self.file_id != file_id:
            with self.Session() as session:
                # Fetch a file and the corresponding diagnostics
                files = session.scalars(select(File)
                                        .where(File.file_id == file_id))\
                        .all()
                if len(files) != 1:
                    raise errors.DatabaseError(f"Bad number of rows "
                                               f"({len(files)}) with "
                                               f"file_id={file_id}!")
                file = files[0]
                diagnostics =\
                    session.scalars(select(Diagnostic)
                                    .where(Diagnostic.file_id == file_id))\
                    .all()

                # Populate instance varaibles
                self.diagplots = [os.path.join(diag.diagnosticpath,
                              diag.diagnosticname) for diag in diagnostics]

                # If there are no diagnostic plots, ask the user if he wants
                # to create them
                if len(diagnostics) == 0 and file is not None:
                    popup = MissingSingleDiagnosticPopup()
                    popup.YesClicked.connect(
                        lambda: self._set_new_diagplots(file))
                    popup.NoClicked.connect(self._set_default_image)
                    popup.exec()


            self.idiag = 0
            self.file_id = file_id
            self.file = file
            self.added_parent_plots = False
            self.added_cal_plots = False
            self.display_file()

    def _set_new_diagplots(self, file):
        """Given a file, create new diagnostics and fetch them from the DB"""
        self.create_missing_diagnostics([file])
        with self.Session() as session:
                # Fetch the new diagnostics
                diagnostics =\
                    session.scalars(select(Diagnostic)
                                    .where(Diagnostic.file_id == file.file_id))\
                    .all()
        # Populate instance variable
        self.diagplots = [os.path.join(diag.diagnosticpath,
                          diag.diagnosticname) for diag in diagnostics]

    def _set_default_image(self):
        """ Set diag to the default image """
        self.diagplots = [config.default_image]

    def write_filename(self):
        if self.file_id is not None:
            print(os.path.join(self.file.filepath, self.file.filename))

    def add_parents_diags(self):
        """ Add the diagnostics of the parent file"""
        if not self.added_parent_plots and self.file_id is not None:
            if self.file.parent_file_id is not None:
                with self.Session() as session:
                    parent_diags =\
                        session.scalars(select(Diagnostic)\
                                        .where(Diagnostic.file_id ==
                                               self.file.parent_file_id)).all()
                if len(parent_diags) == 0:
                    raise warnings.warn("No diagnostics for file (ID: {:d}) "
                                        "'{:s}'!".format(
                                            self.file.file_id, os.path.join(
                                                self.file.filepath,
                                                self.file.filename)),
                                        errors.CoastGuardWarning)

                self.diagplots.extend([os.path.join(diag.diagnosticpath,
                                                    diag.diagnosticname)
                                       for diag in parent_diags])
            self.added_parents_plots = True

    def add_cal_diags(self):
        """ Add the diagnostics of the calibrator file"""
        if not self.added_cal_plots and self.file_id is not None:
            cal_file_id = self.file.cal_file_id
            if cal_file_id is not None:
                with self.Session() as session:
                    cal_diags = session.scalars(select(Diagnostic)\
                        .where(Diagnostic.file_id == cal_file_id)).all()
                if len(cal_diags) == 0:
                    raise warnings.warn("No diagnostics for file (ID: "
                                        "{:d}) '{:s}'!"
                                        .format(self.file.file_id,
                                                os.path.join(
                                                    self.file.filepath,
                                                    self.file.filename
                                                    )),
                                        errors.CoastGuardWarning)
                self.diagplots.extend([os.path.join(diag.diagnosticpath,
                                                    diag.diagnosticname)
                                       for diag in cal_diags])
            self.added_cal_plots = True

    def create_queue(self, priorities=None, stage=None, re_eval=None,
                     mjd_start=None, mjd_end=None, backend=None):
        """Create a queue of files to be shown in the widget.

        Args:
            priorities (list, optional): Priority list defining the targets to
            prioritize. Defaults to None (load all availible files from the
            DB).
            stage (str, optional): The stage of the files to be controlled. Can
            be 'calibrated' or 'cleaned'. Defaults to 'cleaned'.
            re_eval (bool, optional): Reavaluate all files with status new,
            even if they preiviously failed quality control. Defaults to False.
        """

        if stage is None:
            stage = self.stage
        if re_eval is None:
            re_eval = self.re_eval
        if priorities is None:
            priorities = self.priorities
        if mjd_start is None:
            mjd_start = self.mjd_start
        if mjd_end is None:
            mjd_end = self.mjd_end
        if backend is None:
            backend = self.backend
            
        whereclause = (File.status == 'awaiting_qctrl')
        if 'cleaned' in stage and 'calibrated' not in stage:
            whereclause &= (File.stage == 'cleaned')
        elif 'calibrated' in stage and 'cleaned' not in stage:
            whereclause &= (File.stage == 'calibrated') & \
                           (Observation.obstype == 'pulsar')
        else:
            whereclause &= or_((File.stage == 'calibrated') &
                               (Observation.obstype == 'pulsar'),
                               (File.stage == 'cleaned'))

        if self.re_eval:
            whereclause = (File.status == 'failed') & (Qctrl.qcpassed == False)
        else:    
            whereclause = (File.status == 'awaiting_qctrl')

        if priorities:
            whereclause &= (Observation.sourcename.in_(priorities))

        if mjd_start is not None and mjd_end is not None:
            whereclause &= (Observation.start_mjd.between(mjd_start,
                                                          mjd_end))

        if backend is not None:
            whereclause &= (Observation.backend.in_(backend))

        with self.Session() as session:
            query = select(File).join(Observation,
                                      File.obs_id == Observation.obs_id)\
                .join(Qctrl, Qctrl.file_id == File.file_id, isouter=True)\
                .where(whereclause)\
                .order_by(Observation.obstype.asc())
            files = session.scalars(query).all()
        self.files_to_check = [file.file_id for file in files]
        self.check_diagnostics()
        self.advance_file()

    def check_diagnostics(self):
        # Check if diagnostics exist for all observations in the loaded queue
        # If any are missing, create new diagnostics

        with self.Session() as session:
            # Fetch file_ids for all files with missing diagnostics
            missing_diags =\
                  session.scalars(select(File)
                                  .outerjoin(Diagnostic,
                                             File.file_id == Diagnostic.file_id)
                          .where(and_(File.file_id.in_(self.files_to_check),
                                      Diagnostic.file_id.is_(None)))
                                      ).all()

        if len(missing_diags) != 0:
            # Create pop-up
            popup = MissingManyDiagnosticsPopup(
                create_diag=partial(
                    self.create_missing_diagnostics, missing_diags))
            popup.exec()

    def create_missing_diagnostics(self, files):
        total_files = len(files)
        self.progress_dialog = QProgressDialog("Creating missing diagnostics",
                                               "Cancel", 0, total_files, self)
        self.progress_dialog.setWindowTitle("Processing")
        self.progress_dialog.setModal(True)
        self.progress_dialog.canceled.connect(self.cancel_progress_dialog)

        # Start the worker thread
        self.diag_thread = CreateDiagnosticsThread(files)
        self.diag_thread.progress.connect(self.update_progress_dialog)
        self.diag_thread.finished.connect(self.create_diags_thread_finished)
        self.diag_thread.cancelled.connect(self.cancel_create_diags_thread)
        self.diag_thread.start()

        self.progress_dialog.exec() # Block main thread
        
    def create_diags_thread_finished(self):
        # Close the progress dialog
        self.progress_dialog.close()
        self.diag_thread.deleteLater()

    def cancel_create_diags_thread(self):
        # Cancel progress dialog
        self.diag_thread.close()

    def cancel_progress_dialog(self):
        # Cancel the worker thread
        self.progress_dialog.cancel() 
        self.diag_thread.deleteLater()

    def update_progress_dialog(self, current, total):
        self.progress_dialog.setLabelText(f"Creating diagnostic {current} "
                                          f"of {total}")
        self.progress_dialog.setValue(current)

    def zap_file_manually(self, reset_weights=False):
        
        """ Start a pazi session for manual zapping
        Args:
            reset_weights (bool, optional): Reset channel weights.
            Defaults to False.
        """
        arfn = os.path.join(self.file.filepath,
                            self.file.filename)
        zapdialog = ZappingDialog()
        zapdialog.show() # This blocks input to the main quality control window
        
        out = zapdialog.zap(arfn, reset_weights)
        if out is not None and os.path.isfile(out):
            # Successful! Insert entry into DB.
            outdir, outfn = os.path.split(out)
            values = {'filepath': outdir,
                      'filename': outfn,
                      'stage': 'cleaned',
                      'note': "Manually zapped",
                      'status': 'awaiting_qctrl',
                      'snr': utils.get_archive_snr(out),
                      'md5sum': utils.get_md5sum(out),
                      'coords': self.file.coords,
                      'ephem_md5sum': self.file.ephem_md5sum,
                      'filesize': os.path.getsize(out),
                      'parent_file_id': self.file_id}

            # If the parent file is on the calibrate stage, so must the child.
            # It also inherits the cal file of the parent.
            if self.file.stage == 'calibrated':
                values['stage'] = 'calibrated'
                values['cal_file_id'] = self.file.cal_file_id

            version_id = utils.get_version_id(self.Session)
            with self.Session() as session:
                # Insert new entry
                values['version_id'] = version_id
                values['obs_id'] = self.file.obs_id
                new_file_insert = insert(File).values(values)
                new_file_id = session.execute(new_file_insert)\
                    .inserted_primary_key[0]

                # Update parent file's entry
                update_file = update(File).where(File.file_id == self.file_id)\
                    .values(qcpassed=False,
                            status='replaced',
                            note="File had to be cleaned by hand.",
                            last_modified=datetime.now())
                session.execute(update_file)
                session.commit()

            # Add file to history
            self.action_history.append({'file_id': self.file_id,
                                        'action': 'zap'})

            self.advance_file()

    def revert_last_action(self):
        if not self.action_history:
            print("No actions to revert")
            return
        
        last_action = self.action_history.pop()
        old_file_id = last_action['file_id']
        current_file_id = self.file_id

        if not last_action['action'] == 'skip':
            # For non-skips, the database must be updated to recover the
            # preivious state
            with self.Session() as session:
                # set the current file to deleted
                current_file_update = update(File).where(
                    File.file_id == current_file_id)\
                        .values({"is_deleted": True})
                session.execute(current_file_update)        
        
                # revert the old file to a pre-qctrl status
                old_file_update = update(File)\
                    .where(File.file_id == old_file_id)\
                    .values({'status': 'awaiting_qctrl', "note": "",
                            'last_modified': datetime.now()})
                session.execute(old_file_update)
                session.commit()

        self.files_to_check.append(old_file_id)
        self.advance_file()

    def launch_launcher(self):
        self.intro_widget = IntroWidget(
            targets=self.priorities, stage=self.stage,
            re_eval=self.re_eval, mjd_start=self.mjd_start,
            mjd_end=self.mjd_end)
        self.intro_widget.show()
        self.close()


class ZappingDialog(QDialog):
    def __init__(self):
        super(ZappingDialog, self).__init__()
        # Set up with dialog
        self.__setup()
        self.__add_widgets()
        self.activateWindow()
        self.raise_()

    def __setup(self):
        self.setWindowTitle("Zapping...")
        self.setModal(True)
        self.setVisible(True)

    def __add_widgets(self):
        lbl = QLabel()
        lbl.setText("<b>Running psrzap...</b>")
        self.textedit = QPlainTextEdit()
        self.textedit.setReadOnly(True)
        self.textedit.setFont(QFont("Courier"))

        main_layout = QVBoxLayout()
        main_layout.addWidget(lbl)
        main_layout.addWidget(self.textedit)
        self.setLayout(main_layout)

    def __on_finish(self, exitcode):
        msg_dialog = QtWidgets.QMessageBox()
        # print(self.infn)
        # print(self.outfn)
        if not os.path.exists(self.outfn):
            success = False
            self.done(success)
        insize = os.path.getsize(self.infn)
        outsize = os.path.getsize(self.outfn)
        if exitcode != 0:
            success = False
            msg_dialog.setText("Zapping failed!")
        elif not outsize:
            success = False
            msg_dialog.setText("Output zapped file is empty.")
            msg_dialog.setInformativeText("Did you forget to save?")
            msg_dialog.setStandardButtons(QtWidgets.QMessageBox.StandardButton.Yes | QtWidgets.QMessageBox.StandardButton.No)
            msg_dialog.setDefaultButton(QtWidgets.QMessageBox.StandardButton.Yes)
        elif outsize != insize:
            success = True
            msg_dialog.setText("Output zapped file's size (%d) is "
                            "different than input file's size "
                            "(%s: %d bytes)" %
                            (outsize, self.infn, insize))
            msg_dialog.setInformativeText("Save zapped file?")
            msg_dialog.setStandardButtons(QtWidgets.QMessageBox.StandardButton.Save | QtWidgets.QMessageBox.StandardButton.Discard)
            msg_dialog.setDefaultButton(QtWidgets.QMessageBox.StandardButton.Discard)
        else:
            success = True
            msg_dialog.setText("The archive has been zapped.")
            msg_dialog.setInformativeText("Save zapped file?")
            msg_dialog.setStandardButtons(QtWidgets.QMessageBox.StandardButton.Save | QtWidgets.QMessageBox.StandardButton.Discard)
            msg_dialog.setDefaultButton(QtWidgets.QMessageBox.StandardButton.Save)
        ret = msg_dialog.exec()
        if not success:
            pass
        elif ret == QtWidgets.QMessageBox.StandardButton.Save:
            pass
        elif ret == QtWidgets.QMessageBox.StandardButton.Discard:
            success = False
        else:
            raise ValueError("Value returned by message dialog (%d) "
                            "does not match the Save or Discard "
                            "buttons!" % ret)
        self.done(success)


    def __on_stderr(self):
        stderr_data = self.proc.readAllStandardError()
        text = QByteArray(stderr_data).data().decode()
        self.textedit.appendPlainText(text)

    def __on_stdout(self):
        stdout_data = self.proc.readAllStandardOutput()
        text = QByteArray(stdout_data).data().decode()
        self.textedit.appendPlainText(text)

    def zap(self, arfn, reset_weights=False):
        self.setWindowTitle("Zapping {:s}...".format(os.path.basename(arfn)))
        # Create temporary file for output
        tmpdir = os.path.join(config.tmp_directory, 'qctrl') 
        if not os.path.exists(tmpdir):
            os.makedirs(tmpdir)
        tmpfile, tmpoutfn = tempfile.mkstemp(suffix=".ar", dir=tmpdir)
        os.close(tmpfile)  # Close open file handle
        try:
            print("arfn", os.path.basename(arfn))
            print("tmpoutfn", tmpoutfn)
            success = self.__launch_zapping(arfn, reset_weights)
            if success:
                arf = utils.ArchiveFile(arfn)
                archivedir = os.path.join(config.output_location,
                                          config.output_layout) % arf
                # Append .zap to filename
                archivefn = (os.path.basename(arfn)+".pazi") % arf
                outfn = os.path.join(archivedir, archivefn)
                print("outfn", outfn)
                # Ensure group has write permission to this file
                # NOT SURE THIS IS NECESSARY
                #utils.add_group_permissions(tmpoutfn, 'w')
#                shutil.move(tmpoutfn, outfn)
                return outfn
            else:
                return None
        finally:
            if os.path.exists(tmpdir):
                shutil.rmtree(tmpdir)

    def __launch_zapping(self, infn, reset_weights=False):
        self.infn = infn

        arf = utils.ArchiveFile(infn)
        archivedir = os.path.join(config.output_location,
                                  config.output_layout) % arf
        archivefn = (os.path.basename(infn)+".pazi") % arf
        self.outfn = os.path.join(archivedir, archivefn)
        print("self.outfn inside __launch_zapping", self.outfn)

        self.proc = QtCore.QProcess()
        self.proc.readyReadStandardOutput.connect(self.__on_stdout)
        self.proc.readyReadStandardError.connect(self.__on_stderr)
        if reset_weights:
            self.__on_stdout("Resetting profile weights")
            tmpfn = self.outfn + ".in"
            shutil.copy(self.infn, tmpfn)
            self.infn = tmpfn
            self.proc.start('pam', ['-m', '-w', '1', self.infn])
            self.proc.waitForFinished()  # Block until finished
            self.__on_stdout("Done")

        self.proc.finished.connect(self.__on_finish)
        self.proc.start('pazi', [self.infn])
        success = self.exec()
        print(success)
        return success


def main():
    app = QtWidgets.QApplication(sys.argv)

    if args.launcher:
        intro_widget = IntroWidget(stage=args.stage, re_eval=args.re_eval,
                                   mjd_start=args.mjd_start,
                                   mjd_end=args.mjd_end,
                                   backends=args.backends)
        intro_widget.show()
    else:
        qctrl_win = QualityControl(priorities=args.targets, stage=args.stage,
                                   re_eval=args.re_eval,
                                   mjd_start=args.mjd_start,
                                   mjd_end=args.mjd_end,
                                   backends=args.backends)
        # Display the window
        qctrl_win.show()

    exitcode = app.exec()
    sys.exit(exitcode)


if __name__ == "__main__":
    parser = utils.DefaultArguments(description="Quality control interface "
                                    "for Asterix data.")
    parser.add_argument("-T", "--targets", action='append',
                        default=[], dest='targets',
                        help="Names of the target pulsars.")
    parser.add_argument('--start-date', type=str, default=None,
                        action=rsactions.ParseDate, dest='mjd_start',
                        help="Date of the earliest target observation."
                        "(in YYYY-MM-DD format). (Default: None)")
    parser.add_argument('--end-date', type=str, default=None,
                        action=rsactions.ParseDate, dest='mjd_end',
                        help="Date of the latest target observation."
                        "(in YYYY-MM-DD format). (Default: None)")
    parser.add_argument("-B", "--backends", type=str, action='append',
                        default=[], dest='backends',
                        help="The backends of the target obervations")
    parser.add_argument('-C', "--calibrated", dest='stage',
                        action='store_const', default='cleaned',
                        const='calibrated',
                        help="Review calibrated pulsar observations. "
                        "(Default: False)")
    parser.add_argument('-R', "--re-eval", dest='re_eval', action='store_true',
                        help="Review files with status 'new' even if they"
                        "already have a quality control assessment. "
                        "(Default: False)")
    parser.add_argument("-L", "--no-launcher", default=True, dest='launcher',
                        action='store_false',
                        help="Do not initialize launcher. (Default: False)")
    args = parser.parse_args()
    main()

