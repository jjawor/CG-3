Coastuguard3 is the python3 implementation of the old Coastguard pipeline developed by Patric Lazarus. The new pipeline is written in  python3 and introduces a bunch of new features:
- Multiprocessing of pulsars
- Improved UI for the manual RFI detection program
- UI for accesing the database - WIP
- Rework of the processing logic such that the pipeline becomes compatible with many telescopes and backends at the same time
- New RFI cleaning tools - WIP


